/**
 * Created by ares7 on 26.12.2017.
 */
var SITE = "";
var edit = {
    stateBio: false,

    bio: function() {
        // Edit bio
        var cB  = "<span class=\"cursor\" onclick=\"edit.bio();\"><span class=\"glyphicon glyphicon-pencil\"></span> Edit bio</span>";
        var nB  = "<button onclick='update.bio();'>Save bio</button>";

        if(!this.stateBio) {
            var b   = $("#val-bio");
            var old = b.text().trim();

            b.html("<textarea id='v-bio'>" + old + "</textarea>");
            $("#in-bio").html(nB);
        } else {
            $("#val-bio").text( $("#v-bio").val() );
            $("#in-bio").html(cB);
        }

        this.stateBio = !this.stateBio;
    },
    val: function(x) {
        // Edit other
        var input   = $("#val-" + x);
        var editor  = $("#in-" + x);

        var old     = input.text();
        old         = old.split(" ").join("");

        input.html("<input id='v-" + x + "' type='" + (x == "password" ? "password" : (x == "dob" ? "date" : "text")) + "' value='" + (x != "password" ? old : "") + "'>");
        editor.html("<button onclick='update.val(\"" + x + "\");'>SAVE</button>");
    },
    updateFromTwitch: function() {
        // Update from twitch
        $.get("ajax.php", {method: "twitch-update"}, function(data) {
            if(data == "success") {
                location.reload();
            } else {
                console.error(data);
            }
        }, "html");
    }
};

var update = {
    bio: function() {
        var val     = $("#v-bio").val();
        $.get("ajax.php", {method: "bio", value: val}, function(data) {
            if(data == "success") {
                edit.bio();
            } else {
                console.error(data);
                edit.bio();
            }
        }, "html");
    },
    val: function(x) {
        var cB      = "<span class=\"cursor\" onclick=\"edit.val('" + x + "');\"><span class=\"glyphicon glyphicon-pencil\"></span> Edit</span>";

        var val     = $("#v-" + x).val();
        if(x == "password")
            val = md5(val);


        $.get("ajax.php", {method: "val", field: x, value: val}, function(data) {
            if(data == "success") {
                $("#in-" + x).html(cB);
                if(x == "password") {
                    $("#val-" + x).html("**********");
                } else if(x == "avatar") {
                    $("#val-" + x).html("<img src='" + val + "' class='avatar'>");
                }
                else {
                    $("#val-" + x).html(val);
                }
            } else {
                console.error(data);
            }
        }, "html");
    }
};


function logout() {
    $.get(SITE + "ares/authenticate/ajax.php", {logout: true}, function(data) {
        if(data == "success") {
            location.reload();
        } else {
            console.error(data);
        }
    }, "html");
}
