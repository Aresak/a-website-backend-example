<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 31.01.2018
 * Time: 10:25
 */

define("_D", "../");
require_once _D . "aresak.php";
$sql        = Aresak::SQL();

$aytm = new AresakYouTubeModule($sql);
$output = array();
$i = 1;
do {
    $playlist = $aytm->GetPlaylist($i);
    if($playlist == null)
        return;

    $playlist->GetUploads();
    $newVideos = $playlist->NewVideos();


    array_push($output, $newVideos);
    $i ++;
}
while($playlist != null);

\GuzzleHttp\json_decode($output);