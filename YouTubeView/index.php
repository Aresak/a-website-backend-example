<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 30.10.2017
 * Time: 18:10
 */
define("_D", "../");
require_once _D . "aresak.php";
?>
<html>
<head>
    <title>YouTube View by Aresak</title>
    <script src="<?php echo _SITE_; ?>/js/jquery.js"></script>
</head>

<body>
<?php
$sql    = Aresak::SQL();

$aytm = new AresakYouTubeModule($sql);
if(isset($_GET["port"])) {
    $port = $_GET["port"];
    switch($port) {
        case "base":
            if(isset($_GET["rows"]))
                $rows = $_GET["rows"];
            else
                $rows = 5;

            if(isset($_GET["cols"]))
                $cols = $_GET["cols"];
            $cols = 5;

            // messing around fix
            if($rows > 10)
                $rows = 10;
            if($cols > 10)
                $cols = 10;

            if($rows < 1)
                $rows = 1;
            if($cols < 1)
                $cols = 1;

            $query = "SELECT * FROM `atm_ytview_videos` ORDER BY published_at DESC LIMIT " . ($rows * $cols);
            $result = mysqli_query($sql, $query)
                or die(mysqli_error($sql));

            echo "<table>";
            $b = 0;
            for($row = 0; $row < $rows; $row ++) {
                echo "<tr>";
                for($col = 0; $col < $cols; $col ++) {
                    if($b == 0) $b = 1;
                    else $b = 0;

                    $a = $row + $col;
                    $user = "SELECT * FROM ares_accounts WHERE ID='" . Aresak::mysqli_result($result, $a, "user_id") . "'";
                    $userR = mysqli_query($sql, $user)
                        or die(mysqli_error($sql));
                    $u = array();
                    $u["name"] = Aresak::mysqli_result($userR, 0, "display_name");

                    echo "<td class='frame v" . $b . "' onclick='video(\"" . Aresak::mysqli_result($result, $a, "video_id") . "\")'>";
                    echo "<div align='center' class='preview'><img class='previmg' src='https://i.ytimg.com/vi/" . Aresak::mysqli_result($result, $a, "video_id") . "/mqdefault.jpg'></div>";
                    echo "<div class='info'><b>" . $u["name"] . "</b><br>" . Aresak::mysqli_result($result, $a, "title") . "</div>";
                    echo "</td>";
                }
                echo "</tr>";
            }
            echo "</table>";
            ?>
            <style>
                body {
                    font-family: Arial;
                }

                td {
                    width: <?php echo (100 / $cols); ?>%;
                    height: <?php echo (100 / $rows); ?>%;
                    cursor: pointer;
                }

                .v0 {
                    background-color: rgba(240, 239, 236, 1);
                }

                .v1 {
                    background-color: rgba(213, 212, 209, 1);
                }

                .preview {
                    align-content: center;
                }

                table {
                    width: 100%;
                    height: 100%;
                    position absolute;
                }
            </style>
            <script>
                function video(x) {
                    window.open("https://www.youtube.com/watch?v=" + x);
                }

                function userRobot() {
                    $.get("<?php echo _SITE_; ?>/ares/YouTubeView/t.php", {}, function(data) {}, "html");
                }

                setTimeout(userRobot, 1500);
            </script>
            <?php
            break;
    }
}
?>
</body>
</html>
