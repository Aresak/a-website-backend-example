<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 29.12.2017
 * Time: 11:10
 */

define("_D", "../");
define("Google_API", true);
include _D . "aresak.php";


if(!isset($_GET["code"])) {
    die("Error: Missing required parameter 'code' in the request");
}

$code       = $_GET["code"];
$sql        = Aresak::SQL();
$youtube    = new YouTube($sql);

$youtube->Create_Client();
$youtube->Create_OAuth_Service();

$client     = $youtube->Client();
$client->authenticate($code);
$token      = $client->getAccessToken();
$youtube->Token(join("$$$", $token));

$_SESSION["g_token"]    = $token;
if(isset($_SESSION["uid"])) {
    // Link to the account
    $uid                = $_SESSION["uid"];
    $account            = new Account($sql);
    $account->CreateFrom_ID( (int) $uid );

    $youtube->Create_Link( $account );

    if(isset($_SESSION["redirect"])) {
        header("Location: " . $_SESSION["redirect"]);
    } else {
        header("Location: " . _SITE_);
    }
} else {
    $r                  = $youtube->OAuth()->userinfo->get();
    $query              = "SELECT * FROM ares_links WHERE service='" . Service_YouTube . "' AND twitch_id='" . $r->getId() . "'";
    $result             = mysqli_query($sql, $query)
        or die(mysqli_error($sql));

    if(mysqli_num_rows($result) == 0) {
        // Register
        $account    = new Account($sql);
        if(!$account->Register(
            strtolower(Aresak::cleanString($r->getName())),
            $r->getName(),
            "nullpassword",
            $r->getEmail(),
            $r->birthday
        )) {
            die("Failed to register, possible username duplicate");
            // todo bug ready registering with youtube, alert on this
        }

        $account->Activated(true);
        $account->Avatar($r->getPicture());


        $cookie     = new Cookie($sql, $account, Aresak::GenerateString(128));      // Login this user
        $ext        = mysqli_escape_string($sql, $r->getName()) . "||" . $r->getPicture();

        $t                          = join("|||", $youtube->Client()->getAccessToken());
        $_SESSION["google_token"]   = $t;
        $_SESSION["user"]           = $account->ID();

        $query/*redef*/ = "INSERT INTO ares_links (user_id, service, extras, token, twitch_id)
          VALUES ('" . $account->ID() . "', '" . Service_YouTube . "', '$ext',
                  '" . $t . "', '" . $r->getId() . "')";                                // New link query
        $result/*redef*/= mysqli_query($sql, $query)                                    // New link result
        or die(mysqli_error($sql));                                                     // SQLi error handling

        $account->SeenNow();                                                            // Seen now ay

        if(isset($_SESSION["redirect"])) {
            header("Location: " . $_SESSION["redirect"]);
        } else {
            header("Location: " . _SITE_);
        }
    } else {
        // Login
        $account    = new Account($sql);                                                // Get the user's account...
        $account->CreateFrom_ID((int) Aresak::mysqli_result($result, 0, "user_id") );        // ...from the ID

        $cookie     = new Cookie($sql, $account, Aresak::GenerateString(128));          // Save the session

        $_SESSION["twitch_token"]   = $twitch->Token();
        $_SESSION["user"]           = $account->ID();

        if(isset($_SESSION["redirect"])) {
            header("Location: " . $_SESSION["redirect"]);
        } else {
            header("Location: " . _SITE_);
        }
    }
}