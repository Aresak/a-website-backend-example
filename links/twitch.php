<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 21.12.2017
 * Time: 17:08
 */
define("_D", "../");
require_once _D . "aresak.php";

// Check if there is a code in the URL
if (isset($_GET["code"])) {
    if(!isset($_GET["state"]))
        die("Failed to obtain state");
    $state      = $_GET["state"];                                                       // Get the state
    $code       = $_GET["code"];                                                        // Get the code

    // Work with state
    if($state != "null") {
        // If there is not a null state, decode it
        $state      = substr($state, 1, (strlen($state) - 1)) . "==";
        $state      = base64_decode($state);
    }

    $twitch     = new Twitch();                                                         // Create a new Twitch object
    $twitch->GenerateToken($code);                                                      // Generate the token
    $twitch->CreateFrom_Token($twitch->Token());                                        // Create the user from token


    $sql    = Aresak::SQL();
    if($state == "null") {
        // Register User || Login User

        $query      = "SELECT * FROM ares_links
                      WHERE twitch_id='" . $twitch->_ID() . "'";                        // ID query
        $result     = mysqli_query($sql, $query)                                        // ID result
            or die(mysqli_error($sql));                                                 // SQLi error handling

        if(mysqli_num_rows($result) == 0) {
            // Failed to find this ID in the DB
            // Register ...
            if(!isset($_SESSION["pw"])) {
                $password = "nullpassword";
            } else $password = $_SESSION["pw"];
            if(!isset($_SESSION["dob"])) {
                $dob    = "0000-00-00";
            } else $dob     = $_SESSION["dob"];
            $password   = mysqli_escape_string($sql, md5($password));                   // Prepare the password for SQL
            $dob        = mysqli_escape_string($sql, $dob);                             // Prepare date of birth for SQL

            $account    = new Account($sql);                                            // Prepare a new Account object
            if(!$account->Register(                                                     // Register with...
                $twitch->Name(),                                                        // ...name
                $twitch->Display_Name(),                                                // ...display name
                $password,                                                              // ...password
                $twitch->Email(),                                                       // ...email
                $dob                                                                    // ...date of birth
            )) {
                // Failed to register...
                die("Failed to register, possible username duplicate");
                // todo bug ready registering with twitch, alert on this
            }
            // Succeed to register...

            $account->Activated(true);                                                  // Set the account to activated
            $account->Bio($twitch->Bio());                                              // Set the account bio

            $account->Avatar($twitch->Logo());                                          // Set the avatar from Twitch

            $cookie     = new Cookie($sql, $account, Aresak::GenerateString(128));      // Login this user
            $ext        = $twitch->Display_Name() . "||" . $twitch->Logo() . "||" . $twitch->raw_tokens["refresh_token"];

            $_SESSION["twitch_token"]   = $twitch->Token();
            $_SESSION["user"]           = $account->ID();

            $query/*redef*/ = "INSERT INTO ares_links (user_id, service, extras, token, twitch_id)
          VALUES ('" . $account->ID() . "', '" . Service_Twitch . "', '$ext',
                  '" . $twitch->Token() . "', '" . $twitch->_ID() . "')";                   // New link query
            $result/*redef*/= mysqli_query($sql, $query)                                    // New link result
            or die(mysqli_error($sql));                                                     // SQLi error handling

            $account->SeenNow();                                                            // Seen now ay

            if(isset($_SESSION["redirect"])) {
                header("Location: " . $_SESSION["redirect"]);
            } else {
                header("Location: " . _SITE_);
            }
        }
        // Found this ID in the DB
        // Login ...
        $account    = new Account($sql);                                                // Get the user's account...
        $account->CreateFrom_ID((int) Aresak::mysqli_result($result, 0, "user_id") );        // ...from the ID

        $cookie     = new Cookie($sql, $account, Aresak::GenerateString(128));          // Save the session

        $_SESSION["twitch_token"]   = $twitch->Token();
        $_SESSION["user"]           = $account->ID();

        if(isset($_SESSION["redirect"])) {
            header("Location: " . $_SESSION["redirect"]);
        } else {
            header("Location: " . _SITE_);
        }
    } else {
        // Create a link for this user
        $account    = new Account($sql);
        if(!$account->CreateFrom_CreationDate((int) $state)) {
            // There was a fake state
            die("Failed to match state");
        }

        $query      = "SELECT * FROM ares_links WHERE service='" . Service_Twitch . "'
                    AND user_id='" . $account->ID() . "'";                              // Link check query
        $result     = mysqli_query($sql, $query)                                        // Link check result
            or die(mysqli_error($sql));                                                 // SQLi error handling

        if(mysqli_num_rows($result) != 0) {
            // This account has already linked Twitch account
            if(isset($_SESSION["redirect"])) {
                header("Location: " . $_SESSION["redirect"] . "?linked=false");
            } else {
                header("Location: " . _SITE_ . "?linked=false");
            }
        }

        // Create a new link

        $ext        = $twitch->Display_Name() . "||" . $twitch->Logo() . "||" . $twitch->raw_tokens["refresh_token"];
        $query/*redef*/ = "INSERT INTO ares_links (user_id, service, extras, token, twitch_id)
          VALUES ('" . $account->ID() . "', '" . Service_Twitch . "', '" . $ext . "',
                  '" . $twitch->Token() . "', '" . $twitch->_ID() . "')";               // New link query
        $result/*redef*/= mysqli_query($sql, $query)                                    // New link result
            or die(mysqli_error($sql));                                                 // SQLi error handling

        $_SESSION["twitch_token"]   = $twitch->Token();
        $_SESSION["user"]           = $account->ID();

        $account->SeenNow();                                                            // Seen now ay

        if(isset($_SESSION["redirect"])) {
            header("Location: " . $_SESSION["redirect"] . "?linked=true");
        } else {
            header("Location: " . _SITE_ . "?linked=true");
        }
    }
} else {
    die("Failed to obtain code");
}