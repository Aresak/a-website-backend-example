<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 19.12.2017
 * Time: 10:41
 * This script is required on top of every page
 */

// Debug
if(defined("_debug")) {
    /*ini_set('display_errors', 'On');                                // Display error
    error_reporting(E_ALL | E_STRICT);                              // Show even warnings and be very strict*/
} else {
    error_reporting(~E_DEPRECATED);
}

// Defines
define("ares_included", true);                                      // Is this library included?
define("_SITE_", "");                        // The root site URL

// Define services
define("Service_Twitch", 1);                                        // Twitch Service ID
define("Service_YouTube", 2);                                       // YouTube Service ID

// Define Account requirements
define("_CC_UsernameLengthMin",             4);                     // Minimal username length
define("_CC_UsernameLengthMax",             10);                    // Maximal username length
define("_CC_UsernameLegal",                 "/[a-zA-Z0-9_]/");      // What user names are legal
define("_CC_DisplayNameLengthMin",          4);                     // Minimal display name length
define("_CC_DisplayNameLengthMax",          10);                    // Maximal display name length
define("_CC_DisplayNameLegal",              "/[a-zA-Z0-9_]/");      // What display names are legal
define("_CC_PasswordMin",                   8);                     // Minimal length of a password
define("_CC_PasswordMax",                   30);                    // Maximal length of a password
define("_CC_DoBMin",                        "1950");                // Who can be the oldest lul?
define("_CC_DoBMax",                        "2005");                // Youngest in 2005


// Start the session for
if(session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(!defined("_D"))
    die("Not defined path _D");
include_once _D . 'Google/vendor/autoload.php';


/**
 * Aresak Library for DGI
 * @version     1.0
 * @author      Aresak
 */
class Aresak
{
    /**
     * Create a SQL connection to the database
     * @return mysqli
     */
    public static function SQL()
    {
        $f = rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128,
                hash('sha256', "", true),
                substr(base64_decode(""), mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
                MCRYPT_MODE_CBC,
                substr(base64_decode(""), 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC))
            ),
            "\0"
        );
        $b = explode("|", $f);
        $sql = mysqli_connect($b[0], $b[2], $b[1], $b[2])
            or die(mysqli_error($sql));

        // Symbiant Connection, cuz DGI can't get domain online in 2 days
        // ...
        return $sql;
    }

    /**
     *  Cleans a string
     *  @param string $string The input string
     *  @return string
     */
    public static function cleanString($string) {
        $string = str_replace(' ', '_', $string); // Replaces all spaces with underscores.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    /**
     * Get an output from a result field and row
     * @param $result
     * @param $row
     * @param int $field
     * @return Mixed
     */
    public static function mysqli_result($result, $row, $field = 0)
    {
        if ($result === false) return false;
        if ($row >= mysqli_num_rows($result)) return false;
        if (is_string($field) && !(strpos($field, ".") === false)) {
            $t_field = explode(".", $field);
            $field = -1;
            $t_fields = mysqli_fetch_fields($result);
            for ($id = 0; $id < mysqli_num_fields($result); $id++) {
                if ($t_fields[$id]->table == $t_field[0] && $t_fields[$id]->name == $t_field[1]) {
                    $field = $id;
                    break;
                }
            }
            if ($field == -1) return false;
        }
        mysqli_data_seek($result, $row);
        $line = mysqli_fetch_array($result);
        return isset($line[$field]) ? $line[$field] : false;
    }

    /**
     *  Generate a random string by length
     *  @param int $length The length of the string
     *  @return string
     */
    public static function GenerateString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     *  Encrypt an message with a key
     *  @param string $data The data that needs to be encrypted
     *  @param string $key  The encryption key
     *  @return string
     */
    public static function encrypt($data, $key){
        return base64_encode(
            mcrypt_encrypt(
                MCRYPT_RIJNDAEL_128,
                $key,
                $data,
                MCRYPT_MODE_CBC,
                "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
            )
        );
    }

    /**
     *  Decrypt an message with a key
     *  @param string $data The encrypted data that needs to be decrypted
     *  @param string $key  The decryption key
     *  @return string
     */
    public static function decrypt($data, $key){
        $decode = base64_decode($data);
        return mcrypt_decrypt(
            MCRYPT_RIJNDAEL_128,
            $key,
            $decode,
            MCRYPT_MODE_CBC,
            "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
        );
    }

    /**
     *  Writes a meta into head
     *  @return void
     */
    public static function Meta() {
        echo "\n\n<script src='" . _SITE_ . "/ares/aresak.js'></script>";
        echo "\n<script src=\"https://smartlock.google.com/client\"></script>\n";
        echo "\n<link rel='stylesheet' href='" . _SITE_ . "/ares/aresak.css' />\n\n";
    }

    /**
     *  Writes a meta into body
     *  @return void
     */
    public static function Body() {

    }

    /**
     *  Writes a custom Aresak navigation bar
     *  @param Account $account The account object
     *  @return void
     */
    public static function NavBar($account) {
        if(_Logged) {
            // Logged in
            if($account->Role()->ID() == 3) {
                // Administrator
                echo "<li id='admin-item'><a href='" . _SITE_ . "/ares/me/admin.php'>Administration</a></li>";
            }

            echo  "<li id='me-item'>"
                . "<a href='" . _SITE_ . "/ares/me/'>"
                . "<img src='" . $account->Avatar() . "' class='avatar'> " . $account->Display_Name()
                . "</a>"
                . "<span id='logout'><a href='#' class='logout-h' onclick='logout();'>LOGOUT</a></span>"
                . "</li>";
            ?>
<script>$("#me-item").hover(function() {
        $("#logout").show();
    }, function() {
        $("#logout").hide();
    });</script>
            <?php
        } else {
            echo  "<li>"
                . "<a href='" . _SITE_ . "/ares/authenticate'>LOGIN</a>"
                . "</li>";
        }
    }
}

/** Account class for DGI
 *  @author     Aresak
 *  @version    1.2
 */
class Account
{
    /** @var int The account ID */
    private $id;
    /** @var string Username used for login */
    private $username;
    /** @var string The user's display name */
    private $display_name;
    /** @var string User's email */
    private $email;
    /** @var int User's date of birth */
    private $dob;
    /** @var string User's location */
    private $location;
    /** @var string User's avatar URL location */
    private $avatar;
    /** @var string User's bio */
    private $bio;
    /** @var string User's banner URL location */
    private $banner;
    /** @var Twitch User's connected Twitch account */
    private $twitch;
    /** @var YouTube User's connected YouTube account */
    private $youtube;
    /** @var int The date when user created his account */
    private $created_at;
    /** @var int When the user has been seen last */
    private $seen_at;
    /** @var Cookie The cookie that is used to remember the login state */
    private $cookie;
    /** @var mysqli MySQLi connection to database which holds this user's account */
    private $sql;
    /** @var boolean Is the account initialized? */
    private $initialized = false;
    /** @var string The activation key, sent to an email */
    private $activation_key;
    /** @var boolean Is the account activated? */
    private $activated;
    /** @var string The hashed password */
    private $password;
    /** @var string The password salt */
    private $password_salt;
    /** @var Role The user's role */
    private $role;
    /** @var int The user's amount of coins */
    private $coins;

    /**
     * Create a new account object and wait
     * @param mysqli $mysqli The MySQLi connection to the database which holds accounts
     */
    public function __construct($mysqli = null)
    {
        $this->sql = $mysqli;
    }

    /**
     * Returns the account ID
     * @return int
     */
    public function ID() {
        return $this->id;
    }

    /**
     * If empty argument, returns Username otherwise sets the Username
     * @param string $username The username
     * @return mixed
     */
    public function Username($username = null)
    {
        if ($username == null) {
            // The request was to return the username
            return $this->username;
        }

        // The request was to set the username
        // Check if the username is not string
        if (!is_string($username)) {
            // The username is not a string
            ob_start();
            var_dump($username);
            die("<b><span style='color: red;'>Failed to call Account->Username() as the username argument is not a string!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay

        $username   = mysqli_escape_string( $this->sql, strtolower($username) );        // Prepare username for SQLi
        $this->username = $username;

        $query      = "UPDATE ares_accounts
                        SET username='$username' WHERE ID='" . $this->id . "'";         // Update query
        $result     = mysqli_query($this->sql, $query)                                  // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling
        return true;
    }

    /**
     * If empty argument, returns Username otherwise sets the Username
     * @param string $display_name The user's display name
     * @return mixed
     */
    public function Display_Name($display_name = null)
    {
        if ($display_name == null) {
            // The request was to return the display name
            return $this->display_name;
        }

        // The request was to set the display name
        // Check if the display name is not string
        if (!is_string($display_name)) {
            // The display name is not a string
            ob_start();
            var_dump($display_name);
            die("<b><span style='color: red;'>Failed to call Account->Display_Name() as the display name argument is not a string!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay

        $display_name       = mysqli_escape_string($this->sql, $display_name);          // Prepare display name for SQLi
        $this->display_name = $display_name;

        $query              = "UPDATE ares_accounts SET display_name='$display_name'
                              WHERE ID='" . $this->id . "'";                            // Update query
        $result             = mysqli_query($this->sql, $query)                          // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling
        return true;
    }

    /**
     * If empty argument, returns Email otherwise sets the Email
     * @param string $email The email
     * @return mixed
     */
    public function Email($email = null)
    {
        if ($email == null) {
            // The request was to return the email
            return $this->email;
        }

        // The request was to set the email
        // Check if the email is not string
        if (!is_string($email)) {
            // The email is not a string
            ob_start();
            var_dump($email);
            die("<b><span style='color: red;'>Failed to call Account->Email() as the email argument is not a string!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay

        $email          = mysqli_escape_string( $this->sql, strtolower($email) );       // Prepare email for SQLi
        $this->email    = $email;

        $query          = "UPDATE ares_accounts SET email='$email'
                          WHERE ID='" . $this->id . "'";                                // Update query
        $result         = mysqli_query($this->sql, $query)                              // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling
        return true;
    }

    /**
     * If empty argument, returns Date of Birth otherwise sets the Date of Birth
     * @param string $dob The Date of Birth in DD/MM/YYYY
     * @return mixed
     */
    public function DateOfBirth($dob = null)
    {
        if ($dob == null) {
            // The request was to return the date of birth
            return $this->dob;
        }

        // The request was to set the date of birth
        // Check if the date of birth is not string
        if (!is_string($dob)) {
            // The date of birth is not a string
            ob_start();
            var_dump($dob);
            die("<b><span style='color: red;'>Failed to call Account->DateOfBirth() as the date of birth argument is not a string!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay

        $dob        = mysqli_escape_string( $this->sql, $dob );                         // Prepare dob for SQLi
        $this->dob  = $dob;

        $query      = "UPDATE ares_accounts SET dob='$dob'
                      WHERE ID='" . $this->id . "'";                                    // Update query
        $result     = mysqli_query($this->sql, $query)                                  // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling
        return true;
    }

    /**
     * If empty argument, returns Localtion otherwise sets the Location
     * @param string $location The location
     * @return mixed
     */
    public function Location($location = null)
    {
        if ($location == null) {
            // The request was to return the location
            return $this->location;
        }

        // The request was to set the location
        // Check if the location is not string
        if (!is_string($location)) {
            // The location is not a string
            ob_start();
            var_dump($location);
            die("<b><span style='color: red;'>Failed to call Account->Location() as the location argument is not a string!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay

        $location       = mysqli_escape_string( $this->sql, $location );                // Prepare location for SQLi
        $this->location = $location;

        $query          = "UPDATE ares_accounts SET location='$location'
                          WHERE ID='" . $this->id . "'";                                // Update query
        $result         = mysqli_query($this->sql, $query)                              // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling
        return true;
    }

    /**
     * If empty argument, returns Avatar URL otherwise sets the Avatar URL
     * @param string $avatar The Avatar URL
     * @return mixed
     */
    public function Avatar($avatar = null)
    {
        if ($avatar == null) {
            // The request was to return the avatar url
            return $this->avatar;
        }

        // The request was to set the location
        // Check if the avatar is not string
        if (!is_string($avatar)) {
            // The avatar is not a string
            ob_start();
            var_dump($avatar);
            die("<b><span style='color: red;'>Failed to call Account->Avatar() as the avatar argument is not a string!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay

        $avatar         = mysqli_escape_string( $this->sql, $avatar );                  // Prepare location for SQLi
        $this->avatar   = $avatar;

        $query          = "UPDATE ares_accounts SET avatar='$avatar'
                          WHERE ID='" . $this->id . "'";                                // Update query
        $result         = mysqli_query($this->sql, $query)                              // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling
        return true;
    }

    /**
     * If empty argument, returns Bio otherwise sets the Bio
     * @param string $bio The raw text bio
     * @return mixed
     */
    public function Bio($bio = null)
    {
        if ($bio == null) {
            // The request was to return the bio
            return $this->bio;
        }

        // The request was to set the bio
        // Check if the bio is not string
        if (!is_string($bio)) {
            // The bio is not a string
            ob_start();
            var_dump($bio);
            die("<b><span style='color: red;'>Failed to call Account->Bio() as the bio argument is not a string!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay
        $bio            = mysqli_escape_string( $this->sql, $bio );                     // Prepare bio for SQLi
        $this->bio      = $bio;

        $query          = "UPDATE ares_accounts SET bio='$bio'
                          WHERE ID='" . $this->id . "'";                                // Update query
        $result         = mysqli_query($this->sql, $query)                              // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling
        return true;
    }

    /**
     *  If empty argument, returns the coins amount, otherwise sets the coins amount
     *  @param float $coins     The coins amount
     *  @return float|boolean   The coins amount or boolean on set
     *  @since 1.2
     */
    public function Coins($coins = null) {
        if($coins == null) {
            // The request was to return the coins
            return $this->coins;
        }
        // The request was to set the coins
        $query      = "UPDATE ares_accounts SET coins='$coins'
                      WHERE ID='" . $this->id . "'";
        $result     = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        return true;
    }

    /**
     * If empty argument, returns Banner URL otherwise sets the Banner URL
     * @param string $banner The banner URL
     * @return mixed
     */
    public function Banner($banner = null)
    {
        if ($banner == null) {
            // The request was to return the banner url
            return $this->banner;
        }

        // The request was to set the banner url
        // Check if the banner is not string
        if (!is_string($banner)) {
            // The banner is not a string
            ob_start();
            var_dump($banner);
            die("<b><span style='color: red;'>Failed to call Account->Banner() as the banner argument is not a string!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay

        $banner         = mysqli_escape_string($this->sql, $banner);                    // Prepare banner for SQLi
        $this->banner   = $banner;

        $query          = "UPDATE ares_accounts SET banner='$banner'
                          WHERE ID='" . $this->id . "'";                                // Update query
        $result         = mysqli_query($this->sql, $query)                              // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling
        return true;
    }

    /**
     *  If empty argument, returns the hashed password otherwise sets the password
     *  @param string $password The already hashed password with salt
     *  @return string|boolean
     */
    public function Password($password = null) {
        if($password == null) {
            // The request was to return the hashed password
            return $this->password;
        }

        // The request was to set the password
        // Check if the password is not string
        if (!is_string($password)) {
            // The password is not a string
            ob_start();
            var_dump($password);
            die("<b><span style='color: red;'>Failed to call Account->Password() as the password argument is not a string!</span></b><br>" . ob_get_clean());
        }

        // Check if the password is an md5 hash
        if(strlen($password) != 32) {
            // It is not an md5 hash
            die("<b><span style='color: red;'>Failed to call Account->Password() as the password argument is not an md5 hash!</span></b>");
        }
        // The parameter is okay

        $this->password     = $password;
        $query              = "UPDATE ares_accounts SET password='$password'
                              WHERE ID='" . $this->ID() . "'";                      // The update query
        $result             = mysqli_query($this->sql, $query)                      // The update result
            or die(mysqli_error($this->sql));                                       // SQLi error handling
        return true;
    }

    /**
     *  If empty argument, returns the password salt otherwise sets the new password salt
     *  @param string $password_salt The password salt
     *  @return string|boolean
     */
    public function Password_Salt($password_salt = null) {
        if($password_salt == null) {
            // The request was to return the password salt
            return $this->password_salt;
        }

        // The request was to set the password salt
        // Check if the password salt is a string
        if (!is_string($password_salt)) {
            // The password salt is not a string
            ob_start();
            var_dump($password_salt);
            die("<b><span style='color: red;'>Failed to call Account->Password_Salt() as the password salt argument is not a string!</span></b><br>" . ob_get_clean());
        }

        // The argument is okay
        $password_salt      = mysqli_escape_string($this->sql, $password_salt);     // Preparing the arg for SQLi
        $this->password_salt= $password_salt;

        $query              = "UPDATE ares_accounts SET password_salt='$password_salt'
                              WHERE ID='" . $this->ID() . "'";                      // The update query
        $result             = mysqli_query($this->sql, $query)                      // The update result
            or die(mysqli_error($this->sql));                                       // SQLi error handling
        return true;
    }

    /**
     * If empty argument, returns Twitch object otherwise sets the Twitch object
     * @param Twitch $twitch The Twitch object with link
     * @return mixed
     */
    public function Twitch($twitch = null)
    {
        if ($twitch == null) {
            // The request was to return the twitch object
            return $this->twitch;
        }

        // The request was to set the twitch object
        // Check if the twitch is not a twitch object
        if (!$twitch instanceof Twitch) {
            // The twitch is not a twitch object
            ob_start();
            var_dump($twitch);
            die("<b><span style='color: red;'>Failed to call Account->Twitch() as the twitch argument is not a twitch object!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay
        $this->twitch = $twitch;

        // todo Update 'twitch' link in the database
        return true;
    }

    /**
     * If empty argument, returns YouTube object otherwise sets the YouTube object
     * @param YouTube $youtube The YouTube object with link
     * @return mixed
     */
    public function YouTube($youtube = null)
    {
        if ($youtube == null) {
            // The request was to return the youtube object
            return $this->youtube;
        }

        // The request was to set the youtube object
        // Check if the youtube is not a youtube object
        if (!$youtube instanceof YouTube) {
            // The location is not a youtube object
            ob_start();
            var_dump($youtube);
            die("<b><span style='color: red;'>Failed to call Account->YouTube() as the youtube argument is not a youtube object!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay
        $this->youtube = $youtube;

        // todo Update 'youtube' link in the database
        return true;
    }

    /**
     *  Get the role if the parameter is not present, otherwise set the role
     *  @param Role|int $role The role to assign
     *  @since 1.1
     *  @return Role|boolean
     */
    public function Role($role = null) {
        if($role == null) {
            // The request was to get the role
            return $this->role;
        }

        // The request was to set the role
        if(is_integer($role)) {
            // The $role variable is an ID of the role
            $role   = new Role($this->sql, $role);      // Create it as an object instead of the ID
        }

        $update_query   = "UPDATE ares_accounts SET role_id='" . $role->ID() . "'
                           WHERE ID='" . $this->id . "'";               // Update query
        $update_result  = mysqli_query($this->sql, $update_query)       // Update result
            or die(mysqli_error($this->sql));                           // MySQLi error handling

        $this->role     = $role;                                        // Set the new role
        return true;
    }

    /**
     * Returns the date when the account was created
     * @return int
     */
    public function Created_At()
    {
        // The created at date is read only, so only returns
        return $this->created_at;
    }

    /**
     * If empty argument, returns Last Seen date otherwise sets the Last Seen date
     * @param int $seen_at The last seen at date
     * @return mixed
     */
    public function Last_Seen_At($seen_at = null)
    {
        if ($seen_at == null) {
            // The request was to return the last seen at date
            return $this->seen_at;
        }

        // The request was to set the last seen at date
        // Check if the seen at is not integer
        if (!is_int($seen_at)) {
            // The location is not an integer
            ob_start();
            var_dump($seen_at);
            die("<b><span style='color: red;'>Failed to call Account->Last_Seen_at() as the seen at argument is not an integer!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay
        $seen_at        = mysqli_escape_string( $this->sql, $seen_at );                 // Prepare last seen at for SQLi
        $this->seen_at  = $seen_at;


        $query          = "UPDATE ares_accounts SET seen_at='$seen_at'
                          WHERE ID='" . $this->id . "'";                                // Update query
        $result         = mysqli_query($this->sql, $query)                              // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling
        return true;
    }

    /**
     * If empty argument, returns Cookie otherwise sets the Cookie
     * @param Cookie $cookie The cookie object
     * @return mixed
     */
    public function Cookie($cookie = null)
    {
        if ($cookie == null) {
            // The request was to return the cookie
            return $this->cookie;
        }

        // The request was to set the cookie
        // Check if the cookie is not object
        if (!$cookie instanceof Cookie) {
            // The cookie is not a object
            ob_start();
            var_dump($cookie);
            die("<b><span style='color: red;'>Failed to call Account->Cookie() as the cookie argument is not an object!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay

        $this->cookie   = $cookie;
        return true;
    }

    /**
     * If empty argument, returns Activation Key otherwise sets the Activation Key
     * @param string $activation_key The activation key
     * @return mixed
     */
    public function Activation_Key($activation_key = null) {
        if ($activation_key == null) {
            // The request was to return the cookie
            return $this->cookie;
        }

        // The request was to set the cookie
        // Check if the activation key is not string
        if (!is_string($activation_key)) {
            // The activation key is not a string
            ob_start();
            var_dump($activation_key);
            die("<b><span style='color: red;'>Failed to call Account->Activation_Key() as the activation key argument is not a string!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay

        $activation_key         = mysqli_escape_string($this->sql, $activation_key);        // Prepare activation key for SQLi
        $this->activation_key   = $activation_key;

        $query          = "UPDATE ares_accounts SET activation_key='$activation_key'
                          WHERE ID='" . $this->id . "'";                                    // Update query
        $result         = mysqli_query($this->sql, $query)                                  // Update result
            or die(mysqli_error($this->sql));                                               // SQLi error handling
        return true;
    }

    /**
     * If empty argument, returns if is the account activated otherwise set the activated state
     * @param boolean $activated Is the account activated?
     * @return boolean
     */
    public function Activated($activated = null) {
        if($activated == null) {
            // The request was to return if is the account activated
            return ($this->activated == 0 ? false : true);
        }

        // The request was to set if is the account activated
        // Check if the activated parameter is an boolean
        if(!is_bool($activated)) {
            // The activated parameter is not an boolean
            ob_start();
            var_dump($activated);
            die("<b><span style='color: red;'>Failed to call Account->Activated() as the activated argument is not an boolean!</span></b><br>" . ob_get_clean());
        }

        // The parameter is okay
        $this->activated = $activated;

        $query          = "UPDATE ares_accounts SET activated='$activated'
                          WHERE ID='" . $this->id . "'";                                // Update query
        $result         = mysqli_query($this->sql, $query)                              // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling
        return false;
    }

    /**
     *  Is this Account object initialized?
     * @return boolean
     */
    public function Initialized()
    {
        return $this->initialized;
    }

    /**
     *  Create Account object from cookie. Returns whether success or not.
     * @param Cookie $cookie The cookie object
     * @return boolean
     */
    public function CreateFrom_Cookie($cookie)
    {
        return $this->CreateFrom_ID( $cookie->User_ID() );
    }

    /**
     *  Create Account object from login. Returns whether success or not.
     * @param string $username The username - raw string
     * @param string $password The password - md5(password)
     * @throws Exception
     * @return boolean
     */
    public function CreateFrom_Login($username, $password)
    {
        // Check if username is a string
        if(!is_string($username)) {
            // Is not a string
            ob_start();
            var_dump($username);
            die("<b><span style='color: red;'>Failed to call Account->CreateFrom_Login() as the username argument is not a string!</span></b><br>" . ob_get_clean());

        }

        // Username is a string
        // Check if password is a string
        if(!is_string($password)) {
            // Is not a string
            ob_start();
            var_dump($password);
            die("<b><span style='color: red;'>Failed to call Account->CreateFrom_Login() as the password argument is not a string!</span></b><br>" . ob_get_clean());
        }

        // Password is a string
        // Query the username
        $username   = mysqli_escape_string($this->sql, strtolower($username));          // Prepare username for SQLi
        $password   = mysqli_escape_string($this->sql, $password);                      // Prepare password for SQLi

        $query      = "SELECT * FROM ares_accounts WHERE username='$username'";         // Account query
        $result     = mysqli_query($this->sql, $query)                                  // Account result
            or die(mysqli_error($this->sql));                                           // SQLi error handling

        if(mysqli_num_rows($result) == 0) {
            // There is no account with this username
            throw new Exception("0 - Wrong username or password", 0);
        }

        $pw_salted  = md5(Aresak::mysqli_result($result, 0, "password_salt") . $password);      // Get the password salt

        // Check the password
        if($pw_salted != Aresak::mysqli_result($result, 0, "password")) {
            // Wrong password
            throw new Exception("1 - Wrong username or password", 1);
        }

        if(Aresak::mysqli_result($result, 0, "activated") != 1) {
            // The account is not activated
            throw new Exception("3 - Account not activated", 3);
        }


        $this->username         = Aresak::mysqli_result($result, 0, "username");
        $this->display_name     = Aresak::mysqli_result($result, 0, "display_name");
        $this->email            = Aresak::mysqli_result($result, 0, "email");
        $this->dob              = Aresak::mysqli_result($result, 0, "dob");
        $this->location         = Aresak::mysqli_result($result, 0, "location");
        $this->bio              = Aresak::mysqli_result($result, 0, "bio");
        $this->banner           = Aresak::mysqli_result($result, 0, "banner");
        $this->avatar           = Aresak::mysqli_result($result, 0, "avatar");
        $this->created_at       = Aresak::mysqli_result($result, 0, "created_at");
        $this->seen_at          = Aresak::mysqli_result($result, 0, "seen_at");
        $this->activation_key   = Aresak::mysqli_result($result, 0, "activation_key");
        $this->activated        = Aresak::mysqli_result($result, 0, "activated");
        $this->id               = Aresak::mysqli_result($result, 0, "ID");
        $this->password_salt    = Aresak::mysqli_result($result, 0, "password_salt");
        $this->password         = Aresak::mysqli_result($result, 0, "password");
        $this->role             = new Role($this->sql, Aresak::mysqli_result($result, 0, "role_id"));
        $this->coins            = Aresak::mysqli_result($result, 0, "coins");

        $this->initialized      = true;
        $this->SeenNow();

        // todo Link twitch in Account->CreateFrom_Login()
        // todo Link youtube in Account->CreateFrom_Login()

        return true;
    }

    /**
     * Create Account object from ID. Returns whether success or not.
     * @param int $id The user ID
     * @return boolean
     */
    public function CreateFrom_ID($id) {
        // Check if the id is an integer
        if(!is_integer($id)) {
            // ID is not an integer
            ob_start();
            var_dump($id);
            die("<b><span style='color: red;'>Failed to call Account->CreateFrom_ID() as the ID argument is not an integer!</span></b><br>" . ob_get_clean());
        }

        // The id is an integer
        // Query the ID
        $query          = "SELECT * FROM ares_accounts WHERE ID='$id'";         // ID Query
        $result         = mysqli_query($this->sql, $query)                      // ID Result
            or die(mysqli_error($this->sql));                                   // SQLi error handling

        if(mysqli_num_rows($result) == 0) {
            // There is no account with this ID
            return false;
        }


        $this->username         = Aresak::mysqli_result($result, 0, "username");
        $this->display_name     = Aresak::mysqli_result($result, 0, "display_name");
        $this->email            = Aresak::mysqli_result($result, 0, "email");
        $this->dob              = Aresak::mysqli_result($result, 0, "dob");
        $this->location         = Aresak::mysqli_result($result, 0, "location");
        $this->bio              = Aresak::mysqli_result($result, 0, "bio");
        $this->banner           = Aresak::mysqli_result($result, 0, "banner");
        $this->avatar           = Aresak::mysqli_result($result, 0, "avatar");
        $this->created_at       = Aresak::mysqli_result($result, 0, "created_at");
        $this->seen_at          = Aresak::mysqli_result($result, 0, "seen_at");
        $this->activation_key   = Aresak::mysqli_result($result, 0, "activation_key");
        $this->activated        = Aresak::mysqli_result($result, 0, "activated");
        $this->id               = Aresak::mysqli_result($result, 0, "ID");
        $this->password_salt    = Aresak::mysqli_result($result, 0, "password_salt");
        $this->password         = Aresak::mysqli_result($result, 0, "password");
        $this->role             = new Role($this->sql, Aresak::mysqli_result($result, 0, "role_id"));
        $this->coins            = Aresak::mysqli_result($result, 0, "coins");

        $this->initialized      = true;
        $this->SeenNow();


        // todo Link twitch in Account->CreateFrom_ID()
        // todo Link youtube in Account->CreateFrom_ID()

        return true;
    }

    /**
     *  Create Account object from the date when the user registered. Returns whether success or not
     *  @param int $created_at The date when the user registered
     *  @return boolean
     */
    public function CreateFrom_CreationDate($created_at) {
        // Check if the created at parameter is an integer
        if(!is_integer($created_at)) {
            // It is not an integer
            ob_start();
            var_dump($created_at);
            die("<b><span style='color: red;'>Failed to call Account->CreateFrom_CreationDate() as the created at argument is not an integer!</span></b><br>" . ob_get_clean());
        }
        // The parameter is okay

        $query      = "SELECT * FROM ares_accounts WHERE created_at='$created_at'";         // User query
        $result     = mysqli_query($this->sql, $query)                                      // User result
            or die(mysqli_error($this->sql));                                               // SQLi error handling


        if(mysqli_num_rows($result) == 0) {
            // There is no account registered at this date
            return false;
        }

        $this->username         = Aresak::mysqli_result($result, 0, "username");
        $this->display_name     = Aresak::mysqli_result($result, 0, "display_name");
        $this->email            = Aresak::mysqli_result($result, 0, "email");
        $this->dob              = Aresak::mysqli_result($result, 0, "dob");
        $this->location         = Aresak::mysqli_result($result, 0, "location");
        $this->bio              = Aresak::mysqli_result($result, 0, "bio");
        $this->banner           = Aresak::mysqli_result($result, 0, "banner");
        $this->avatar           = Aresak::mysqli_result($result, 0, "avatar");
        $this->created_at       = Aresak::mysqli_result($result, 0, "created_at");
        $this->seen_at          = Aresak::mysqli_result($result, 0, "seen_at");
        $this->activation_key   = Aresak::mysqli_result($result, 0, "activation_key");
        $this->activated        = Aresak::mysqli_result($result, 0, "activated");
        $this->id               = Aresak::mysqli_result($result, 0, "ID");
        $this->password_salt    = Aresak::mysqli_result($result, 0, "password_salt");
        $this->password         = Aresak::mysqli_result($result, 0, "password");
        $this->role             = new Role($this->sql, Aresak::mysqli_result($result, 0, "role_id"));
        $this->coins            = Aresak::mysqli_result($result, 0, "coins");

        $this->initialized      = true;

        $this->SeenNow();                                                                   // Seen now!


        // todo Link twitch in Account->CreateFrom_CreationDate()
        // todo Link youtube in Account->CreateFrom_CreationDate()


        return true;
    }

    /**
     *  Register an account. Returns whether success or not.
     *  @param string $username The user's name used to login
     *  @param string $display_name The user's display name
     *  @param string $password The password - md5(password)
     *  @param string $email The email
     *  @param string $dob The date of birth
     *  @throws Exception
     *  @return boolean
     */
    public function Register(
        $username,
        $display_name,
        $password,
        $email,
        $dob
    ) {
        // Safe SQLi
        $username       = strtolower( mysqli_escape_string($this->sql, $username) );    // Prepare username
        $display_name   = mysqli_escape_string($this->sql, $display_name);              // Prepare display name
        $password       = mysqli_escape_string($this->sql, $password);                  // Prepare password that is in md5(pw)
        $email          = strtolower( mysqli_escape_string($this->sql, $email) );       // Prepare email
        $dob            = mysqli_escape_string($this->sql, $dob);                       // Prepare date of birth

        $pw_salt        = Aresak::GenerateString(25);
        $pw_salted      = md5($pw_salt . $password);                                    // Password md5(salt + md5(pw))

        $act_key        = Aresak::GenerateString(100);                                  // The activation key


        // Check if there is already this username registered
        $usernameCheck_query = "SELECT * FROM ares_accounts WHERE username='$username'";// The username check query
        $usernameCheck_res   = mysqli_query($this->sql, $usernameCheck_query)           // The username check result
            or die(mysqli_error($this->sql));                                           // The username check sql error handling

        if(mysqli_num_rows($usernameCheck_res) > 0) {
            // There is already this username registered
            throw new Exception("0 - Username is already registered", 0);
        }
        // The username is okay
        // Check if there is already this email registered
        $emailCheck_query    = "SELECT * FROM ares_accounts WHERE email='$email'";      // The email check query
        $emailCheck_result   = mysqli_query($this->sql, $emailCheck_query)              // The email check result
            or die(mysqli_error($this->sql));                                           // The email check sql error handling

        if(mysqli_num_rows($emailCheck_result) > 0) {
            // There is already this email registered
            throw new Exception("1 - Email is already registered", 1);
        }
        // The email is okay


        // The registration sql query
        $cd         = time();
        $reg_query = "INSERT INTO ares_accounts (username, display_name, password, password_salt, email, dob, created_at, avatar, activation_key) VALUES
                  ('$username', '$display_name', '$pw_salted', '$pw_salt', '$email', '$dob', '" . $cd . "',
                    'https://upload.wikimedia.org/wikipedia/commons/e/e4/Elliot_Grieveson.png', '$act_key')";
        $reg_result = mysqli_query($this->sql, $reg_query)                              // The registration result
            or die(mysqli_error($this->sql));                                           // The registration sql error handling


        $activation_mail = file_get_contents(                                           // Get the activation mail html
            _SITE_ . "/ares/mails/activation_mail.html");
        $key_base = base64_encode($username . "|" . $act_key);                          // Generate the base64

        $activation_mail = strtr($activation_mail, array(                               // Replace the parts in the mail
            "{name}" => $display_name,
            "{self}" => _SITE_,
            "{activation_base}" => $key_base
        ));

        $mail_result = mail(
            $email,                                                                     // Where to send the email
            "Activate your email - DragonsGetIt",                                       // Email subject
            $activation_mail,                                                           // Email body
            "From: noreply@dragonsgetit.com"                                            // Email headers
        );

        if(!$mail_result) {
            // The email didn't sent lol
            die("<b><span style='color: red;'>Failed to send the activation email!</span></b>");
        }

        $this->CreateFrom_CreationDate((int) $cd);

        return true;                                                                    // Registered successfully
    }

    /**
     *  Updates the last seen variable in the database to this time
     *  @return void
     */
    public function SeenNow() {
        $query          = "UPDATE ares_accounts SET seen_at='" . time() . "'
                        WHERE ID='" . $this->id . "'";                                  // Update query
        $result         = mysqli_query($this->sql, $query)                              // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling
    }

    /**
     *  Returns all users in the database
     *  @param mysqli $sql The MySQLi connection
     *  @since 1.1
     *  @return Account[] The array of all users
     */
    public static function GetAllMembers($sql) {
        $query  = "SELECT * FROM ares_accounts";
        $result = mysqli_query($sql, $query)
            or die(mysqli_error($sql));
        $users = array();
        for($i = 0; $i < mysqli_num_rows($result); $i ++) {
            $user                   = new Account($sql);
            $user->username         = Aresak::mysqli_result($result, $i, "username");
            $user->display_name     = Aresak::mysqli_result($result, $i, "display_name");
            $user->email            = Aresak::mysqli_result($result, $i, "email");
            $user->dob              = Aresak::mysqli_result($result, $i, "dob");
            $user->location         = Aresak::mysqli_result($result, $i, "location");
            $user->bio              = Aresak::mysqli_result($result, $i, "bio");
            $user->banner           = Aresak::mysqli_result($result, $i, "banner");
            $user->avatar           = Aresak::mysqli_result($result, $i, "avatar");
            $user->created_at       = Aresak::mysqli_result($result, $i, "created_at");
            $user->seen_at          = Aresak::mysqli_result($result, $i, "seen_at");
            $user->activation_key   = Aresak::mysqli_result($result, $i, "activation_key");
            $user->activated        = Aresak::mysqli_result($result, $i, "activated");
            $user->id               = Aresak::mysqli_result($result, $i, "ID");
            $user->password_salt    = Aresak::mysqli_result($result, $i, "password_salt");
            $user->password         = Aresak::mysqli_result($result, $i, "password");
            $user->role             = new Role($user->sql, Aresak::mysqli_result($result, $i, "role_id"));
            $user->coins            = Aresak::mysqli_result($result, $i, "coins");
            $user->initialized      = true;
            $users[$i]              = $user;
        }
        return $users;
    }
}

/**
 *  Used to hold Transaction information
 *  @version    1.0
 *  @author     Aresak
 */
class Transaction {
    /** @var mysqli The MySQLi connection to the transaction */
    private $sql;

    /** @var Account The associated account for this transaction */
    private $account = null;


    /** @var int The ID of the transaction */
    private $id;

    /** @var int The type of the transaction where the 0 is ADD and 1 is REMOVE */
    private $type;

    /** @var float The amount of coins in this transaction */
    private $amount;

    /** @var string The reason tag of this transaction */
    private $reason_tag;

    /** @var string The reason message of this transaction */
    private $reason_message;

    /** @var int The associated user's ID */
    private $user_id;

    /**
     *  Creates a raw transaction
     *  @param mysqli $sql The MySQLi connection to the database
     */
    public function __construct($sql) {
        $this->sql  = $sql;
    }

    /**
     *  Extracts the information for the given transaction by ID.
     *  @param int $id The ID of the transaction
     *  @return boolean Whether the transaction was found or not
     */
    public function CreateFrom_ID($id) {
        $query          = "SELECT * FROM ares_transactions WHERE ID='$id'";             // Get query
        $result         = mysqli_query($this->sql, $query)                              // Get result
            or die(mysqli_error($this->sql));                                           // MySQLi error handling

        if(mysqli_num_rows($result) == 0)
            return false;                                                               // Transaction not found

        $this->id       = $id;                                                          // Set the ID
        $this->type     = Aresak::mysqli_result($result, 0, "type");                    // Set the type
        $this->amount   = Aresak::mysqli_result($result, 0, "amount");                  // Set the amount
        $this->reason_tag   = Aresak::mysqli_result($result, 0, "reason_tag");          // Set the reason tag
        $this->reason_message   = Aresak::mysqli_result($result, 0, "reason_message");  // Set the reason message
        $this->user_id  = Aresak::mysqli_result($result, 0, "user_id");                 // Set the user id
        $account        = new Account($this->sql);                                      // Initiate account
        $account->CreateFrom_ID($this->user_id);                                        // Fetch the account from ID
        $this->account  = $account;                                                     // Set the account

        return true;                                                                    // Transaction found
    }

    /**
     *  Returns the ID of the transaction. Readonly
     */
    public function ID() {
        return $this->id;
    }

    /**
     *  Returns the type of the transaction. Readonly<br>
     *  <ul>
     *      <li>0 - ADD</li>
     *      <li>1 - REMOVE</li>
     *  </ul>
     */
    public function Type() {
        return $this->type;
    }

    /**
     *  Returns the amount of the coins in this transaction. Readonly
     */
    public function Amount() {
        return $this->amount;
    }

    /**
     *  Returns the associated user's ID. Readonly
     */
    public function User_ID() {
        return $this->user_id;
    }

    /**
     *  If there is a tag parameter present, set the reason tag, otherwise get reason tag
     *  @param string $tag The transaction reason's tag
     *  @return string|boolean <ul>
     *                              <li><b>string</b> - When the request is to get the reason tag</li>
     *                              <li><b>boolean</b> - When the request is to set the reason tag</li>
     *                         </ul>
     */
    public function Reason_Tag($tag = null) {
        if($tag == null) {
            return $this->reason_tag;                                   // The request was to get the reason tag
        }

        // The request was to set the reason tag
        $query          = "UPDATE ares_transactions SET reason_tag='$tag'
                          WHERE ID='" . $this->id . "'";                // Update query
        $result         = mysqli_query($this->sql, $query)              // Update result
            or die(mysqli_error($this->sql));                           // MySQLi error handling

        $this->reason_tag   = $tag;
        return true;
    }

    /**
     *  If there is a message parameter present, set the reason message, otherwise get reason message
     *  @param string $message The transaction reason message
     *  @return string|boolean <ul>
     *                              <li><b>string</b> - When the request is to get the reason message</li>
     *                              <li><b>boolean</b> - When the request is to set the reason message</li>
     *                         </ul>
     */
    public function Reason_Message($message = null) {
        if($message == null) {
            return $this->reason_message;                               // The request was to get the reason message
        }

        // The request was to set the reason message
        $query          = "UPDATE ares_transactions SET reason_message='$message'
                           WHERE ID='" . $this->id . "'";               // Update query
        $result         = mysqli_query($this->sql, $query)              // Update result
            or die(mysqli_error($this->sql));                           // MySQLi error handling

        $this->reason_message = $message;
        return true;
    }

    /**
     *  If there is an account parameter present, set the account, otherwise get the account
     *  @param Account $account The account
     *  @return Account|boolean <ul>
     *                              <li><b>Account</b> - When the request is to get the Account</li>
     *                              <li><b>boolean</b> - When the request is to set the Account</li>
     *                          </ul>
     */
    public function Account($account = null) {
        if($account == null) {
            return $this->account;                                      // The request was to get the Account
        }

        $this->account  = $account;                                     // The request was to set the Account
        return true;
    }

    /**
     *  Creates a new transaction
     *  @param mysqli $sql      The MySQLi connection to the database
     *  @param Account $account The account to operate with coins
     *  @param float $amount    The amount of coins to operate with
     *  @param string $reason_tag   The transaction reason tag
     *  @param string $reason_message   The transaction reason message
     *  @param boolean $type    The type of the transaction. FALSE is ADD and TRUE is REMOVE
     *  @return Transaction The created transaction
     */
    public static function Create_New($sql, $type, $account, $amount, $reason_tag, $reason_message) {
        $query          = "INSERT INTO ares_transactions (`type`, amount, reason_tag, reason_message, user_id) VALUES
      ('$type', '$amount', '$reason_tag', '$reason_message', '" . $account->ID() . "'); SELECT * FROM
        ares_transactions WHERE ID=Scope_Identity()";
        $result         = mysqli_query($sql, $query)
            or die(mysqli_error($sql));

        if($type) {
            $account->Coins( $account->Coins() - $amount );         // Remove coins
        } else {
            $account->Coins( $account->Coins() + $amount );         // Add coins
        }

        $transaction    = new Transaction($sql);
        $transaction->CreateFrom_ID(Aresak::mysqli_result($result, 0, "ID"));
        return $transaction;
    }

    /**
     *  If there is an account parameter present, get all transactions associated with that account,
     *  otherwise get all transactions of all users
     *  @param Account $account The account to get the transactions for
     *  @param mysqli  $sql     The MySQLi connection to the database
     *  @return Transaction[] All transactions either with the associated the account or without
     */
    public static function GetAllTransactions($sql, $account = null) {
        if($account == null) {
            // The request was to get all transactions in the database
            $query      = "SELECT * FROM ares_transactions";
        } else {
            // The request was to get all transactions for the account
            $query      = "SELECT * FROM ares_transactions WHERE user_id='" . $account->ID() . "'";
        }

        $result         = mysqli_query($sql, $query)                                                // Query result
            or die(mysqli_error($sql));                                                             // MySQLi error handling

        $transactions = array();                                                                    // Result array

        for($i = 0; $i < mysqli_num_rows($result); $i ++) {                                         // Iterate thru all
            $transaction = new Transaction($sql);                                                   // Raw transaction

            $transaction->id       = Aresak::mysqli_result($result, $i, "ID");                      // Set the ID
            $transaction->type     = Aresak::mysqli_result($result, $i, "type");                    // Set the type
            $transaction->amount   = Aresak::mysqli_result($result, $i, "amount");                  // Set the amount
            $transaction->reason_tag   = Aresak::mysqli_result($result, $i, "reason_tag");          // Set the reason tag
            $transaction->reason_message   = Aresak::mysqli_result($result, $i, "reason_message");  // Set the reason message
            $transaction->user_id  = Aresak::mysqli_result($result, $i, "user_id");                 // Set the user id
            if($account != null)
                $transaction->account  = $account;                                                  // Set the account

            array_push($transactions, $transaction);                                                // Push into array
        }

        return $transactions;                                                                       // Flush array of transactions
    }
}

/**
 *  Used to hold Bot Host information
 *  @version    1.0
 *  @author     Aresak
 */
class Bot_Host {
    /** @var float The bot application supported version */
    private $appVersionSupport = 5.1;

    /** @var mysqli The MySQLi connection to the database */
    private $sql;

    /** @var boolean Is the object initialized */
    private $initialized;


    /** @var int The ID of the host session */
    private $id;

    /** @var int The ID of the user */
    private $user_id;

    /** @var string The current API key */
    private $key;

    /** @var string The previous API key */
    private $key_old;

    /** @var int When the session started (UNIX) */
    private $session_start;

    /** @var int When the session was last checked (UNIX) */
    private $session_check;

    /** @var string The version of the bot application */
    private $version;

    /** @var boolean Is this host the current active */
    private $active;

    /** @var float How much coins earned users from this host during session */
    private $session_earned_users;

    /** @var float How much coins earned the host during the session */
    private $session_earned_self;

    /** @var int When the session API key expires at (UNIX) */
    private $key_expire;


    /** @var Account The host's Account object link */
    private $account;


    /**
     *  Creates a new raw host object
     *  @param mysqli $sql The MySQLi connection to the database
     */
    public function __construct($sql) {
        $this->sql  = $sql;
    }


    /**
     *  Get the ID of the host session
     *  @return int ID of the host session
     */
    public function ID() {
        return $this->id;
    }

    /**
     *  If the key parameter is present, set the key, otherwise get the key
     *  @param string $key  They session API key
     *  @return string|boolean  <ul>
     *                              <li><b>string</b> - When the request is to GET</li>
     *                              <li><b>boolean</b> - When the request is to SET</li>
     *                          </ul>
     */
    public function Key($key = null) {
        if($key == null) {
            return $this->key;                                                          // The request was to get the key
        }

        // The request was to set the key
        $query      = "UPDATE ares_bot_hosts SET `key`='$key'
                      WHERE ID='" . $this->id . "'";                                    // Update query
        $result     = mysqli_query($this->sql, $query)                                  // Update result
            or die(mysqli_error($this->sql));                                           // MySQLi error handling

        $this->key  = $key;
        return true;
    }

    /**
     *  If the key parameter is present, set the previous key, otherwise get the previous key
     *  @param string $key  The previous session API key
     *  @return string|boolean  <ul>
     *                              <li><b><string/b> - When the request is to GET</li>
     *                              <li><b>boolean</b> - When the request is to SET</li>
     *                          </ul>
     */
    public function Key_Old($key = null) {
        if($key == null) {
            return $this->key_old;                                      // The request was to get the old key
        }
        // The request was to set the old key

        $query      = "UPDATE ares_bot_hosts SET key_old='$key'
                      WHERE ID='" . $this->id . "'";                    // Update query
        $result     = mysqli_query($this->sql, $query)                  // Update result
            or die(mysqli_error($this->sql));                           // MySQLi error handling

        $this->key_old  = $key;
        return true;
    }

    /**
     *  If the time parameter is present, set the session start time, otherwise get the session start time
     *  @param int $time    The session start time in UNIX
     *  @return int|boolean     <ul>
     *                              <li><b>int</b> - When the request is to GET</li>
     *                              <li><b>boolean</b> - When the request is to SET</li>
     *                          </ul>
     */
    public function Session_Start($time = null) {
        if($time == null) {
            return $this->session_start;                                // The request was to get the start time
        }
        // The request was to set the start time

        $query      = "UPDATE ares_bot_hosts SET session_start='$time'
                      WHERE ID='" . $this->id . "'";                    // Update query
        $result     = mysqli_query($this->sql, $query)                  // Update result
            or die(mysqli_error($this->sql));                           // MySQLi error handling

        $this->session_start    = $time;
        return true;
    }

    /**
     *  If the time parameter is present, set the session check time, otherwise get the session check time
     *  @param int $time    The session check time in UNIX
     *  @return int|boolean     <ul>
     *                              <li><b>int</b> - When the request is to GET</li>
     *                              <li><b>boolean</b> - When the request is to SET</li>
     *                          </ul>
     */
    public function Session_Check($time = null) {
        if($time == null) {
            return $this->session_check;                                // The request was to get the last check time
        }
        // The request was to set the last check time

        $query      = "UPDATE ares_bot_hosts SET session_check='$time'
                      WHERE ID='" . $this->id . "'";                    // Update query
        $result     = mysqli_query($this->sql, $query)                  // Update result
            or die(mysqli_error($this->sql));                           // MySQLi error handling

        $this->session_check    = $time;
        return true;
    }

    /**
     *  If the version parameter is present, set the bot version, otherwise get the bot version
     *  @param string $version  The version of the bot application
     *  @return string|boolean  <ul>
     *                              <li><b>string</b> - When the request is to GET</li>
     *                              <li><b>boolean</b> - When the request is to SET</li>
     *                          </ul>
     */
    public function Version($version = null) {
        if($version == null) {
            return $this->version;                                      // The request was to get the version
        }
        // The request was to set the version

        $query      = "UPDATE ares_bot_hosts SET `version`='$version'
                      WHERE ID='" . $this->id . "'";                    // Update query
        $result     = mysqli_query($this->sql, $query)                  // Update result
            or die(mysqli_error($this->sql));                           // MySQLi error handling

        $this->version  = $version;
        return true;
    }

    /**
     *  If the activate parameter is present, set whether the host is active, otherwise get whether the host is active
     *  @param boolean $active  Whether the host is the one who is processing the chat at the moment
     *  @return boolean         <ul>
     *                              <li><b>boolean</b> - Whether the host is processing the chat ATM.</li>
     *                          </ul>
     */
    public function Active($active = null) {
        if($active == null) {
            return $this->active;                                       // The request was to get whether the host is active
        }
        // Set whether the host is active processor

        $query      = "UPDATE ares_bot_hosts SET active='$active'
                      WHERE ID='" . $this->id . "'";                    // Update query
        $result     = mysqli_query($this->sql, $query)                  // Update result
            or die(mysqli_error($this->sql));                           // MySQLi error handling

        $this->active   = $active;
        return true;
    }

    /**
     *  If the earnings parameter is present, set the earnings, otherwise get the earnings of the users during
     *  this session by this host
     *  @param float $earnings  The amount of coins users earned this session by this host
     *  @return float|boolean   <ul>
     *                              <li><b>float</b> - When the request is to GET</li>
     *                              <li><b>boolean</b> - When the request is to SET</li>
     *                          </ul>
     */
    public function Session_Earned_Users($earnings = null) {
        if($earnings == null) {
            return $this->session_earned_users;                             // The request was to get the users' earnings
        }
        // The request was to set users' earnings

        $query      = "UPDATE ares_bot_hosts SET session_earned_users='$earnings'
                      WHERE ID='" . $this->id . "'";                        // Update query
        $result     = mysqli_query($this->sql, $query)                      // Update result
            or die(mysqli_error($this->sql));                               // MySQLi error handling

        $this->session_earned_users = $earnings;
        return true;
    }

    /**
     *  If the earnings parameter is present, set the earnings, otherwise get the earnings of the host
     *  during this session
     *  @param float $earnings  The ammount of coins the host earned by hosting this session
     *  @return float|boolean   <ul>
     *                              <li><b>float</b> - When the request is to GET</li>
     *                              <li><b>boolean</b> - When the request is to SET</li>
     *                          </ul>
     */
    public function Session_Earned_Self($earnings = null) {
        if($earnings == null) {
            return $this->session_earned_self;                              // The request was to get the self earning
        }
        // The request was to set the self earnings

        $query      = "UPDATE ares_bot_hosts SET session_earned_self='$earnings'
                      WHERE ID='" . $this->id . "'";                        // Update query
        $result     = mysqli_query($this->sql, $query)                      // Update result
            or die(mysqli_error($this->sql));                               // MySQLi error handling

        $this->session_earned_self  = $earnings;
        return true;
    }

    /**
     *  If the time parameter is present, set the time of the API key expiration, otherwise get the expiration time (UNIX)
     *  @param int $time        The time when the session API key expires at in UNIX
     *  @return int|boolean     <ul>
     *                              <li><b>int</b> - When the request is to GET</li>
     *                              <li><b><boolean/b> - When the request is to SET</li>
     *                          </ul>
     */
    public function Key_Expire($time = null) {
        if($time == null) {
            return $this->key_expire;                                           // The request was to get the expiration time
        }
        // The request was to set the expiration time

        $query      = "UPDATE ares_bot_hosts SET key_expire='$time'
                      WHERE ID='" . $this->id . "'";                            // Update query
        $result     = mysqli_query($this->sql, $query)                          // Update result
            or die(mysqli_error($this->sql));                                   // MySQLi error handling

        $this->key_expire   = $time;
        return true;
    }

    /**
     *  Is this object initialized
     *  @return boolean Whether is this object initialized
     */
    public function Initialized() {
        return $this->initialized;
    }

    /**
     *  Get the host's Account
     *  @return Account The host's Account
     */
    public function Account() {
        if($this->account == null) {                                                            // The account is null
            $acc = new Account($this->sql);                                                     // Create raw account object
            $acc->CreateFrom_ID($this->user_id);                                                // Create from ID
            $this->account = $acc;                                                              // Set the account
        }
        return $this->account;
    }

    /**
     *  Extracts the info about the host by the ID
     *  @param int $id The host session ID
     *  @return boolean Whether the host was found
     */
    public function CreateFrom_ID($id) {
        $query      = "SELECT * FROM ares_bot_hosts WHERE ID='$id'";                            // Get query
        $result     = mysqli_query($this->sql, $query)                                          // Get result
            or die(mysqli_error($this->sql));                                                   // MySQLi error handling

        if(mysqli_num_rows($result) == 0)
            return false;                                                                       // Session not found


        $this->id                   = $id;                                                      // Set the ID
        $this->user_id              = Aresak::mysqli_result($result, 0, "user_id");             // Set the User ID
        $this->key                  = Aresak::mysqli_result($result, 0, "key");                 // Set the key
        $this->key_old              = Aresak::mysqli_result($result, 0, "key_old");             // Set the previous key
        $this->session_start        = Aresak::mysqli_result($result, 0, "session_start");       // Set the session start time
        $this->session_check        = Aresak::mysqli_result($result, 0, "session_check");       // Set the session check time
        $this->version              = Aresak::mysqli_result($result, 0, "version");             // Set the version
        $this->active               = Aresak::mysqli_result($result, 0, "active");              // Set whether active
        $this->session_earned_users = Aresak::mysqli_result($result, 0, "session_earned_users");// Set user earnings
        $this->session_earned_self  = Aresak::mysqli_result($result, 0, "session_earned_self"); // Set self earnings
        $this->key_expire           = Aresak::mysqli_result($result, 0, "key_expire");          // Set key expiration time
        $this->initialized          = true;                                                     // Set as initialized

        return true;                                                                            // Session found
    }

    /**
     *  Extracts the info about the host from the Key
     *  @param string $key The session API key
     *  @return boolean Whether the host was found
     */
    public function CreateFrom_Key($key) {
        $query      = "SELECT * FROM ares_bot_hosts WHERE `key`='$key'";                        // Get query
        $result     = mysqli_query($this->sql, $query)                                          // Get result
            or die(mysqli_error($this->sql));                                                   // MySQLi error handling

        if(mysqli_num_rows($result) == 0)
            return false;                                                                       // Session not found


        $this->id                   = Aresak::mysqli_result($result, 0, "ID");                  // Set the ID
        $this->user_id              = Aresak::mysqli_result($result, 0, "user_id");             // Set the User ID
        $this->key                  = Aresak::mysqli_result($result, 0, "key");                 // Set the key
        $this->key_old              = Aresak::mysqli_result($result, 0, "key_old");             // Set the previous key
        $this->session_start        = Aresak::mysqli_result($result, 0, "session_start");       // Set the session start time
        $this->session_check        = Aresak::mysqli_result($result, 0, "session_check");       // Set the session check time
        $this->version              = Aresak::mysqli_result($result, 0, "version");             // Set the version
        $this->active               = Aresak::mysqli_result($result, 0, "active");              // Set whether active
        $this->session_earned_users = Aresak::mysqli_result($result, 0, "session_earned_users");// Set user earnings
        $this->session_earned_self  = Aresak::mysqli_result($result, 0, "session_earned_self"); // Set self earnings
        $this->key_expire           = Aresak::mysqli_result($result, 0, "key_expire");          // Set key expiration time
        $this->initialized          = true;                                                     // Set as initialized

        return true;                                                                            // Session found
    }

    /**
     *  Extracts the info about the host from Account
     *  @param Account $account The hosting account
     *  @return boolean Whether the host was found
     */
    public function CreateFrom_Account($account) {
        $query      = "SELECT * FROM ares_bot_hosts WHERE user_id='" . $account->ID() . "'";    // Get query
        $result     = mysqli_query($this->sql, $query)                                          // Get result
            or die(mysqli_error($this->sql));                                                   // MySQLi error handling

        if(mysqli_num_rows($result) == 0)
            return false;                                                                       // Session not found


        $this->id                   = Aresak::mysqli_result($result, 0, "ID");                  // Set the ID
        $this->user_id              = Aresak::mysqli_result($result, 0, "user_id");             // Set the User ID
        $this->key                  = Aresak::mysqli_result($result, 0, "key");                 // Set the key
        $this->key_old              = Aresak::mysqli_result($result, 0, "key_old");             // Set the previous key
        $this->session_start        = Aresak::mysqli_result($result, 0, "session_start");       // Set the session start time
        $this->session_check        = Aresak::mysqli_result($result, 0, "session_check");       // Set the session check time
        $this->version              = Aresak::mysqli_result($result, 0, "version");             // Set the version
        $this->active               = Aresak::mysqli_result($result, 0, "active");              // Set whether active
        $this->session_earned_users = Aresak::mysqli_result($result, 0, "session_earned_users");// Set user earnings
        $this->session_earned_self  = Aresak::mysqli_result($result, 0, "session_earned_self"); // Set self earnings
        $this->key_expire           = Aresak::mysqli_result($result, 0, "key_expire");          // Set key expiration time
        $this->initialized          = true;                                                     // Set as initialized
        $this->account              = $account;                                                 // Set the account

        return true;                                                                            // Session found
    }

    /**
     *  Deletes this host session in the database
     *  @return boolean Whether the session has been deleted successfully
     */
    public function Delete() {
        $query      = "DELETE FROM ares_bot_hosts WHERE ID='" . $this->id . "'";                // Delete query
        $result     = mysqli_query($this->sql, $query)                                          // Delete result
            or die(mysqli_error($this->sql));                                                   // MySQLi error handling
    }

    /**
     *  Creates a new host session.<br>
     *  <b>Error Codes:</b>
     *  <ul>
     *      <li><b>0</b> - No error has occurred.</li>
     *      <li><b>10</b> - Account is already hosting a session. Every user can host max of 1 session.</li>
     *      <li><b>20</b> - Version of the bot is outdated.</li>
     *  </ul>
     *  @param Account $account The hosting account
     *  @param string $version  The bot version
     *  @return array[] Array() {<ul>
     *                      <li>["error"] = If empty, no error has occurred</li>
     *                      <li>["error-code"] = If <b>0</b>, no error has occurred, otherwise an error code</li>
     *                      <li>["key"] = The session API key generated</li>
     *                      <li>["key-expire"] = When the session API key expires</li>
     *                      <li>["active"] = Whether go active instantly or not</li>
     *                  <ul>
     *                  }
     */
    public function CreateNew($account, $version) {
        $output = array();
        // Check whether the user is already hosting...
        if($this->CreateFrom_Account($account)) {
            // The user is already hosting
            $output["error"]        = "Account is already hosting a session. Every user can host max of 1 session.";
            $output["error-code"]   = 10;
        } else {
            // The user is not hosting already
            // Check whether the user is using supported version...
            if($version != $this->appVersionSupport) {
                // The user is using outdated version
                $output["error"]        = "Version of the bot is outdated.";
                $output["error-code"]   = 20;
            } else {
                // The user is using up-to-date version
                // Check if do we have active processor
                $checkQuery = "SELECT * FROM ares_bot_hosts WHERE active='1'";          // Check query
                $checkResult = mysqli_query($this->sql, $checkQuery)                    // Check result
                    or die(mysqli_error($this->sql));                                   // MySQLi error handling

                $active     = (mysqli_num_rows($checkResult) == 0 ? 1 : 0);             // Become an processor instantly?
                $key        = Aresak::GenerateString(20);                               // Generate API key
                $keyExpire  = time() + (60*60);                                         // API Key expiration time

                // Create the host object

                $query      = "INSERT INTO ares_bot_hosts (user_id, `key`, session_start,
                              session_check, version, active, key_expire) VALUES ('" . $account->ID() . "',
                              '$key', '" . time() . "', '" . time() . "', '$version', '$active',
                              '$keyExpire')";                                           // Insert query
                $result     = mysqli_query($this->sql, $query)                          // Insert result
                    or die(mysqli_error($this->sql));                                   // MySQLi error handling

                $output["key"]          = $key;                                         // Set the key
                $output["key-expire"]   = $keyExpire;                                   // Set the key expiration
                $output["active"]       = $active;                                      // Set whether processor
                $output["current"]      = time();                                       // Set current time
                $this->CreateFrom_Account($account);                                    // Initiate this object
            }
        }

        return $output;
    }

    /**
     *  Get all current hosts
     *  @param  mysqli $sql The MySQLi connection to the database
     *  @return Bot_Host[] The array of all current hosts
     */
    public static function GetAllHosts($sql) {
        $query      = "SELECT * FROM ares_bot_hosts";                                               // Get query
        $result     = mysqli_query($sql, $query)                                                    // Get result
            or die(mysqli_error($sql));                                                             // MySQLi error handling

        $hosts = array();                                                                           // Empty hosts array

        for($i = 0; $i < mysqli_num_rows($result); $i ++) {
            $host = new Bot_Host($sql);
            $host->id                   = Aresak::mysqli_result($result, 0, "ID");                  // Set the ID
            $host->user_id              = Aresak::mysqli_result($result, 0, "user_id");             // Set the User ID
            $host->key                  = Aresak::mysqli_result($result, 0, "key");                 // Set the key
            $host->key_old              = Aresak::mysqli_result($result, 0, "key_old");             // Set the previous key
            $host->session_start        = Aresak::mysqli_result($result, 0, "session_start");       // Set the session start time
            $host->session_check        = Aresak::mysqli_result($result, 0, "session_check");       // Set the session check time
            $host->version              = Aresak::mysqli_result($result, 0, "version");             // Set the version
            $host->active               = Aresak::mysqli_result($result, 0, "active");              // Set whether active
            $host->session_earned_users = Aresak::mysqli_result($result, 0, "session_earned_users");// Set user earnings
            $host->session_earned_self  = Aresak::mysqli_result($result, 0, "session_earned_self"); // Set self earnings
            $host->key_expire           = Aresak::mysqli_result($result, 0, "key_expire");          // Set key expiration time
            $host->initialized          = true;                                                     // Set as initialized
            $hosts[$i]                  = $host;                                                    // Push host into the array
        }

        return $hosts;                                                                              // Returns hosts array
    }
}

/**
 * Used to hold Twitch account information
 * @version     1.1
 * @author      Aresak
 */
class Twitch
{
    /** @var int The link ID */
    private $id;
    /** @var Account The connected account */
    private $account;
    /** @var string The authorized token */
    private $token;
    /** @var int The Twitch ID */
    private $_id;
    /** @var string The bio on Twitch */
    private $bio;
    /** @var string When the user registered on Twitch */
    private $created_at;
    /** @var string The display name on Twitch */
    private $display_name;
    /** @var string The email linked to Twitch account */
    private $email;
    /** @var boolean Is the email linked to Twitch verified? */
    private $email_verified;
    /** @var string The URL of Twitch avatar */
    private $logo;
    /** @var string The safe lowered name of the account */
    private $name;
    /** @var boolean Is the account partnered to Twitch? */
    private $partnered;
    /** @var boolean Is twitter connected to this account? */
    private $twitter_connected;
    /** @var string The type of account on Twitch */
    private $type;
    /** @var string When the account was last updated at on Twitch */
    private $updated_at;
    /** @var boolean Is this object initialized */
    private $initialized;
    /** @var mysqli The SQL connection */
    private $sql;

    /** @var string The API's redirect URL used after OAuth */
    private static $_redirectUri        = "";
    /** @var string The API's Client ID */
    private static $_clientID           = "";
    /** @var string The API's Client Secret code */
    private static $_clientSecret       = "";



    public $raw_tokens;
    public $extras;

    /**
     *  Create a Twitch object
     *  @param mysqli $sql The SQL connection
     */
    public function __construct($sql = null)
    {
        $this->sql  = $sql;
    }

    /**
     *  Returns the API's redirect URL after OAuth
     *  @return string
     */
    public static function RedirectURI() {
        return Twitch::$_redirectUri;
    }

    /**
     *  Returns the API's Client ID
     *  @return string
     */
    public static function ClientID() {
        return Twitch::$_clientID;
    }

    /**
     *  Returns the API's Client Secret
     *  @return string
     */
    public static function ClientSecret() {
        return Twitch::$_clientSecret;
    }

    /**
     *  Return the ID of the link
     *  @return int
     */
    public function ID() {
        return $this->id;
    }

    /**
     *  If there is not parameter, returns the Account otherwise set the Account
     *  @param Account $account The user's Account
     *  @return mixed
     */
    public function Account($account = null) {
        if($account == null) {
            // The request was to return the account
            return $this->account;
        }

        // The request was to set the account
        // Check if the account param is an Account object
        if(!$account instanceof Account) {
            // The account is not an Account object
            ob_start();
            var_dump($account);
            die("<b><span style='color: red;'>Failed to call Twitch->Account() as the account argument is not an Account object!</span></b><br>" . ob_get_clean());
        }

        // Check if the account is initialized
        if(!$account->Initialized()) {
            die("<b><span style='color: red;'>Failed to call Twitch->Account() the account object has not been initialized!</span></b><br>" . ob_get_clean());
        }

        $this->account = $account;  // Set the account
        return true;
    }

    /**
     *  Returns the token for this Twitch object
     *  @return string
     */
    public function Token() {
        return $this->token;
    }

    /**
     *  Returns the Twitch ID
     *  @return int
     */
    public function _ID() {
        return $this->_id;
    }

    /**
     *  Returns the bio on Twitch
     *  @return string
     */
    public function Bio() {
        return $this->bio;
    }

    /**
     *  When the account was created on Twitch
     *  @return string
     */
    public function Created_At() {
        return $this->created_at;
    }

    /**
     *  The display name on Twitch
     *  @return string
     */
    public function Display_Name() {
        return $this->display_name;
    }

    /**
     *  The linked email to Twitch account
     *  @return string
     */
    public function Email() {
        return $this->email;
    }

    /**
     *  Is the linked email verified?
     *  @return boolean
     */
    public function Email_Verified() {
        return $this->email_verified;
    }

    /**
     *  Get the URL of logo on Twitch account
     */
    public function Logo() {
        return $this->logo;
    }

    /**
     *  Returns the safe lowercased name on Twitch (used for login too)
     */
    public function Name() {
        return $this->name;
    }

    /**
     *  Is the account on Twitch partnered?
     *  @return boolean
     */
    public function Partnered() {
        return $this->partnered;
    }

    /**
     *  Has this account connected Twitter account?
     *  @return boolean
     */
    public function Twitter_Connected() {
        return $this->twitter_connected;
    }

    /**
     *  Get the type of Account on Twitch
     *  @return string
     */
    public function Type() {
        return $this->type;
    }

    /**
     *  When was this account last updated at
     *  @return string
     */
    public function Updated_At() {
        return $this->updated_at;
    }

    /**
     *  Is this object initialized?
     *  @return boolean
     */
    public function Initialized() {
        return $this->initialized;
    }

    /**
     *  Create the object from the given token
     *  @param string $token The user token
     *  @return boolean
     */
    public function CreateFrom_Token($token) {
        $this->token    = $token;

        $result         = $this->_get("kraken/user");
        if($result == false)
            return false;


        $j              = json_decode($result, true);

        if(!isset($j["display_name"]))
            return false;

        $this->display_name     = $j["display_name"];
        $this->_id              = $j["_id"];
        $this->name             = $j["name"];
        $this->type             = $j["type"];
        $this->bio              = $j["bio"];
        $this->created_at       = $j["created_at"];
        $this->updated_at       = $j["updated_at"];
        $this->logo             = $j["logo"];
        $this->email            = $j["email"];
        $this->partnered        = $j["partnered"];

        $this->initialized      = true;

        return true;
    }

    /**
     *  Create the object from Account. If the token is expired, it doesn't fetch the new data
     *  @param Account $account The given account
     *  @return boolean
     */
    public function CreateFrom_Account($account) {
        if(!$account->Initialized()) {
            die("<b><span style='color: red;'>Error: Account is not initialized</span></b>");
        }

        $query      = "SELECT * FROM ares_links WHERE service='" . Service_Twitch . "' AND user_id='" . $account->ID() . "'";
        $result     = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        if(mysqli_num_rows($result) == 0) {
            return false;
        }
        $this->extras   = Aresak::mysqli_result($result, 0, "extras");
        $this->CreateFrom_Token(Aresak::mysqli_result($result, 0, "token"));
        return true;
    }

    /**
     *  Generates a Twitch authentication link with given scopes
     *  @param string[] $scopes All scopes, the user has to agree with
     *  @param Account $account The account this links to
     *  @return string
     */
    public function GenerateAuthLink($scopes, $account = null) {
        if($account != null) {
            if($account->Initialized()) {
                $state      = base64_encode($account->Created_At());
                $state      = "$" . substr($state, 0, (strlen($state) - 2)) . "$";
            } else $state = "null";
        } else {
            $state = "null";
        }

        return "https://api.twitch.tv/kraken/oauth2/authorize"      // Base link
            . "?client_id=" . self::ClientID()                    // Client ID
            . "&redirect_uri=" . self::RedirectURI()              // Redirect URL
            . "&response_type=code"                               // Response type
            . "&state=$state"                                     // State (linking account or not)
            . "&scope=" . join("+", $scopes);                     // Required scopes
    }

    /**
     *  Generates a new token from a code
     *  @param string $code The code obtained from Twitch Auth page
     *  @return void
     */
    public function GenerateToken($code) {
        $res = self::Post("api/oauth2/token", array(                                        // Set the POST Fields
            "client_id"     => Twitch::ClientID(),                                          // Twitch API Client ID
            "client_secret" => Twitch::ClientSecret(),                                      // Twitch API Client Secret
            "grant_type"    => "authorization_code",                                        // Callback Grant Type
            "redirect_uri"  => Twitch::RedirectURI(),                                       // Twitch API Redirect URI
            "code"          => $code                                                        // The obtained code
        ));

        $j = json_decode($res, true);
        $this->token        = $j["access_token"];
        $this->raw_tokens   = $j;
    }

    /**
     *  Sends an GET request to Twitch with client id and oauth attached
     *  @param string $endpoint The API endpoint
     *  @param string[] $params The extra parameters passing into the endpoint
     *  @return string
     */
    public function _get($endpoint, $params = null) {
        return @file_get_contents("https://api.twitch.tv/" . $endpoint . "?client_id=" . self::ClientID() . "&oauth_token=" . $this->token
                    . ($params != null ? "&" . join("&", $params) : ""));
    }

    /**
     *  Sends an POST request to Twitch with client id and oauth attached
     *  @param string $endpoint The API endpoint
     *  @param string[] $fields The extra parameters passing into the endpoint
     *  @return string
     */
    public function _post($endpoint, $fields) {
        $url = "https://api.twitch.tv/" . $endpoint;
        $context = stream_context_create(array(
            "http" => array(
                "header"    =>  "Content-type: application/x-www-form-urlencoded\r\n"
                              . "Client-ID: " . $this->ClientID() . "\r\nAccept: application/vnd.twitchtv.v2+json\r\nAuthorization: OAuth " . $this->token,
                "method"    =>  "POST",
                "content"   =>  http_build_query($fields)
            )
        ));
        return @file_get_contents($url, false, $context);
    }

    /**
     *  Sends an GET request to Twitch
     *  @param string $endpoint The API endpoint
     *  @return string
     */
    public static function Get($endpoint) {
        return @file_get_contents("https://api.twitch.tv/" . $endpoint);
    }

    /**
     *  Sends an POST request to Twitch
     *  @param string $endpoint The API endpoint
     *  @param string[] $fields The extra parameters passing into the endpoint
     *  @return string
     */
    public static function Post($endpoint, $fields) {
        $url = "https://api.twitch.tv/" . $endpoint;
        $context = stream_context_create(array(
            "http" => array(
                "header"    =>  "Content-type: application/x-www-form-urlencoded\r\nAccept: application/vnd.twitchtv.v2+json\r\n",
                "method"    =>  "POST",
                "content"   =>  http_build_query($fields)
            )
        ));

        return @file_get_contents($url, false, $context);
    }
}

/**
 * Used to hold YouTube account information
 * @version     1.2
 * @author      Aresak
 */
class YouTube
{
    /** @var string The Google API Key */
    private static $API_Key         = "";
    /** @var string The Google API Client ID */
    private static $Client_ID       = "";
    /** @var string The Google API Client Secret */
    private static $Client_Secret   = "";
    /** @var string The Google API Redirect URL */
    private static $Redirect_URL    = "";
    // todo when pushing into the DGI website, change this Redirect URL

    /** @var Google_Client Google Client */
    private $client                 = null;
    /** @var Google_Service_Oauth2 Google OAuth 2 Service */
    private $oauth                  = null;
    /** @var boolean Is the YouTube object initialized */
    private $initialized            = false;
    /** @var string Google API Token */
    private $token;
    /** @var mysqli MySQLi Connection */
    private $sql;
    /** @var int DB Link ID */
    private $link_id;
    /** @var Account Linked user's account */
    private $account;
    public $extras;

    /**
     *  Create a YouTube object
     *  @param mysqli $sql MySQLi connection
     */
    public function __construct($sql = null)
    {
        $this->sql      = $sql;
        if(!class_exists("Google_Client")) {
            die("Error: Google API library not included");
        }
    }

    /**
     *  Creates a Google Client
     *  @return void
     */
    public function Create_Client() {
        $this->client       = new Google_Client();
        $this->client->setApplicationName("Dragons Get It");
        $this->client->setClientId( self::$Client_ID );
        $this->client->setClientSecret( self::$Client_Secret );
        $this->client->setRedirectUri( self::$Redirect_URL );

        $this->client->addScope( array(Google_Service_YouTube::YOUTUBE_READONLY, Google_Service_Plus::USERINFO_PROFILE, Google_Service_Plus::USERINFO_EMAIL) );
    }

    /**
     *  Creates a Google OAuth Service
     *  @return void
     */
    public function Create_OAuth_Service() {
        $this->oauth        = new Google_Service_Oauth2( $this->client );
        $this->initialized  = true;
    }

    /**
     *  Returns a Google Client
     *  @return Google_Client
     */
    public function Client() {
        return $this->client;
    }

    /**
     *  Returns a Google OAuth Service
     *  @return Google_Oauth2Service
     */
    public function OAuth() {
        return $this->oauth;
    }

    /**
     *  Returns whether this object is initialized or not
     *  @return boolean
     */
    public function Initialized() {
        return $this->initialized;
    }

    /** If empty argument, returns the token otherwise sets the token
     *  @param string $token The Google API User token
     *  @return string|boolean
     */
    public function Token($token = null) {
        if($token   == null) {
            // The request was to return the token
            return $this->token;
        }

        // The request was to set the token
        $this->token    = $token;
        if($this->account == null)
            return true;

        $query          = "UPDATE ares_links SET token='$token'
                          WHERE user_id='" . $this->account->ID() . "' AND service='" . Service_YouTube . "'";
        $result         = mysqli_query($this->sql, $query)          // Update result
            or die(mysqli_error($this->sql));                       // SQLi error handling
        return true;
    }

    /**
     *  If empty argument, returns the Account otherwise sets the token
     *  @param Account $account The user's Account
     *  @return Account|boolean
     */
    public function Account($account = null) {
        if($account == null) {
            // The request was to return the Account
            return $this->account;
        }

        // The request was to set the account
        $this->account      = $account;     // Set the account
    }

    /**
     *  Links this YouTube object to an Account
     *  @param Account $account The user's Account
     *  @return boolean
     */
    public function Create_Link($account) {
        if(!$this->initialized)         // Check if the YouTube object is initialized
            die("Error: YouTube object is not initialized, cannot create a link");

        if(!$account->Initialized())    // Check if the Account object is initialized
            die("Error: Account object is not initialized, cannot create a link");


        // Get the user data from Google
        $data       = $this->OAuth()->userinfo->get();

        $query2     = "SELECT * FROM ares_links WHERE user_id='" . $account->ID() . "'
                    AND service='" . Service_YouTube . "'";                     // Check query
        $result2    = mysqli_query($this->sql, $query2)                         // Check result
            or die(mysqli_error($this->sql));                                   // SQLi error handling

        if(mysqli_num_rows($result2) != 0) {
            return false;
        }

        $yt         = new Google_Service_YouTube($this->client);
        $ytr        = $yt->channels->listChannels("brandingSettings", array("mine" => true));
        /** @var $ytc Google_Service_YouTube_Channel channel */
        $ytc        = $ytr['items'][0];

        $ext        = $data->getName() . "||" . $data->getPicture() . "||" . $ytc->getId();

        // Insert Query
        $query      = "INSERT INTO ares_links (user_id, service, token, twitch_id, extras) VALUES
        ('" . $account->ID() . "', '" . Service_YouTube . "', '" . $this->Token() . "', '" . $data->getId() . "', '$ext')";
        $result     = mysqli_query($this->sql, $query)                          // Insert result
            or die(mysqli_error($this->sql));                                   // SQLi error handling

        $result3    = mysqli_query($this->sql, $query2)                         // Redo the check query
            or die(mysqli_error($this->sql));                                   // SQLi error handling

        $this->link_id  = Aresak::mysqli_result($result3, 0, "ID");             // Set the link ID
        return true;
    }

    /**
     *  Creates an YouTube object from the Account
     *  @param Account $account The user's Account
     *  @return boolean
     */
    public function CreateFrom_Account($account) {
        $query          = "SELECT * FROM ares_links WHERE service='" . Service_YouTube . "' AND user_id='" . $account->ID() . "'";
        $result         = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        if(mysqli_num_rows($result) == 0)
            return false;

        $this->token    = Aresak::mysqli_result($result, 0, "token");
        $this->extras   = Aresak::mysqli_result($result, 0, "extras");
        $this->link_id  = Aresak::mysqli_result($result, 0, "ID");

        $this->Create_Client();
        $this->Create_OAuth_Service();

        $this->client->setAccessToken($this->token);
        return true;
    }
}

/**
 *  Module for fetching playlists
 */
class AresakYouTubeModule {
    /** @var $key string A Google API key */
    public $key  = "";
    /** @var $base string A base url for the Google API */
    public $base    = "https://www.googleapis.com/youtube/v3/";
    /** @var $sql mysqli A MySQLi connection */
    public $sql;


    function __construct($sql) {
        $this->sql  = $sql;
    }

    public function GetPlaylist($position) {
        $query = "SELECT * FROM atm_ytview_playlists WHERE ID='$position'";
        $result = mysqli_query($this->sql, $query)
            or die(mysqli_error($this->sql));

        if(mysqli_num_rows($result) == 1) {
            $playlist = new YTPlaylist(
                $this,
                Aresak::mysqli_result($result, 0, "playlist_id"),
                Aresak::mysqli_result($result, 0, "user_id"),
                Aresak::mysqli_result($result, 0, "locked")
            );

            return $playlist;
        } else {
            return null;
        }
    }
}

/**
 *  Class which holds an playlist info for the YouTube module
 */
class YTPlaylist {
    /** @var $aytm AresakYouTubeModule Module parent */
    public $aytm;
    /** @var $videos YTVideo[] The array of Video objects */
    public $videos = array();
    public $playlistId, $userId, $locked;

    /**
     *  @param AresakYouTubeModule $aytm The parent module
     *  @param String $playlistId The playlist ID
     *  @param String $userId The user ID
     *  @param boolean $locked Whether the playlist is locked or not
     */
    function __construct($aytm, $playlistId, $userId, $locked)
    {
        $this->aytm = $aytm;
        $this->playlistId = $playlistId;
        $this->userId = $userId;
        $this->locked = $locked;
    }

    /**
     *  Fetch uploads from the Google API
     *  @param boolean $all Whether to return all videos or not
     */
    public function GetUploads($all = false) {
        $content = file_get_contents("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=" . $this->playlistId . "&key=" . $this->aytm->key, true);
        $json = json_decode($content, true);

        foreach($json["items"] as $upload) {
            $video = new YTVideo($this, $upload);
            array_push($this->videos, $video);
        }

        if($all) {
            $b = true;
            while($b) {
                if(!isset($json["nextPageToken"]))
                    $b = false;
                else
                    $token = $json["nextPageToken"];
                $content = file_get_contents("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50" . (isset($token) ? "&pageToken=$token" : "") . "&playlistId=" . $this->playlistId . "&key=" . $this->aytm->key, true);
                $json = json_decode($content, true);

                foreach($json["items"] as $upload) {
                    $video = new YTVideo($this, $upload);
                    array_push($this->videos, $video);
                }
            }
        }
    }

    /** @return YTVideo[] A new videos that have just been added to the DB */
    public function NewVideos() {
        if($this->locked)
            return null;

        $newVideos = array();
        foreach($this->videos as $video) {
            $query = "SELECT * FROM atm_ytview_videos WHERE video_id='" . $video->videoId . "'";
            $result = mysqli_query($this->aytm->sql, $query)
                or die(mysqli_error($this->aytm->sql));

            if(mysqli_num_rows($result) == 1) {
                return $newVideos;
            } else {
                array_push($newVideos, $video);
                $video->title = mysqli_escape_string($this->aytm->sql, $video->title);

                $query_insert = "INSERT INTO atm_ytview_videos (user_id, playlist_id, video_id, channel_id, title, published_at) VALUES
                  ('" . $this->userId . "', '" . $this->playlistId . "', '" . $video->videoId . "', '" . $video->channelId . "', '" . $video->title . "', '" . strtotime($video->published_at) . "')";
                $result_insert = mysqli_query($this->aytm->sql, $query_insert)
                    or die(mysqli_error($this->aytm->sql));
            }
        }

        return $newVideos;
    }
}

/**
 *  The class which holds information about the video for the playlist
 */
class YTVideo {
    /** @var $playlist YTPlaylist YouTube playlist reference */
    public $playlist;
    public $published_at, $channelId, $title, $description, $channelTitle, $position, $videoId;

    /**
     *  @param YTPlaylist $playlist The youtube playlist
     *  @param String[] $json The data in JSON
     */
    function __construct($playlist, $json) {
        $this->playlist = $playlist;
        $cur = $json["snippet"];
        $this->channelId = $cur["channelId"];
        $this->published_at = $cur["publishedAt"];
        $this->title = $cur["title"];
        $this->description = $cur["description"];
        $this->channelTitle = $cur["channelTitle"];
        $this->position = $cur["position"];
        $this->videoId = $cur["resourceId"]["videoId"];
    }

    /**
     * Returns an image link with given resolution:<br>
     * Width:   120<br>
     * Height:  90
     */
    public function GetThumbnailDefault() {
        return "https://i.ytimg.com/vi/" . $this->videoId . "/default.jpg";
    }

    /**
     * Returns an image link with given resolution:<br>
     * Width:   320<br>
     * Height:  180
     */
    public function GetThumbnailMedium() {
        return "https://i.ytimg.com/vi/" . $this->videoId . "/mqdefault.jpg";
    }

    /**
     * Returns an image link with given resolution:<br>
     * Width:   480<br>
     * Height:  360
     */
    public function GetThumbnailHigh() {
        return "https://i.ytimg.com/vi/" . $this->videoId . "/hqdefault.jpg";
    }

    /**
     * Returns an image link with given resolution:<br>
     * Width:   640<br>
     * Height:  480
     */
    public function GetThumbnailStandard() {
        return "https://i.ytimg.com/vi/" . $this->videoId . "/sddefault.jpg";
    }

    /**
     * Returns an image link with given resolution:<br>
     * Width:   (the top quality width the video was uploaded at)<br>
     * Height:  (the top quality height the video was uploaded at)
     */
    public function GetThumbnailMaxRes() {
        return "https://i.ytimg.com/vi/" . $this->videoId . "/maxresdefault.jpg";
    }
}

/**
 *  Used to hold Role information
 *  @version    1.0
 *  @author     Aresak
 */
class Role {
    /** @var int The ID of the role */
    private $id;

    /** @var string The name of the role */
    private $name;

    /** @var string The display name of the role */
    private $display_name;

    /** @var string The user's name color when assigned this role (HEX) */
    private $color;

    /** @var mysqli The MySQLi connection to the database where the roles are stored at */
    private $sql;


    /**
     *  Construct the role object
     *  @param mysqli $sql The MySQLi connection where this cookie is stored at
     *  @param int $role_id The ID of the role
     */
    public function __construct($sql, $role_id = null) {
        $this->sql = $sql;

        if($role_id != null) {                                              // Load the role data from db

            $role_query = "SELECT * FROM ares_roles WHERE ID='$role_id'";   // The role query
            $role_result = mysqli_query($this->sql, $role_query)            // The role result
                or die(mysqli_error($this->sql));                           // MySQLi error handling

            if(mysqli_num_rows($role_result) == 1) {                        // Load only if the role exists

                $this->id       = $role_id;
                $this->name     = Aresak::mysqli_result($role_result, 0, "name");
                $this->display_name = Aresak::mysqli_result($role_result, 0, "display_name");
                $this->color    = Aresak::mysqli_result($role_result, 0, "color");

            } else {
                // Error: The role with this ID doesn't exists, wanna do something here?
                // ...
            }
        }
    }


    /**
        Returns the ID of the role
     */
    public function ID() {
        return $this->id;
    }

    /**
     *  Get the name of the role or if there is name parameter inserted, set the new name.
     *  @param string $name The new role name. Will be lower cased
     *  @return string|boolean
     */
    public function Name($name = null) {
        if($name == null) {
            // The request was to return the role name
            return $this->name;
        }

        // The request was to set the role name
        $name = strtolower($name);                                              // Lowers the name
        $name = strtr($name, array(" " => "_"));                                // Replaces spaces with underscores

        $update_query   = "UPDATE ares_roles SET `name`='$name' WHERE ID='" . $this->id . "'";  // Update query
        $update_result  = mysqli_query($this->sql, $update_query)               // Update result
            or die(mysqli_error($this->sql));                                   // MySQLi error handling

        $this->name = $name;                                                    // Set the new name
        return true;
    }

    /**
     *  Get the display name of the role or if there is new display name parameter inserted, set the new display name.
     *  @param string $display_name The new display name
     *  @return string|boolean
     */
    public function DisplayName($display_name = null) {
        if($display_name == null) {
            // The request was to return the display name
            return $this->display_name;
        }

        // The request was to set the display name
        $update_query   = "UPDATE ares_roles SET display_name='$display_name'
                          WHERE ID='" . $this->id . "'";                        // Update query
        $update_result  = mysqli_query($this->sql, $update_query)               // Update result
            or die(mysqli_error($this->sql));                                   // MySQLi error handling

        $this->display_name = $display_name;                                    // Set the new display name
        return true;
    }

    /**
     *  Get the color of the role or if there is color parameter inserted, set the new color.
     *  @param string $color The color (in hex)
     *  @return string|boolean
     */
    public function Color($color = null) {
        if($color == null) {
            // The request was to return the role color
            return $this->color;
        }

        // The request was to set the role color
        $update_query   = "UPDATE ares_roles SET color='$color'
                          WHERE ID='" . $this->id . "'";                        // Update query
        $update_result  = mysqli_query($this->sql, $update_query)               // Update result
            or die(mysqli_error($this->sql));                                   // MySQLi error handling

        $this->color = $color;                                                  // Set the new color
        return true;
    }

    /**
     *  Creates a new role
     *  @param string $display_name The display name of the role. The name of the role will be this name lower cased
     *                              and spaces replaced with underscores
     *  @param string $color        The color of the role. Must be a hex color code
     *  @param mysqli $sql          The MySQLi connection
     *  @return Role                The created role
     */
    public static function CreateNew($sql, $display_name, $color) {
        $name = strtolower(                                                                 // Lower case the name
            strtr($display_name, array(" " => "_"))                                         // Replace spaces with underscores
        );

        $insert_query   = "INSERT INTO ares_roles (`name`, display_name, color)
        VALUES ('$name', '$display_name', '$color')";                                       // Insert query
        $insert_result  = mysqli_query($sql, $insert_query)                                 // Insert result
            or die(mysqli_error($sql));                                                     // MySQLi error handling

        $role = new Role($sql);                                                             // Create the role object
        $role->CreateFrom_Name($name);                                                      // Creates the role from the name
        return $role;                                                                       // Returns the created role
    }

    /**
     *  Returns the array of all roles
     *  @param mysqli $sql The MySQLi connection
     *  @return Role[]              The array of all roles
     */
    public static function GetAllRoles($sql) {
        $roles = array();                                                                   // Create the array

        $roles_query    = "SELECT * FROM ares_roles";                                       // Query to get all roles
        $roles_result   = mysqli_query($sql, $roles_query)                                  // Result of the query
            or die(mysqli_error($sql));                                                     // MySQLi error handling

        for($i = 0; $i < mysqli_num_rows($roles_result); $i ++) {
                                                                                            // Iterate thru all of the roles
            $role = new Role($sql);                                                         // Create the role object
            $role->id       = Aresak::mysqli_result($roles_result, $i, "ID");               // Assign the ID
            $role->name     = Aresak::mysqli_result($roles_result, $i, "name");             // Assign the name
            $role->display_name = Aresak::mysqli_result($roles_result, $i, "display_name"); // Assign the display name
            $role->color    = Aresak::mysqli_result($roles_result, $i, "color");            // Assign the color

            $roles[$i]      = $role;                                                        // Push this role into the array
        }

        return $roles;                                                                      // Return the array
    }

    /**
     *  Gets the role with the name
     *  @param string $name The ID name of the role
     *  @return boolean The result
     */
    public function CreateFrom_Name($name) {
        $role_query     = "SELECT * FROM ares_roles WHERE `name`='$name'";                  // Get query
        $role_result    = mysqli_query($this->sql, $role_query)                             // Get result
            or die(mysqli_error($this->sql));                                               // MySQLi error handling

        if(mysqli_num_rows($role_result) == 0) {
            // Role not found
            return false;
        }
        // Role was found
        $this->id           = Aresak::mysqli_result($role_result, 0, "ID");                 // Assign the ID
        $this->name         = $name;                                                        // Assign the name
        $this->display_name = Aresak::mysqli_result($role_result, 0, "display_name");       // Assign the display name
        $this->color        = Aresak::mysqli_result($role_result, 0, "color");              // Assign the color
        return true;
    }
}

/**
 * Used to hold Cookie information
 * @version     1.0
 * @author      Aresak
 */
class Cookie {
    /** @var int The cookie ID in the database */
    private $id;
    /** @var int The user ID in the database */
    private $user_id;
    /** @var int The unix time when the cookie expires */
    private $expires_at;
    /** @var string The cookie key */
    private $key;
    /** @var mysqli The MySQLi connection to the database where the cookies are stored at */
    private $sql;
    /** @var int How long in seconds it takes for cookie to expire */
    private static $expiration = 8035200;

    /**
     *  Construct a Cookie object with user object and key
     *  @param Account $user User account
     *  @param string $key The cookie key
     *  @param mysqli $sql The MySQLi connection where this cookie will be stored at
     *  @param boolean $temporary Is the session only temporary?
     */
    public function __construct($sql, $user = null, $key = "", $temporary = false)
    {
        $this->sql = $sql;
        if($user != null) {                     // The request is to create a new cookie
            // Check if user is type of Account
            if(!is_object($user)) {
                // Is not type of Account
                ob_start();
                var_dump($user);
                die("<b><span style='color: red;'>Failed to call Cookie->Construct() as the user argument is not an Account object!</span></b><br>" . ob_get_clean());
            }

            // Check if key is type of string
            if(!is_string($key)) {
                // Is not type of string
                ob_start();
                var_dump($key);
                die("<b><span style='color: red;'>Failed to call Cookie->Construct() as the key argument is not a string!</span></b><br>" . ob_get_clean());
            }

            // Check if the user is initialized
            if(!$user->Initialized()) {
                // The user object is not initialized
                die("<b><span style='color: red;'>Failed to call Cookie->Construct() as the user object is not initialized!</span></b>");
            }

            $this->user_id  = $user->ID();  // Set the user id
            $this->key      = $key;         // Set the key

            if($temporary)
                $expires    = time() + 3600*24;                                                 // Expires in 24 hours
            else
                $expires    = time() + Cookie::$expiration;                                     // Expires in 3 months
            $query          = "INSERT INTO ares_cookies (user_id, expires_at, `key`, creation_ip)
    VALUES ('" . $this->user_id . "', '$expires', '$key', '" . $_SERVER["REMOTE_ADDR"] . "')";  // Insert query
            $result         = mysqli_query($this->sql, $query)                                  // Insert result
                or die(mysqli_error($this->sql));                                               // SQLi error handling


            if($temporary)
                setcookie("aresak_auth", $this->key, null, "/");
            else
                setcookie("aresak_auth", $this->key, $expires, "/");

            $this->expires_at   = $expires;
        }
    }

    /**
     *  Returns the expiration length of all cookies in seconds
     *  @return int
     */
    public static function Expiration_Length() {
        return Cookie::$expiration;
    }

    /**
     *  If empty argument, returns User object otherwise set the User object
     *  @param Account $user User account
     *  @return mixed
     */
    public function User($user = null) {
        if($user == null) {
            // The request was to return User object
            $user = new Account($this->sql);
            $user->CreateFrom_ID((int) $this->user_id);
            return $user;
        }

        // The request was to set the User object
        // Check if the user parameter is type of Account
        if(!$user instanceof Account) {
            // Is not type of account
            ob_start();
            var_dump($user);
            die("<b><span style='color: red;'>Failed to call Cookie->User() as the user argument is not an Account object!</span></b><br>" . ob_get_clean());
        }

        // The parameter is okay
        // Check if the user is initialized
        if(!$user->Initialized()) {
            // The user object is not initialized
            die("<b><span style='color: red;'>Failed to call Cookie->Construct() as the user object is not initialized!</span></b>");
        }

        $this->user_id  = $user->ID();                                              // Save user id [local]


        $query          = "UPDATE ares_cookies SET user_id='" . $user->ID() . "'
                            WHERE ID='" . $this->id . "'";                          // Update query
        $result         = mysqli_query($this->sql, $query)                          // Update result
            or die(mysqli_error($this->sql));                                       // SQLi error handling
        return true;
    }

    /**
     *  If empty argument, returns the User ID otherwise set the User ID
     *  @var int $user_id The User ID
     *  @return boolean
     */
    public function User_ID($user_id = null) {
        if($user_id == null) {
            // The request was to return user id
            return $user_id;
        }

        // The request was to set the user id
        // Check if the user id parameter is type of integer
        if(!is_integer($user_id)) {
            // Is not type of integer
            ob_start();
            var_dump($user_id);
            die("<b><span style='color: red;'>Failed to call Cookie->User_ID() as the user id argument is not an integer!</span></b><br>" . ob_get_clean());
        }

        $this->user_id = $user_id;                                                      // save user id [local]

        $query          = "UPDATE ares_cookies SET user_id='$user_id'
                            WHERE ID='" . $this->id . "'";                              // Update query
        $result         = mysqli_query($this->sql, $query)                              // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling

        return true;
    }

    /**
     *  If empty argument, returns timestamp of the expiration otherwise set the timestamp
     *  @param int $expires_at The unix timestamp of the time when this cookie expires
     *  @return mixed
     */
    public function Expires_At($expires_at = null) {
        if($expires_at == null) {
            // The request was to return the expires at date
            return $this->expires_at;
        }

        // The request was to set the expires at date
        // Check if the expires at parameter is type of integer
        if(!is_int($expires_at)) {
            // The expires at parameter is not type of integer
            ob_start();
            var_dump($expires_at);
            die("<b><span style='color: red;'>Failed to call Cookie->Expires_At() as the expires at argument is not an integer!</span></b><br>" . ob_get_clean());
        }

        // The parameter is okay
        $this->expires_at = $expires_at;

        $query          = "UPDATE ares_cookies SET expires_at='$expires_at'
                            WHERE ID='" . $this->id . "'";                              // Update query
        $result         = mysqli_query($this->sql, $query)                              // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling

        return true;
    }

    /**
     * Return the key for this cookie
     * @return string
     */
    public function Key() {
        return $this->key;
    }

    /**
     *  Return the ID of this cookie
     *  @return int
     */
    public function ID() {
        return $this->id;
    }


    /**
     *  Returns the cookies used for user
     *  @param Account $user The user object
     *  @return Cookie[]
     */
    public function GetCookiesForUser($user) {
        // Check if the user parameter is type of Account
        if(!$user instanceof Account) {
            // Is not type of account
            ob_start();
            var_dump($user);
            die("<b><span style='color: red;'>Failed to call Cookie->User() as the user argument is not an Account object!</span></b><br>" . ob_get_clean());
        }

        // The parameter is okay
        // Check if the user is initialized
        if(!$user->Initialized()) {
            // The user object is not initialized
            die("<b><span style='color: red;'>Failed to call Cookie->Construct() as the user object is not initialized!</span></b>");
        }

        $query      = "SELECT * FROM ares_cookies WHERE user_id='" . $user->ID() . "'";     // Cookies query
        $result     = mysqli_query($this->sql, $query)                                      // Cookies result
            or die(mysqli_error($this->sql));                                               // SQLi error handling

        // Iterate through all cookies
        $cookies = array();
        for($i = 0; $i < mysqli_num_rows($result); $i ++) {
            $cookie = new Cookie($this->sql);                                               // Create new cookie...
            $cookie->CreateFrom_Input(                                                      // ...from input
                Aresak::mysqli_result($result, $i, "ID"),                                           // The cookie ID
                Aresak::mysqli_result($result, $i, "user_id"),                                      // The user ID
                Aresak::mysqli_result($result, $i, "expires_at"),                                   // When the cookie expires
                Aresak::mysqli_result($result, $i, "key"),                                          // The cookie key
                Aresak::mysqli_result($result, $i, "creation_ip")                                   // The IP used when writing this cookie to the DB
            );

            $cookies = array_push($cookies, $cookie);
        }

        return $cookies;
    }

    /**
     *  Create the Cookie from key
     *  @param string $key The key for the cookie
     *  @return boolean
     */
    public function CreateFrom_Key($key) {
        // Check if the key parameter is string
        if(!is_string($key)) {
            // Is not string
            ob_start();
            var_dump($key);
            die("<b><span style='color: red;'>Failed to call Cookie->User() as the user argument is not an Account object!</span></b><br>" . ob_get_clean());
        }

        // The parameter is okay
        $key            = mysqli_escape_string($this->sql, $key);                       // Prepare key for the SQLi

        $this->RemoveExpired();                                                         // Remove expired cookies

        $query          = "SELECT * FROM ares_cookies WHERE `key`='$key'";              // Update query
        $result         = mysqli_query($this->sql, $query)                              // Update result
            or die(mysqli_error($this->sql));                                           // SQLi error handling


        if(mysqli_num_rows($result) == 0) {
            // There is no cookie with this key
            return false;
        }

        // Save it to the class
        $this->id       = Aresak::mysqli_result($result, 0, "ID");
        $this->user_id  = Aresak::mysqli_result($result, 0, "user_id");
        $this->expires_at   = Aresak::mysqli_result($result, 0, "expires_at");
        $this->key      = $key;

        $this->Regenerate();                                                            // Generate a new key

        return true;
    }

    /**
     *  Create a cookie object from raw input
     *  @var int $id The cookie ID
     *  @var int $user_id The User ID
     *  @var string $key The cookie key
     *  @var string $creation_ip The IP used when writing this cookie to the database
     */
    public function CreateFrom_Input(
        $id,
        $user_id,
        $expires_at,
        $key,
        $creation_ip
    ) {
        $this->id           = $id;
        $this->user_id      = $user_id;
        $this->expires_at   = $expires_at;
        $this->key          = $key;
        //$this->creation_ip        = $creation_ip;
    }

    /**
     *  Create a cookie from the session
     *  @return boolean
     */
    public function CreateFrom_Session() {
        if(!isset($_SESSION["aresak_cookie"])) {
            return false;                                                                   // There is no saved session
        }

        $db         = $this->CreateFrom_Key($_SESSION["aresak_cookie"]);
        if(!$db) {
            unset($_SESSION["aresak_cookie"]);                                              // Unset the fake cookie
        }
        return true;
    }

    /**
     *  Removes expired cookies
     *  @return boolean
     */
    private function RemoveExpired() {

        $query      = "DELETE FROM ares_cookies WHERE expires_at <= '" . time() . "'";      // Delete query
        $result     = mysqli_query($this->sql, $query)                                      // Delete result
            or die(mysqli_error($this->sql));                                               // SQLi error handling

        return true;
    }

    /**
     *  Generates a new key for this cookie
     *  @return void
     */
    public function Regenerate() {
        $newKey     = Aresak::GenerateString(128);                                          // Generate a new 128 key

        $query      = "UPDATE ares_cookies SET `key`='$newKey'
                      WHERE ID='" . $this->id . "'";                                        // Update query
        $result     = mysqli_query($this->sql, $query)                                      // Update result
            or die(mysqli_error($this->sql));                                               // SQLi error handling

        setcookie("aresak_auth", $newKey, time() + Cookie::$expiration, "/");               // Give the cookie to user
        $this->key  = $newKey;                                                              // Set the new key
    }

    public function Delete() {
        setcookie("aresak_auth", "", time() - 1, "/");                                      // Expire the cookie

        $query      = "DELETE FROM ares_cookies WHERE ID='" . $this->ID() . "'";            // Delete query
        $result     = mysqli_query($this->sql, $query)                                      // Delete result
            or die(mysqli_error($this->sql));                                               // SQLi error handling
    }
}


/**
 * Mobile Detect Library
 * =====================
 *
 * Motto: "Every business should have a mobile detection script to detect mobile readers"
 *
 * Mobile_Detect is a lightweight PHP class for detecting mobile devices (including tablets).
 * It uses the User-Agent string combined with specific HTTP headers to detect the mobile environment.
 *
 * @author      Current authors: Serban Ghita <serbanghita@gmail.com>
 *                               Nick Ilyin <nick.ilyin@gmail.com>
 *
 *              Original author: Victor Stanciu <vic.stanciu@gmail.com>
 *
 * @license     Code and contributions have 'MIT License'
 *              More details: https://github.com/serbanghita/Mobile-Detect/blob/master/LICENSE.txt
 *
 * @link        Homepage:     http://mobiledetect.net
 *              GitHub Repo:  https://github.com/serbanghita/Mobile-Detect
 *              Google Code:  http://code.google.com/p/php-mobile-detect/
 *              README:       https://github.com/serbanghita/Mobile-Detect/blob/master/README.md
 *              HOWTO:        https://github.com/serbanghita/Mobile-Detect/wiki/Code-examples
 *
 * @version     2.8.25
 */
class Mobile_Detect
{
    /**
     * Mobile detection type.
     *
     * @deprecated since version 2.6.9
     */
    const DETECTION_TYPE_MOBILE = 'mobile';

    /**
     * Extended detection type.
     *
     * @deprecated since version 2.6.9
     */
    const DETECTION_TYPE_EXTENDED = 'extended';

    /**
     * A frequently used regular expression to extract version #s.
     *
     * @deprecated since version 2.6.9
     */
    const VER = '([\w._\+]+)';

    /**
     * Top-level device.
     */
    const MOBILE_GRADE_A = 'A';

    /**
     * Mid-level device.
     */
    const MOBILE_GRADE_B = 'B';

    /**
     * Low-level device.
     */
    const MOBILE_GRADE_C = 'C';

    /**
     * Stores the version number of the current release.
     */
    const VERSION = '2.8.25';

    /**
     * A type for the version() method indicating a string return value.
     */
    const VERSION_TYPE_STRING = 'text';

    /**
     * A type for the version() method indicating a float return value.
     */
    const VERSION_TYPE_FLOAT = 'float';

    /**
     * A cache for resolved matches
     * @var array
     */
    protected $cache = array();

    /**
     * The User-Agent HTTP header is stored in here.
     * @var string
     */
    protected $userAgent = null;

    /**
     * HTTP headers in the PHP-flavor. So HTTP_USER_AGENT and SERVER_SOFTWARE.
     * @var array
     */
    protected $httpHeaders = array();

    /**
     * CloudFront headers. E.g. CloudFront-Is-Desktop-Viewer, CloudFront-Is-Mobile-Viewer & CloudFront-Is-Tablet-Viewer.
     * @var array
     */
    protected $cloudfrontHeaders = array();

    /**
     * The matching Regex.
     * This is good for debug.
     * @var string
     */
    protected $matchingRegex = null;

    /**
     * The matches extracted from the regex expression.
     * This is good for debug.
     * @var string
     */
    protected $matchesArray = null;

    /**
     * The detection type, using self::DETECTION_TYPE_MOBILE or self::DETECTION_TYPE_EXTENDED.
     *
     * @deprecated since version 2.6.9
     *
     * @var string
     */
    protected $detectionType = self::DETECTION_TYPE_MOBILE;

    /**
     * HTTP headers that trigger the 'isMobile' detection
     * to be true.
     *
     * @var array
     */
    protected static $mobileHeaders = array(

        'HTTP_ACCEPT' => array('matches' => array(
            // Opera Mini; @reference: http://dev.opera.com/articles/view/opera-binary-markup-language/
            'application/x-obml2d',
            // BlackBerry devices.
            'application/vnd.rim.html',
            'text/vnd.wap.wml',
            'application/vnd.wap.xhtml+xml'
        )),
        'HTTP_X_WAP_PROFILE' => null,
        'HTTP_X_WAP_CLIENTID' => null,
        'HTTP_WAP_CONNECTION' => null,
        'HTTP_PROFILE' => null,
        // Reported by Opera on Nokia devices (eg. C3).
        'HTTP_X_OPERAMINI_PHONE_UA' => null,
        'HTTP_X_NOKIA_GATEWAY_ID' => null,
        'HTTP_X_ORANGE_ID' => null,
        'HTTP_X_VODAFONE_3GPDPCONTEXT' => null,
        'HTTP_X_HUAWEI_USERID' => null,
        // Reported by Windows Smartphones.
        'HTTP_UA_OS' => null,
        // Reported by Verizon, Vodafone proxy system.
        'HTTP_X_MOBILE_GATEWAY' => null,
        // Seen this on HTC Sensation. SensationXE_Beats_Z715e.
        'HTTP_X_ATT_DEVICEID' => null,
        // Seen this on a HTC.
        'HTTP_UA_CPU' => array('matches' => array('ARM')),
    );

    /**
     * List of mobile devices (phones).
     *
     * @var array
     */
    protected static $phoneDevices = array(
        'iPhone' => '\biPhone\b|\biPod\b', // |\biTunes
        'BlackBerry' => 'BlackBerry|\bBB10\b|rim[0-9]+',
        'HTC' => 'HTC|HTC.*(Sensation|Evo|Vision|Explorer|6800|8100|8900|A7272|S510e|C110e|Legend|Desire|T8282)|APX515CKT|Qtek9090|APA9292KT|HD_mini|Sensation.*Z710e|PG86100|Z715e|Desire.*(A8181|HD)|ADR6200|ADR6400L|ADR6425|001HT|Inspire 4G|Android.*\bEVO\b|T-Mobile G1|Z520m',
        'Nexus' => 'Nexus One|Nexus S|Galaxy.*Nexus|Android.*Nexus.*Mobile|Nexus 4|Nexus 5|Nexus 6',
        // @todo: Is 'Dell Streak' a tablet or a phone? ;)
        'Dell' => 'Dell.*Streak|Dell.*Aero|Dell.*Venue|DELL.*Venue Pro|Dell Flash|Dell Smoke|Dell Mini 3iX|XCD28|XCD35|\b001DL\b|\b101DL\b|\bGS01\b',
        'Motorola' => 'Motorola|DROIDX|DROID BIONIC|\bDroid\b.*Build|Android.*Xoom|HRI39|MOT-|A1260|A1680|A555|A853|A855|A953|A955|A956|Motorola.*ELECTRIFY|Motorola.*i1|i867|i940|MB200|MB300|MB501|MB502|MB508|MB511|MB520|MB525|MB526|MB611|MB612|MB632|MB810|MB855|MB860|MB861|MB865|MB870|ME501|ME502|ME511|ME525|ME600|ME632|ME722|ME811|ME860|ME863|ME865|MT620|MT710|MT716|MT720|MT810|MT870|MT917|Motorola.*TITANIUM|WX435|WX445|XT300|XT301|XT311|XT316|XT317|XT319|XT320|XT390|XT502|XT530|XT531|XT532|XT535|XT603|XT610|XT611|XT615|XT681|XT701|XT702|XT711|XT720|XT800|XT806|XT860|XT862|XT875|XT882|XT883|XT894|XT901|XT907|XT909|XT910|XT912|XT928|XT926|XT915|XT919|XT925|XT1021|\bMoto E\b',
        'Samsung' => '\bSamsung\b|SM-G9250|GT-19300|SGH-I337|BGT-S5230|GT-B2100|GT-B2700|GT-B2710|GT-B3210|GT-B3310|GT-B3410|GT-B3730|GT-B3740|GT-B5510|GT-B5512|GT-B5722|GT-B6520|GT-B7300|GT-B7320|GT-B7330|GT-B7350|GT-B7510|GT-B7722|GT-B7800|GT-C3010|GT-C3011|GT-C3060|GT-C3200|GT-C3212|GT-C3212I|GT-C3262|GT-C3222|GT-C3300|GT-C3300K|GT-C3303|GT-C3303K|GT-C3310|GT-C3322|GT-C3330|GT-C3350|GT-C3500|GT-C3510|GT-C3530|GT-C3630|GT-C3780|GT-C5010|GT-C5212|GT-C6620|GT-C6625|GT-C6712|GT-E1050|GT-E1070|GT-E1075|GT-E1080|GT-E1081|GT-E1085|GT-E1087|GT-E1100|GT-E1107|GT-E1110|GT-E1120|GT-E1125|GT-E1130|GT-E1160|GT-E1170|GT-E1175|GT-E1180|GT-E1182|GT-E1200|GT-E1210|GT-E1225|GT-E1230|GT-E1390|GT-E2100|GT-E2120|GT-E2121|GT-E2152|GT-E2220|GT-E2222|GT-E2230|GT-E2232|GT-E2250|GT-E2370|GT-E2550|GT-E2652|GT-E3210|GT-E3213|GT-I5500|GT-I5503|GT-I5700|GT-I5800|GT-I5801|GT-I6410|GT-I6420|GT-I7110|GT-I7410|GT-I7500|GT-I8000|GT-I8150|GT-I8160|GT-I8190|GT-I8320|GT-I8330|GT-I8350|GT-I8530|GT-I8700|GT-I8703|GT-I8910|GT-I9000|GT-I9001|GT-I9003|GT-I9010|GT-I9020|GT-I9023|GT-I9070|GT-I9082|GT-I9100|GT-I9103|GT-I9220|GT-I9250|GT-I9300|GT-I9305|GT-I9500|GT-I9505|GT-M3510|GT-M5650|GT-M7500|GT-M7600|GT-M7603|GT-M8800|GT-M8910|GT-N7000|GT-S3110|GT-S3310|GT-S3350|GT-S3353|GT-S3370|GT-S3650|GT-S3653|GT-S3770|GT-S3850|GT-S5210|GT-S5220|GT-S5229|GT-S5230|GT-S5233|GT-S5250|GT-S5253|GT-S5260|GT-S5263|GT-S5270|GT-S5300|GT-S5330|GT-S5350|GT-S5360|GT-S5363|GT-S5369|GT-S5380|GT-S5380D|GT-S5560|GT-S5570|GT-S5600|GT-S5603|GT-S5610|GT-S5620|GT-S5660|GT-S5670|GT-S5690|GT-S5750|GT-S5780|GT-S5830|GT-S5839|GT-S6102|GT-S6500|GT-S7070|GT-S7200|GT-S7220|GT-S7230|GT-S7233|GT-S7250|GT-S7500|GT-S7530|GT-S7550|GT-S7562|GT-S7710|GT-S8000|GT-S8003|GT-S8500|GT-S8530|GT-S8600|SCH-A310|SCH-A530|SCH-A570|SCH-A610|SCH-A630|SCH-A650|SCH-A790|SCH-A795|SCH-A850|SCH-A870|SCH-A890|SCH-A930|SCH-A950|SCH-A970|SCH-A990|SCH-I100|SCH-I110|SCH-I400|SCH-I405|SCH-I500|SCH-I510|SCH-I515|SCH-I600|SCH-I730|SCH-I760|SCH-I770|SCH-I830|SCH-I910|SCH-I920|SCH-I959|SCH-LC11|SCH-N150|SCH-N300|SCH-R100|SCH-R300|SCH-R351|SCH-R400|SCH-R410|SCH-T300|SCH-U310|SCH-U320|SCH-U350|SCH-U360|SCH-U365|SCH-U370|SCH-U380|SCH-U410|SCH-U430|SCH-U450|SCH-U460|SCH-U470|SCH-U490|SCH-U540|SCH-U550|SCH-U620|SCH-U640|SCH-U650|SCH-U660|SCH-U700|SCH-U740|SCH-U750|SCH-U810|SCH-U820|SCH-U900|SCH-U940|SCH-U960|SCS-26UC|SGH-A107|SGH-A117|SGH-A127|SGH-A137|SGH-A157|SGH-A167|SGH-A177|SGH-A187|SGH-A197|SGH-A227|SGH-A237|SGH-A257|SGH-A437|SGH-A517|SGH-A597|SGH-A637|SGH-A657|SGH-A667|SGH-A687|SGH-A697|SGH-A707|SGH-A717|SGH-A727|SGH-A737|SGH-A747|SGH-A767|SGH-A777|SGH-A797|SGH-A817|SGH-A827|SGH-A837|SGH-A847|SGH-A867|SGH-A877|SGH-A887|SGH-A897|SGH-A927|SGH-B100|SGH-B130|SGH-B200|SGH-B220|SGH-C100|SGH-C110|SGH-C120|SGH-C130|SGH-C140|SGH-C160|SGH-C170|SGH-C180|SGH-C200|SGH-C207|SGH-C210|SGH-C225|SGH-C230|SGH-C417|SGH-C450|SGH-D307|SGH-D347|SGH-D357|SGH-D407|SGH-D415|SGH-D780|SGH-D807|SGH-D980|SGH-E105|SGH-E200|SGH-E315|SGH-E316|SGH-E317|SGH-E335|SGH-E590|SGH-E635|SGH-E715|SGH-E890|SGH-F300|SGH-F480|SGH-I200|SGH-I300|SGH-I320|SGH-I550|SGH-I577|SGH-I600|SGH-I607|SGH-I617|SGH-I627|SGH-I637|SGH-I677|SGH-I700|SGH-I717|SGH-I727|SGH-i747M|SGH-I777|SGH-I780|SGH-I827|SGH-I847|SGH-I857|SGH-I896|SGH-I897|SGH-I900|SGH-I907|SGH-I917|SGH-I927|SGH-I937|SGH-I997|SGH-J150|SGH-J200|SGH-L170|SGH-L700|SGH-M110|SGH-M150|SGH-M200|SGH-N105|SGH-N500|SGH-N600|SGH-N620|SGH-N625|SGH-N700|SGH-N710|SGH-P107|SGH-P207|SGH-P300|SGH-P310|SGH-P520|SGH-P735|SGH-P777|SGH-Q105|SGH-R210|SGH-R220|SGH-R225|SGH-S105|SGH-S307|SGH-T109|SGH-T119|SGH-T139|SGH-T209|SGH-T219|SGH-T229|SGH-T239|SGH-T249|SGH-T259|SGH-T309|SGH-T319|SGH-T329|SGH-T339|SGH-T349|SGH-T359|SGH-T369|SGH-T379|SGH-T409|SGH-T429|SGH-T439|SGH-T459|SGH-T469|SGH-T479|SGH-T499|SGH-T509|SGH-T519|SGH-T539|SGH-T559|SGH-T589|SGH-T609|SGH-T619|SGH-T629|SGH-T639|SGH-T659|SGH-T669|SGH-T679|SGH-T709|SGH-T719|SGH-T729|SGH-T739|SGH-T746|SGH-T749|SGH-T759|SGH-T769|SGH-T809|SGH-T819|SGH-T839|SGH-T919|SGH-T929|SGH-T939|SGH-T959|SGH-T989|SGH-U100|SGH-U200|SGH-U800|SGH-V205|SGH-V206|SGH-X100|SGH-X105|SGH-X120|SGH-X140|SGH-X426|SGH-X427|SGH-X475|SGH-X495|SGH-X497|SGH-X507|SGH-X600|SGH-X610|SGH-X620|SGH-X630|SGH-X700|SGH-X820|SGH-X890|SGH-Z130|SGH-Z150|SGH-Z170|SGH-ZX10|SGH-ZX20|SHW-M110|SPH-A120|SPH-A400|SPH-A420|SPH-A460|SPH-A500|SPH-A560|SPH-A600|SPH-A620|SPH-A660|SPH-A700|SPH-A740|SPH-A760|SPH-A790|SPH-A800|SPH-A820|SPH-A840|SPH-A880|SPH-A900|SPH-A940|SPH-A960|SPH-D600|SPH-D700|SPH-D710|SPH-D720|SPH-I300|SPH-I325|SPH-I330|SPH-I350|SPH-I500|SPH-I600|SPH-I700|SPH-L700|SPH-M100|SPH-M220|SPH-M240|SPH-M300|SPH-M305|SPH-M320|SPH-M330|SPH-M350|SPH-M360|SPH-M370|SPH-M380|SPH-M510|SPH-M540|SPH-M550|SPH-M560|SPH-M570|SPH-M580|SPH-M610|SPH-M620|SPH-M630|SPH-M800|SPH-M810|SPH-M850|SPH-M900|SPH-M910|SPH-M920|SPH-M930|SPH-N100|SPH-N200|SPH-N240|SPH-N300|SPH-N400|SPH-Z400|SWC-E100|SCH-i909|GT-N7100|GT-N7105|SCH-I535|SM-N900A|SGH-I317|SGH-T999L|GT-S5360B|GT-I8262|GT-S6802|GT-S6312|GT-S6310|GT-S5312|GT-S5310|GT-I9105|GT-I8510|GT-S6790N|SM-G7105|SM-N9005|GT-S5301|GT-I9295|GT-I9195|SM-C101|GT-S7392|GT-S7560|GT-B7610|GT-I5510|GT-S7582|GT-S7530E|GT-I8750|SM-G9006V|SM-G9008V|SM-G9009D|SM-G900A|SM-G900D|SM-G900F|SM-G900H|SM-G900I|SM-G900J|SM-G900K|SM-G900L|SM-G900M|SM-G900P|SM-G900R4|SM-G900S|SM-G900T|SM-G900V|SM-G900W8|SHV-E160K|SCH-P709|SCH-P729|SM-T2558|GT-I9205|SM-G9350|SM-J120F|SM-G920F|SM-G920V|SM-G930F|SM-N910C',
        'LG' => '\bLG\b;|LG[- ]?(C800|C900|E400|E610|E900|E-900|F160|F180K|F180L|F180S|730|855|L160|LS740|LS840|LS970|LU6200|MS690|MS695|MS770|MS840|MS870|MS910|P500|P700|P705|VM696|AS680|AS695|AX840|C729|E970|GS505|272|C395|E739BK|E960|L55C|L75C|LS696|LS860|P769BK|P350|P500|P509|P870|UN272|US730|VS840|VS950|LN272|LN510|LS670|LS855|LW690|MN270|MN510|P509|P769|P930|UN200|UN270|UN510|UN610|US670|US740|US760|UX265|UX840|VN271|VN530|VS660|VS700|VS740|VS750|VS910|VS920|VS930|VX9200|VX11000|AX840A|LW770|P506|P925|P999|E612|D955|D802|MS323)',
        'Sony' => 'SonyST|SonyLT|SonyEricsson|SonyEricssonLT15iv|LT18i|E10i|LT28h|LT26w|SonyEricssonMT27i|C5303|C6902|C6903|C6906|C6943|D2533',
        'Asus' => 'Asus.*Galaxy|PadFone.*Mobile',
        'NokiaLumia' => 'Lumia [0-9]{3,4}',
        // http://www.micromaxinfo.com/mobiles/smartphones
        // Added because the codes might conflict with Acer Tablets.
        'Micromax' => 'Micromax.*\b(A210|A92|A88|A72|A111|A110Q|A115|A116|A110|A90S|A26|A51|A35|A54|A25|A27|A89|A68|A65|A57|A90)\b',
        // @todo Complete the regex.
        'Palm' => 'PalmSource|Palm', // avantgo|blazer|elaine|hiptop|plucker|xiino ;
        'Vertu' => 'Vertu|Vertu.*Ltd|Vertu.*Ascent|Vertu.*Ayxta|Vertu.*Constellation(F|Quest)?|Vertu.*Monika|Vertu.*Signature', // Just for fun ;)
        // http://www.pantech.co.kr/en/prod/prodList.do?gbrand=VEGA (PANTECH)
        // Most of the VEGA devices are legacy. PANTECH seem to be newer devices based on Android.
        'Pantech' => 'PANTECH|IM-A850S|IM-A840S|IM-A830L|IM-A830K|IM-A830S|IM-A820L|IM-A810K|IM-A810S|IM-A800S|IM-T100K|IM-A725L|IM-A780L|IM-A775C|IM-A770K|IM-A760S|IM-A750K|IM-A740S|IM-A730S|IM-A720L|IM-A710K|IM-A690L|IM-A690S|IM-A650S|IM-A630K|IM-A600S|VEGA PTL21|PT003|P8010|ADR910L|P6030|P6020|P9070|P4100|P9060|P5000|CDM8992|TXT8045|ADR8995|IS11PT|P2030|P6010|P8000|PT002|IS06|CDM8999|P9050|PT001|TXT8040|P2020|P9020|P2000|P7040|P7000|C790',
        // http://www.fly-phone.com/devices/smartphones/ ; Included only smartphones.
        'Fly' => 'IQ230|IQ444|IQ450|IQ440|IQ442|IQ441|IQ245|IQ256|IQ236|IQ255|IQ235|IQ245|IQ275|IQ240|IQ285|IQ280|IQ270|IQ260|IQ250',
        // http://fr.wikomobile.com
        'Wiko' => 'KITE 4G|HIGHWAY|GETAWAY|STAIRWAY|DARKSIDE|DARKFULL|DARKNIGHT|DARKMOON|SLIDE|WAX 4G|RAINBOW|BLOOM|SUNSET|GOA(?!nna)|LENNY|BARRY|IGGY|OZZY|CINK FIVE|CINK PEAX|CINK PEAX 2|CINK SLIM|CINK SLIM 2|CINK +|CINK KING|CINK PEAX|CINK SLIM|SUBLIM',
        'iMobile' => 'i-mobile (IQ|i-STYLE|idea|ZAA|Hitz)',
        // Added simvalley mobile just for fun. They have some interesting devices.
        // http://www.simvalley.fr/telephonie---gps-_22_telephonie-mobile_telephones_.html
        'SimValley' => '\b(SP-80|XT-930|SX-340|XT-930|SX-310|SP-360|SP60|SPT-800|SP-120|SPT-800|SP-140|SPX-5|SPX-8|SP-100|SPX-8|SPX-12)\b',
        // Wolfgang - a brand that is sold by Aldi supermarkets.
        // http://www.wolfgangmobile.com/
        'Wolfgang' => 'AT-B24D|AT-AS50HD|AT-AS40W|AT-AS55HD|AT-AS45q2|AT-B26D|AT-AS50Q',
        'Alcatel' => 'Alcatel',
        'Nintendo' => 'Nintendo 3DS',
        // http://en.wikipedia.org/wiki/Amoi
        'Amoi' => 'Amoi',
        // http://en.wikipedia.org/wiki/INQ
        'INQ' => 'INQ',
        // @Tapatalk is a mobile app; http://support.tapatalk.com/threads/smf-2-0-2-os-and-browser-detection-plugin-and-tapatalk.15565/#post-79039
        'GenericPhone' => 'Tapatalk|PDA;|SAGEM|\bmmp\b|pocket|\bpsp\b|symbian|Smartphone|smartfon|treo|up.browser|up.link|vodafone|\bwap\b|nokia|Series40|Series60|S60|SonyEricsson|N900|MAUI.*WAP.*Browser',
    );

    /**
     * List of tablet devices.
     *
     * @var array
     */
    protected static $tabletDevices = array(
        // @todo: check for mobile friendly emails topic.
        'iPad' => 'iPad|iPad.*Mobile',
        // Removed |^.*Android.*Nexus(?!(?:Mobile).)*$
        // @see #442
        'NexusTablet' => 'Android.*Nexus[\s]+(7|9|10)',
        'SamsungTablet' => 'SAMSUNG.*Tablet|Galaxy.*Tab|SC-01C|GT-P1000|GT-P1003|GT-P1010|GT-P3105|GT-P6210|GT-P6800|GT-P6810|GT-P7100|GT-P7300|GT-P7310|GT-P7500|GT-P7510|SCH-I800|SCH-I815|SCH-I905|SGH-I957|SGH-I987|SGH-T849|SGH-T859|SGH-T869|SPH-P100|GT-P3100|GT-P3108|GT-P3110|GT-P5100|GT-P5110|GT-P6200|GT-P7320|GT-P7511|GT-N8000|GT-P8510|SGH-I497|SPH-P500|SGH-T779|SCH-I705|SCH-I915|GT-N8013|GT-P3113|GT-P5113|GT-P8110|GT-N8010|GT-N8005|GT-N8020|GT-P1013|GT-P6201|GT-P7501|GT-N5100|GT-N5105|GT-N5110|SHV-E140K|SHV-E140L|SHV-E140S|SHV-E150S|SHV-E230K|SHV-E230L|SHV-E230S|SHW-M180K|SHW-M180L|SHW-M180S|SHW-M180W|SHW-M300W|SHW-M305W|SHW-M380K|SHW-M380S|SHW-M380W|SHW-M430W|SHW-M480K|SHW-M480S|SHW-M480W|SHW-M485W|SHW-M486W|SHW-M500W|GT-I9228|SCH-P739|SCH-I925|GT-I9200|GT-P5200|GT-P5210|GT-P5210X|SM-T311|SM-T310|SM-T310X|SM-T210|SM-T210R|SM-T211|SM-P600|SM-P601|SM-P605|SM-P900|SM-P901|SM-T217|SM-T217A|SM-T217S|SM-P6000|SM-T3100|SGH-I467|XE500|SM-T110|GT-P5220|GT-I9200X|GT-N5110X|GT-N5120|SM-P905|SM-T111|SM-T2105|SM-T315|SM-T320|SM-T320X|SM-T321|SM-T520|SM-T525|SM-T530NU|SM-T230NU|SM-T330NU|SM-T900|XE500T1C|SM-P605V|SM-P905V|SM-T337V|SM-T537V|SM-T707V|SM-T807V|SM-P600X|SM-P900X|SM-T210X|SM-T230|SM-T230X|SM-T325|GT-P7503|SM-T531|SM-T330|SM-T530|SM-T705|SM-T705C|SM-T535|SM-T331|SM-T800|SM-T700|SM-T537|SM-T807|SM-P907A|SM-T337A|SM-T537A|SM-T707A|SM-T807A|SM-T237|SM-T807P|SM-P607T|SM-T217T|SM-T337T|SM-T807T|SM-T116NQ|SM-P550|SM-T350|SM-T550|SM-T9000|SM-P9000|SM-T705Y|SM-T805|GT-P3113|SM-T710|SM-T810|SM-T815|SM-T360|SM-T533|SM-T113|SM-T335|SM-T715|SM-T560|SM-T670|SM-T677|SM-T377|SM-T567|SM-T357T|SM-T555|SM-T561|SM-T713|SM-T719|SM-T813|SM-T819|SM-T580|SM-T355Y|SM-T280|SM-T817A|SM-T820|SM-W700|SM-P580|SM-T587', // SCH-P709|SCH-P729|SM-T2558|GT-I9205 - Samsung Mega - treat them like a regular phone.
        // http://docs.aws.amazon.com/silk/latest/developerguide/user-agent.html
        'Kindle' => 'Kindle|Silk.*Accelerated|Android.*\b(KFOT|KFTT|KFJWI|KFJWA|KFOTE|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|WFJWAE|KFSAWA|KFSAWI|KFASWI|KFARWI|KFFOWI|KFGIWI|KFMEWI)\b|Android.*Silk/[0-9.]+ like Chrome/[0-9.]+ (?!Mobile)',
        // Only the Surface tablets with Windows RT are considered mobile.
        // http://msdn.microsoft.com/en-us/library/ie/hh920767(v=vs.85).aspx
        'SurfaceTablet' => 'Windows NT [0-9.]+; ARM;.*(Tablet|ARMBJS)',
        // http://shopping1.hp.com/is-bin/INTERSHOP.enfinity/WFS/WW-USSMBPublicStore-Site/en_US/-/USD/ViewStandardCatalog-Browse?CatalogCategoryID=JfIQ7EN5lqMAAAEyDcJUDwMT
        'HPTablet' => 'HP Slate (7|8|10)|HP ElitePad 900|hp-tablet|EliteBook.*Touch|HP 8|Slate 21|HP SlateBook 10',
        // Watch out for PadFone, see #132.
        // http://www.asus.com/de/Tablets_Mobile/Memo_Pad_Products/
        'AsusTablet' => '^.*PadFone((?!Mobile).)*$|Transformer|TF101|TF101G|TF300T|TF300TG|TF300TL|TF700T|TF700KL|TF701T|TF810C|ME171|ME301T|ME302C|ME371MG|ME370T|ME372MG|ME172V|ME173X|ME400C|Slider SL101|\bK00F\b|\bK00C\b|\bK00E\b|\bK00L\b|TX201LA|ME176C|ME102A|\bM80TA\b|ME372CL|ME560CG|ME372CG|ME302KL| K010 | K011 | K017 | K01E |ME572C|ME103K|ME170C|ME171C|\bME70C\b|ME581C|ME581CL|ME8510C|ME181C|P01Y|PO1MA|P01Z',
        'BlackBerryTablet' => 'PlayBook|RIM Tablet',
        'HTCtablet' => 'HTC_Flyer_P512|HTC Flyer|HTC Jetstream|HTC-P715a|HTC EVO View 4G|PG41200|PG09410',
        'MotorolaTablet' => 'xoom|sholest|MZ615|MZ605|MZ505|MZ601|MZ602|MZ603|MZ604|MZ606|MZ607|MZ608|MZ609|MZ615|MZ616|MZ617',
        'NookTablet' => 'Android.*Nook|NookColor|nook browser|BNRV200|BNRV200A|BNTV250|BNTV250A|BNTV400|BNTV600|LogicPD Zoom2',
        // http://www.acer.ro/ac/ro/RO/content/drivers
        // http://www.packardbell.co.uk/pb/en/GB/content/download (Packard Bell is part of Acer)
        // http://us.acer.com/ac/en/US/content/group/tablets
        // http://www.acer.de/ac/de/DE/content/models/tablets/
        // Can conflict with Micromax and Motorola phones codes.
        'AcerTablet' => 'Android.*; \b(A100|A101|A110|A200|A210|A211|A500|A501|A510|A511|A700|A701|W500|W500P|W501|W501P|W510|W511|W700|G100|G100W|B1-A71|B1-710|B1-711|A1-810|A1-811|A1-830)\b|W3-810|\bA3-A10\b|\bA3-A11\b|\bA3-A20\b|\bA3-A30',
        // http://eu.computers.toshiba-europe.com/innovation/family/Tablets/1098744/banner_id/tablet_footerlink/
        // http://us.toshiba.com/tablets/tablet-finder
        // http://www.toshiba.co.jp/regza/tablet/
        'ToshibaTablet' => 'Android.*(AT100|AT105|AT200|AT205|AT270|AT275|AT300|AT305|AT1S5|AT500|AT570|AT700|AT830)|TOSHIBA.*FOLIO',
        // http://www.nttdocomo.co.jp/english/service/developer/smart_phone/technical_info/spec/index.html
        // http://www.lg.com/us/tablets
        'LGTablet' => '\bL-06C|LG-V909|LG-V900|LG-V700|LG-V510|LG-V500|LG-V410|LG-V400|LG-VK810\b',
        'FujitsuTablet' => 'Android.*\b(F-01D|F-02F|F-05E|F-10D|M532|Q572)\b',
        // Prestigio Tablets http://www.prestigio.com/support
        'PrestigioTablet' => 'PMP3170B|PMP3270B|PMP3470B|PMP7170B|PMP3370B|PMP3570C|PMP5870C|PMP3670B|PMP5570C|PMP5770D|PMP3970B|PMP3870C|PMP5580C|PMP5880D|PMP5780D|PMP5588C|PMP7280C|PMP7280C3G|PMP7280|PMP7880D|PMP5597D|PMP5597|PMP7100D|PER3464|PER3274|PER3574|PER3884|PER5274|PER5474|PMP5097CPRO|PMP5097|PMP7380D|PMP5297C|PMP5297C_QUAD|PMP812E|PMP812E3G|PMP812F|PMP810E|PMP880TD|PMT3017|PMT3037|PMT3047|PMT3057|PMT7008|PMT5887|PMT5001|PMT5002',
        // http://support.lenovo.com/en_GB/downloads/default.page?#
        'LenovoTablet' => 'Lenovo TAB|Idea(Tab|Pad)( A1|A10| K1|)|ThinkPad([ ]+)?Tablet|YT3-X90L|YT3-X90F|YT3-X90X|Lenovo.*(S2109|S2110|S5000|S6000|K3011|A3000|A3500|A1000|A2107|A2109|A1107|A5500|A7600|B6000|B8000|B8080)(-|)(FL|F|HV|H|)',
        // http://www.dell.com/support/home/us/en/04/Products/tab_mob/tablets
        'DellTablet' => 'Venue 11|Venue 8|Venue 7|Dell Streak 10|Dell Streak 7',
        // http://www.yarvik.com/en/matrix/tablets/
        'YarvikTablet' => 'Android.*\b(TAB210|TAB211|TAB224|TAB250|TAB260|TAB264|TAB310|TAB360|TAB364|TAB410|TAB411|TAB420|TAB424|TAB450|TAB460|TAB461|TAB464|TAB465|TAB467|TAB468|TAB07-100|TAB07-101|TAB07-150|TAB07-151|TAB07-152|TAB07-200|TAB07-201-3G|TAB07-210|TAB07-211|TAB07-212|TAB07-214|TAB07-220|TAB07-400|TAB07-485|TAB08-150|TAB08-200|TAB08-201-3G|TAB08-201-30|TAB09-100|TAB09-211|TAB09-410|TAB10-150|TAB10-201|TAB10-211|TAB10-400|TAB10-410|TAB13-201|TAB274EUK|TAB275EUK|TAB374EUK|TAB462EUK|TAB474EUK|TAB9-200)\b',
        'MedionTablet' => 'Android.*\bOYO\b|LIFE.*(P9212|P9514|P9516|S9512)|LIFETAB',
        'ArnovaTablet' => '97G4|AN10G2|AN7bG3|AN7fG3|AN8G3|AN8cG3|AN7G3|AN9G3|AN7dG3|AN7dG3ST|AN7dG3ChildPad|AN10bG3|AN10bG3DT|AN9G2',
        // http://www.intenso.de/kategorie_en.php?kategorie=33
        // @todo: http://www.nbhkdz.com/read/b8e64202f92a2df129126bff.html - investigate
        'IntensoTablet' => 'INM8002KP|INM1010FP|INM805ND|Intenso Tab|TAB1004',
        // IRU.ru Tablets http://www.iru.ru/catalog/soho/planetable/
        'IRUTablet' => 'M702pro',
        'MegafonTablet' => 'MegaFon V9|\bZTE V9\b|Android.*\bMT7A\b',
        // http://www.e-boda.ro/tablete-pc.html
        'EbodaTablet' => 'E-Boda (Supreme|Impresspeed|Izzycomm|Essential)',
        // http://www.allview.ro/produse/droseries/lista-tablete-pc/
        'AllViewTablet' => 'Allview.*(Viva|Alldro|City|Speed|All TV|Frenzy|Quasar|Shine|TX1|AX1|AX2)',
        // http://wiki.archosfans.com/index.php?title=Main_Page
        // @note Rewrite the regex format after we add more UAs.
        'ArchosTablet' => '\b(101G9|80G9|A101IT)\b|Qilive 97R|Archos5|\bARCHOS (70|79|80|90|97|101|FAMILYPAD|)(b|c|)(G10| Cobalt| TITANIUM(HD|)| Xenon| Neon|XSK| 2| XS 2| PLATINUM| CARBON|GAMEPAD)\b',
        // http://www.ainol.com/plugin.php?identifier=ainol&module=product
        'AinolTablet' => 'NOVO7|NOVO8|NOVO10|Novo7Aurora|Novo7Basic|NOVO7PALADIN|novo9-Spark',
        'NokiaLumiaTablet' => 'Lumia 2520',
        // @todo: inspect http://esupport.sony.com/US/p/select-system.pl?DIRECTOR=DRIVER
        // Readers http://www.atsuhiro-me.net/ebook/sony-reader/sony-reader-web-browser
        // http://www.sony.jp/support/tablet/
        'SonyTablet' => 'Sony.*Tablet|Xperia Tablet|Sony Tablet S|SO-03E|SGPT12|SGPT13|SGPT114|SGPT121|SGPT122|SGPT123|SGPT111|SGPT112|SGPT113|SGPT131|SGPT132|SGPT133|SGPT211|SGPT212|SGPT213|SGP311|SGP312|SGP321|EBRD1101|EBRD1102|EBRD1201|SGP351|SGP341|SGP511|SGP512|SGP521|SGP541|SGP551|SGP621|SGP612|SOT31',
        // http://www.support.philips.com/support/catalog/worldproducts.jsp?userLanguage=en&userCountry=cn&categoryid=3G_LTE_TABLET_SU_CN_CARE&title=3G%20tablets%20/%20LTE%20range&_dyncharset=UTF-8
        'PhilipsTablet' => '\b(PI2010|PI3000|PI3100|PI3105|PI3110|PI3205|PI3210|PI3900|PI4010|PI7000|PI7100)\b',
        // db + http://www.cube-tablet.com/buy-products.html
        'CubeTablet' => 'Android.*(K8GT|U9GT|U10GT|U16GT|U17GT|U18GT|U19GT|U20GT|U23GT|U30GT)|CUBE U8GT',
        // http://www.cobyusa.com/?p=pcat&pcat_id=3001
        'CobyTablet' => 'MID1042|MID1045|MID1125|MID1126|MID7012|MID7014|MID7015|MID7034|MID7035|MID7036|MID7042|MID7048|MID7127|MID8042|MID8048|MID8127|MID9042|MID9740|MID9742|MID7022|MID7010',
        // http://www.match.net.cn/products.asp
        'MIDTablet' => 'M9701|M9000|M9100|M806|M1052|M806|T703|MID701|MID713|MID710|MID727|MID760|MID830|MID728|MID933|MID125|MID810|MID732|MID120|MID930|MID800|MID731|MID900|MID100|MID820|MID735|MID980|MID130|MID833|MID737|MID960|MID135|MID860|MID736|MID140|MID930|MID835|MID733|MID4X10',
        // http://www.msi.com/support
        // @todo Research the Windows Tablets.
        'MSITablet' => 'MSI \b(Primo 73K|Primo 73L|Primo 81L|Primo 77|Primo 93|Primo 75|Primo 76|Primo 73|Primo 81|Primo 91|Primo 90|Enjoy 71|Enjoy 7|Enjoy 10)\b',
        // @todo http://www.kyoceramobile.com/support/drivers/
        //    'KyoceraTablet' => null,
        // @todo http://intexuae.com/index.php/category/mobile-devices/tablets-products/
        //    'IntextTablet' => null,
        // http://pdadb.net/index.php?m=pdalist&list=SMiT (NoName Chinese Tablets)
        // http://www.imp3.net/14/show.php?itemid=20454
        'SMiTTablet' => 'Android.*(\bMID\b|MID-560|MTV-T1200|MTV-PND531|MTV-P1101|MTV-PND530)',
        // http://www.rock-chips.com/index.php?do=prod&pid=2
        'RockChipTablet' => 'Android.*(RK2818|RK2808A|RK2918|RK3066)|RK2738|RK2808A',
        // http://www.fly-phone.com/devices/tablets/ ; http://www.fly-phone.com/service/
        'FlyTablet' => 'IQ310|Fly Vision',
        // http://www.bqreaders.com/gb/tablets-prices-sale.html
        'bqTablet' => 'Android.*(bq)?.*(Elcano|Curie|Edison|Maxwell|Kepler|Pascal|Tesla|Hypatia|Platon|Newton|Livingstone|Cervantes|Avant|Aquaris [E|M]10)|Maxwell.*Lite|Maxwell.*Plus',
        // http://www.huaweidevice.com/worldwide/productFamily.do?method=index&directoryId=5011&treeId=3290
        // http://www.huaweidevice.com/worldwide/downloadCenter.do?method=index&directoryId=3372&treeId=0&tb=1&type=software (including legacy tablets)
        'HuaweiTablet' => 'MediaPad|MediaPad 7 Youth|IDEOS S7|S7-201c|S7-202u|S7-101|S7-103|S7-104|S7-105|S7-106|S7-201|S7-Slim',
        // Nec or Medias Tab
        'NecTablet' => '\bN-06D|\bN-08D',
        // Pantech Tablets: http://www.pantechusa.com/phones/
        'PantechTablet' => 'Pantech.*P4100',
        // Broncho Tablets: http://www.broncho.cn/ (hard to find)
        'BronchoTablet' => 'Broncho.*(N701|N708|N802|a710)',
        // http://versusuk.com/support.html
        'VersusTablet' => 'TOUCHPAD.*[78910]|\bTOUCHTAB\b',
        // http://www.zync.in/index.php/our-products/tablet-phablets
        'ZyncTablet' => 'z1000|Z99 2G|z99|z930|z999|z990|z909|Z919|z900',
        // http://www.positivoinformatica.com.br/www/pessoal/tablet-ypy/
        'PositivoTablet' => 'TB07STA|TB10STA|TB07FTA|TB10FTA',
        // https://www.nabitablet.com/
        'NabiTablet' => 'Android.*\bNabi',
        'KoboTablet' => 'Kobo Touch|\bK080\b|\bVox\b Build|\bArc\b Build',
        // French Danew Tablets http://www.danew.com/produits-tablette.php
        'DanewTablet' => 'DSlide.*\b(700|701R|702|703R|704|802|970|971|972|973|974|1010|1012)\b',
        // Texet Tablets and Readers http://www.texet.ru/tablet/
        'TexetTablet' => 'NaviPad|TB-772A|TM-7045|TM-7055|TM-9750|TM-7016|TM-7024|TM-7026|TM-7041|TM-7043|TM-7047|TM-8041|TM-9741|TM-9747|TM-9748|TM-9751|TM-7022|TM-7021|TM-7020|TM-7011|TM-7010|TM-7023|TM-7025|TM-7037W|TM-7038W|TM-7027W|TM-9720|TM-9725|TM-9737W|TM-1020|TM-9738W|TM-9740|TM-9743W|TB-807A|TB-771A|TB-727A|TB-725A|TB-719A|TB-823A|TB-805A|TB-723A|TB-715A|TB-707A|TB-705A|TB-709A|TB-711A|TB-890HD|TB-880HD|TB-790HD|TB-780HD|TB-770HD|TB-721HD|TB-710HD|TB-434HD|TB-860HD|TB-840HD|TB-760HD|TB-750HD|TB-740HD|TB-730HD|TB-722HD|TB-720HD|TB-700HD|TB-500HD|TB-470HD|TB-431HD|TB-430HD|TB-506|TB-504|TB-446|TB-436|TB-416|TB-146SE|TB-126SE',
        // Avoid detecting 'PLAYSTATION 3' as mobile.
        'PlaystationTablet' => 'Playstation.*(Portable|Vita)',
        // http://www.trekstor.de/surftabs.html
        'TrekstorTablet' => 'ST10416-1|VT10416-1|ST70408-1|ST702xx-1|ST702xx-2|ST80208|ST97216|ST70104-2|VT10416-2|ST10216-2A|SurfTab',
        // http://www.pyleaudio.com/Products.aspx?%2fproducts%2fPersonal-Electronics%2fTablets
        'PyleAudioTablet' => '\b(PTBL10CEU|PTBL10C|PTBL72BC|PTBL72BCEU|PTBL7CEU|PTBL7C|PTBL92BC|PTBL92BCEU|PTBL9CEU|PTBL9CUK|PTBL9C)\b',
        // http://www.advandigital.com/index.php?link=content-product&jns=JP001
        // because of the short codenames we have to include whitespaces to reduce the possible conflicts.
        'AdvanTablet' => 'Android.* \b(E3A|T3X|T5C|T5B|T3E|T3C|T3B|T1J|T1F|T2A|T1H|T1i|E1C|T1-E|T5-A|T4|E1-B|T2Ci|T1-B|T1-D|O1-A|E1-A|T1-A|T3A|T4i)\b ',
        // http://www.danytech.com/category/tablet-pc
        'DanyTechTablet' => 'Genius Tab G3|Genius Tab S2|Genius Tab Q3|Genius Tab G4|Genius Tab Q4|Genius Tab G-II|Genius TAB GII|Genius TAB GIII|Genius Tab S1',
        // http://www.galapad.net/product.html
        'GalapadTablet' => 'Android.*\bG1\b',
        // http://www.micromaxinfo.com/tablet/funbook
        'MicromaxTablet' => 'Funbook|Micromax.*\b(P250|P560|P360|P362|P600|P300|P350|P500|P275)\b',
        // http://www.karbonnmobiles.com/products_tablet.php
        'KarbonnTablet' => 'Android.*\b(A39|A37|A34|ST8|ST10|ST7|Smart Tab3|Smart Tab2)\b',
        // http://www.myallfine.com/Products.asp
        'AllFineTablet' => 'Fine7 Genius|Fine7 Shine|Fine7 Air|Fine8 Style|Fine9 More|Fine10 Joy|Fine11 Wide',
        // http://www.proscanvideo.com/products-search.asp?itemClass=TABLET&itemnmbr=
        'PROSCANTablet' => '\b(PEM63|PLT1023G|PLT1041|PLT1044|PLT1044G|PLT1091|PLT4311|PLT4311PL|PLT4315|PLT7030|PLT7033|PLT7033D|PLT7035|PLT7035D|PLT7044K|PLT7045K|PLT7045KB|PLT7071KG|PLT7072|PLT7223G|PLT7225G|PLT7777G|PLT7810K|PLT7849G|PLT7851G|PLT7852G|PLT8015|PLT8031|PLT8034|PLT8036|PLT8080K|PLT8082|PLT8088|PLT8223G|PLT8234G|PLT8235G|PLT8816K|PLT9011|PLT9045K|PLT9233G|PLT9735|PLT9760G|PLT9770G)\b',
        // http://www.yonesnav.com/products/products.php
        'YONESTablet' => 'BQ1078|BC1003|BC1077|RK9702|BC9730|BC9001|IT9001|BC7008|BC7010|BC708|BC728|BC7012|BC7030|BC7027|BC7026',
        // http://www.cjshowroom.com/eproducts.aspx?classcode=004001001
        // China manufacturer makes tablets for different small brands (eg. http://www.zeepad.net/index.html)
        'ChangJiaTablet' => 'TPC7102|TPC7103|TPC7105|TPC7106|TPC7107|TPC7201|TPC7203|TPC7205|TPC7210|TPC7708|TPC7709|TPC7712|TPC7110|TPC8101|TPC8103|TPC8105|TPC8106|TPC8203|TPC8205|TPC8503|TPC9106|TPC9701|TPC97101|TPC97103|TPC97105|TPC97106|TPC97111|TPC97113|TPC97203|TPC97603|TPC97809|TPC97205|TPC10101|TPC10103|TPC10106|TPC10111|TPC10203|TPC10205|TPC10503',
        // http://www.gloryunion.cn/products.asp
        // http://www.allwinnertech.com/en/apply/mobile.html
        // http://www.ptcl.com.pk/pd_content.php?pd_id=284 (EVOTAB)
        // @todo: Softwiner tablets?
        // aka. Cute or Cool tablets. Not sure yet, must research to avoid collisions.
        'GUTablet' => 'TX-A1301|TX-M9002|Q702|kf026', // A12R|D75A|D77|D79|R83|A95|A106C|R15|A75|A76|D71|D72|R71|R73|R77|D82|R85|D92|A97|D92|R91|A10F|A77F|W71F|A78F|W78F|W81F|A97F|W91F|W97F|R16G|C72|C73E|K72|K73|R96G
        // http://www.pointofview-online.com/showroom.php?shop_mode=product_listing&category_id=118
        'PointOfViewTablet' => 'TAB-P506|TAB-navi-7-3G-M|TAB-P517|TAB-P-527|TAB-P701|TAB-P703|TAB-P721|TAB-P731N|TAB-P741|TAB-P825|TAB-P905|TAB-P925|TAB-PR945|TAB-PL1015|TAB-P1025|TAB-PI1045|TAB-P1325|TAB-PROTAB[0-9]+|TAB-PROTAB25|TAB-PROTAB26|TAB-PROTAB27|TAB-PROTAB26XL|TAB-PROTAB2-IPS9|TAB-PROTAB30-IPS9|TAB-PROTAB25XXL|TAB-PROTAB26-IPS10|TAB-PROTAB30-IPS10',
        // http://www.overmax.pl/pl/katalog-produktow,p8/tablety,c14/
        // @todo: add more tests.
        'OvermaxTablet' => 'OV-(SteelCore|NewBase|Basecore|Baseone|Exellen|Quattor|EduTab|Solution|ACTION|BasicTab|TeddyTab|MagicTab|Stream|TB-08|TB-09)',
        // http://hclmetablet.com/India/index.php
        'HCLTablet' => 'HCL.*Tablet|Connect-3G-2.0|Connect-2G-2.0|ME Tablet U1|ME Tablet U2|ME Tablet G1|ME Tablet X1|ME Tablet Y2|ME Tablet Sync',
        // http://www.edigital.hu/Tablet_es_e-book_olvaso/Tablet-c18385.html
        'DPSTablet' => 'DPS Dream 9|DPS Dual 7',
        // http://www.visture.com/index.asp
        'VistureTablet' => 'V97 HD|i75 3G|Visture V4( HD)?|Visture V5( HD)?|Visture V10',
        // http://www.mijncresta.nl/tablet
        'CrestaTablet' => 'CTP(-)?810|CTP(-)?818|CTP(-)?828|CTP(-)?838|CTP(-)?888|CTP(-)?978|CTP(-)?980|CTP(-)?987|CTP(-)?988|CTP(-)?989',
        // MediaTek - http://www.mediatek.com/_en/01_products/02_proSys.php?cata_sn=1&cata1_sn=1&cata2_sn=309
        'MediatekTablet' => '\bMT8125|MT8389|MT8135|MT8377\b',
        // Concorde tab
        'ConcordeTablet' => 'Concorde([ ]+)?Tab|ConCorde ReadMan',
        // GoClever Tablets - http://www.goclever.com/uk/products,c1/tablet,c5/
        'GoCleverTablet' => 'GOCLEVER TAB|A7GOCLEVER|M1042|M7841|M742|R1042BK|R1041|TAB A975|TAB A7842|TAB A741|TAB A741L|TAB M723G|TAB M721|TAB A1021|TAB I921|TAB R721|TAB I720|TAB T76|TAB R70|TAB R76.2|TAB R106|TAB R83.2|TAB M813G|TAB I721|GCTA722|TAB I70|TAB I71|TAB S73|TAB R73|TAB R74|TAB R93|TAB R75|TAB R76.1|TAB A73|TAB A93|TAB A93.2|TAB T72|TAB R83|TAB R974|TAB R973|TAB A101|TAB A103|TAB A104|TAB A104.2|R105BK|M713G|A972BK|TAB A971|TAB R974.2|TAB R104|TAB R83.3|TAB A1042',
        // Modecom Tablets - http://www.modecom.eu/tablets/portal/
        'ModecomTablet' => 'FreeTAB 9000|FreeTAB 7.4|FreeTAB 7004|FreeTAB 7800|FreeTAB 2096|FreeTAB 7.5|FreeTAB 1014|FreeTAB 1001 |FreeTAB 8001|FreeTAB 9706|FreeTAB 9702|FreeTAB 7003|FreeTAB 7002|FreeTAB 1002|FreeTAB 7801|FreeTAB 1331|FreeTAB 1004|FreeTAB 8002|FreeTAB 8014|FreeTAB 9704|FreeTAB 1003',
        // Vonino Tablets - http://www.vonino.eu/tablets
        'VoninoTablet' => '\b(Argus[ _]?S|Diamond[ _]?79HD|Emerald[ _]?78E|Luna[ _]?70C|Onyx[ _]?S|Onyx[ _]?Z|Orin[ _]?HD|Orin[ _]?S|Otis[ _]?S|SpeedStar[ _]?S|Magnet[ _]?M9|Primus[ _]?94[ _]?3G|Primus[ _]?94HD|Primus[ _]?QS|Android.*\bQ8\b|Sirius[ _]?EVO[ _]?QS|Sirius[ _]?QS|Spirit[ _]?S)\b',
        // ECS Tablets - http://www.ecs.com.tw/ECSWebSite/Product/Product_Tablet_List.aspx?CategoryID=14&MenuID=107&childid=M_107&LanID=0
        'ECSTablet' => 'V07OT2|TM105A|S10OT1|TR10CS1',
        // Storex Tablets - http://storex.fr/espace_client/support.html
        // @note: no need to add all the tablet codes since they are guided by the first regex.
        'StorexTablet' => 'eZee[_\']?(Tab|Go)[0-9]+|TabLC7|Looney Tunes Tab',
        // Generic Vodafone tablets.
        'VodafoneTablet' => 'SmartTab([ ]+)?[0-9]+|SmartTabII10|SmartTabII7|VF-1497',
        // French tablets - Essentiel B http://www.boulanger.fr/tablette_tactile_e-book/tablette_tactile_essentiel_b/cl_68908.htm?multiChoiceToDelete=brand&mc_brand=essentielb
        // Aka: http://www.essentielb.fr/
        'EssentielBTablet' => 'Smart[ \']?TAB[ ]+?[0-9]+|Family[ \']?TAB2',
        // Ross & Moor - http://ross-moor.ru/
        'RossMoorTablet' => 'RM-790|RM-997|RMD-878G|RMD-974R|RMT-705A|RMT-701|RME-601|RMT-501|RMT-711',
        // i-mobile http://product.i-mobilephone.com/Mobile_Device
        'iMobileTablet' => 'i-mobile i-note',
        // http://www.tolino.de/de/vergleichen/
        'TolinoTablet' => 'tolino tab [0-9.]+|tolino shine',
        // AudioSonic - a Kmart brand
        // http://www.kmart.com.au/webapp/wcs/stores/servlet/Search?langId=-1&storeId=10701&catalogId=10001&categoryId=193001&pageSize=72&currentPage=1&searchCategory=193001%2b4294965664&sortBy=p_MaxPrice%7c1
        'AudioSonicTablet' => '\bC-22Q|T7-QC|T-17B|T-17P\b',
        // AMPE Tablets - http://www.ampe.com.my/product-category/tablets/
        // @todo: add them gradually to avoid conflicts.
        'AMPETablet' => 'Android.* A78 ',
        // Skk Mobile - http://skkmobile.com.ph/product_tablets.php
        'SkkTablet' => 'Android.* (SKYPAD|PHOENIX|CYCLOPS)',
        // Tecno Mobile (only tablet) - http://www.tecno-mobile.com/index.php/product?filterby=smart&list_order=all&page=1
        'TecnoTablet' => 'TECNO P9',
        // JXD (consoles & tablets) - http://jxd.hk/products.asp?selectclassid=009008&clsid=3
        'JXDTablet' => 'Android.* \b(F3000|A3300|JXD5000|JXD3000|JXD2000|JXD300B|JXD300|S5800|S7800|S602b|S5110b|S7300|S5300|S602|S603|S5100|S5110|S601|S7100a|P3000F|P3000s|P101|P200s|P1000m|P200m|P9100|P1000s|S6600b|S908|P1000|P300|S18|S6600|S9100)\b',
        // i-Joy tablets - http://www.i-joy.es/en/cat/products/tablets/
        'iJoyTablet' => 'Tablet (Spirit 7|Essentia|Galatea|Fusion|Onix 7|Landa|Titan|Scooby|Deox|Stella|Themis|Argon|Unique 7|Sygnus|Hexen|Finity 7|Cream|Cream X2|Jade|Neon 7|Neron 7|Kandy|Scape|Saphyr 7|Rebel|Biox|Rebel|Rebel 8GB|Myst|Draco 7|Myst|Tab7-004|Myst|Tadeo Jones|Tablet Boing|Arrow|Draco Dual Cam|Aurix|Mint|Amity|Revolution|Finity 9|Neon 9|T9w|Amity 4GB Dual Cam|Stone 4GB|Stone 8GB|Andromeda|Silken|X2|Andromeda II|Halley|Flame|Saphyr 9,7|Touch 8|Planet|Triton|Unique 10|Hexen 10|Memphis 4GB|Memphis 8GB|Onix 10)',
        // http://www.intracon.eu/tablet
        'FX2Tablet' => 'FX2 PAD7|FX2 PAD10',
        // http://www.xoro.de/produkte/
        // @note: Might be the same brand with 'Simply tablets'
        'XoroTablet' => 'KidsPAD 701|PAD[ ]?712|PAD[ ]?714|PAD[ ]?716|PAD[ ]?717|PAD[ ]?718|PAD[ ]?720|PAD[ ]?721|PAD[ ]?722|PAD[ ]?790|PAD[ ]?792|PAD[ ]?900|PAD[ ]?9715D|PAD[ ]?9716DR|PAD[ ]?9718DR|PAD[ ]?9719QR|PAD[ ]?9720QR|TelePAD1030|Telepad1032|TelePAD730|TelePAD731|TelePAD732|TelePAD735Q|TelePAD830|TelePAD9730|TelePAD795|MegaPAD 1331|MegaPAD 1851|MegaPAD 2151',
        // http://www1.viewsonic.com/products/computing/tablets/
        'ViewsonicTablet' => 'ViewPad 10pi|ViewPad 10e|ViewPad 10s|ViewPad E72|ViewPad7|ViewPad E100|ViewPad 7e|ViewSonic VB733|VB100a',
        // http://www.odys.de/web/internet-tablet_en.html
        'OdysTablet' => 'LOOX|XENO10|ODYS[ -](Space|EVO|Xpress|NOON)|\bXELIO\b|Xelio10Pro|XELIO7PHONETAB|XELIO10EXTREME|XELIOPT2|NEO_QUAD10',
        // http://www.captiva-power.de/products.html#tablets-en
        'CaptivaTablet' => 'CAPTIVA PAD',
        // IconBIT - http://www.iconbit.com/products/tablets/
        'IconbitTablet' => 'NetTAB|NT-3702|NT-3702S|NT-3702S|NT-3603P|NT-3603P|NT-0704S|NT-0704S|NT-3805C|NT-3805C|NT-0806C|NT-0806C|NT-0909T|NT-0909T|NT-0907S|NT-0907S|NT-0902S|NT-0902S',
        // http://www.teclast.com/topic.php?channelID=70&topicID=140&pid=63
        'TeclastTablet' => 'T98 4G|\bP80\b|\bX90HD\b|X98 Air|X98 Air 3G|\bX89\b|P80 3G|\bX80h\b|P98 Air|\bX89HD\b|P98 3G|\bP90HD\b|P89 3G|X98 3G|\bP70h\b|P79HD 3G|G18d 3G|\bP79HD\b|\bP89s\b|\bA88\b|\bP10HD\b|\bP19HD\b|G18 3G|\bP78HD\b|\bA78\b|\bP75\b|G17s 3G|G17h 3G|\bP85t\b|\bP90\b|\bP11\b|\bP98t\b|\bP98HD\b|\bG18d\b|\bP85s\b|\bP11HD\b|\bP88s\b|\bA80HD\b|\bA80se\b|\bA10h\b|\bP89\b|\bP78s\b|\bG18\b|\bP85\b|\bA70h\b|\bA70\b|\bG17\b|\bP18\b|\bA80s\b|\bA11s\b|\bP88HD\b|\bA80h\b|\bP76s\b|\bP76h\b|\bP98\b|\bA10HD\b|\bP78\b|\bP88\b|\bA11\b|\bA10t\b|\bP76a\b|\bP76t\b|\bP76e\b|\bP85HD\b|\bP85a\b|\bP86\b|\bP75HD\b|\bP76v\b|\bA12\b|\bP75a\b|\bA15\b|\bP76Ti\b|\bP81HD\b|\bA10\b|\bT760VE\b|\bT720HD\b|\bP76\b|\bP73\b|\bP71\b|\bP72\b|\bT720SE\b|\bC520Ti\b|\bT760\b|\bT720VE\b|T720-3GE|T720-WiFi',
        // Onda - http://www.onda-tablet.com/buy-android-onda.html?dir=desc&limit=all&order=price
        'OndaTablet' => '\b(V975i|Vi30|VX530|V701|Vi60|V701s|Vi50|V801s|V719|Vx610w|VX610W|V819i|Vi10|VX580W|Vi10|V711s|V813|V811|V820w|V820|Vi20|V711|VI30W|V712|V891w|V972|V819w|V820w|Vi60|V820w|V711|V813s|V801|V819|V975s|V801|V819|V819|V818|V811|V712|V975m|V101w|V961w|V812|V818|V971|V971s|V919|V989|V116w|V102w|V973|Vi40)\b[\s]+',
        'JaytechTablet' => 'TPC-PA762',
        'BlaupunktTablet' => 'Endeavour 800NG|Endeavour 1010',
        // http://www.digma.ru/support/download/
        // @todo: Ebooks also (if requested)
        'DigmaTablet' => '\b(iDx10|iDx9|iDx8|iDx7|iDxD7|iDxD8|iDsQ8|iDsQ7|iDsQ8|iDsD10|iDnD7|3TS804H|iDsQ11|iDj7|iDs10)\b',
        // http://www.evolioshop.com/ro/tablete-pc.html
        // http://www.evolio.ro/support/downloads_static.html?cat=2
        // @todo: Research some more
        'EvolioTablet' => 'ARIA_Mini_wifi|Aria[ _]Mini|Evolio X10|Evolio X7|Evolio X8|\bEvotab\b|\bNeura\b',
        // @todo http://www.lavamobiles.com/tablets-data-cards
        'LavaTablet' => 'QPAD E704|\bIvoryS\b|E-TAB IVORY|\bE-TAB\b',
        // http://www.breezetablet.com/
        'AocTablet' => 'MW0811|MW0812|MW0922|MTK8382|MW1031|MW0831|MW0821|MW0931|MW0712',
        // http://www.mpmaneurope.com/en/products/internet-tablets-14/android-tablets-14/
        'MpmanTablet' => 'MP11 OCTA|MP10 OCTA|MPQC1114|MPQC1004|MPQC994|MPQC974|MPQC973|MPQC804|MPQC784|MPQC780|\bMPG7\b|MPDCG75|MPDCG71|MPDC1006|MP101DC|MPDC9000|MPDC905|MPDC706HD|MPDC706|MPDC705|MPDC110|MPDC100|MPDC99|MPDC97|MPDC88|MPDC8|MPDC77|MP709|MID701|MID711|MID170|MPDC703|MPQC1010',
        // https://www.celkonmobiles.com/?_a=categoryphones&sid=2
        'CelkonTablet' => 'CT695|CT888|CT[\s]?910|CT7 Tab|CT9 Tab|CT3 Tab|CT2 Tab|CT1 Tab|C820|C720|\bCT-1\b',
        // http://www.wolderelectronics.com/productos/manuales-y-guias-rapidas/categoria-2-miTab
        'WolderTablet' => 'miTab \b(DIAMOND|SPACE|BROOKLYN|NEO|FLY|MANHATTAN|FUNK|EVOLUTION|SKY|GOCAR|IRON|GENIUS|POP|MINT|EPSILON|BROADWAY|JUMP|HOP|LEGEND|NEW AGE|LINE|ADVANCE|FEEL|FOLLOW|LIKE|LINK|LIVE|THINK|FREEDOM|CHICAGO|CLEVELAND|BALTIMORE-GH|IOWA|BOSTON|SEATTLE|PHOENIX|DALLAS|IN 101|MasterChef)\b',
        // http://www.mi.com/en
        'MiTablet' => '\bMI PAD\b|\bHM NOTE 1W\b',
        // http://www.nbru.cn/index.html
        'NibiruTablet' => 'Nibiru M1|Nibiru Jupiter One',
        // http://navroad.com/products/produkty/tablety/
        'NexoTablet' => 'NEXO NOVA|NEXO 10|NEXO AVIO|NEXO FREE|NEXO GO|NEXO EVO|NEXO 3G|NEXO SMART|NEXO KIDDO|NEXO MOBI',
        // http://leader-online.com/new_site/product-category/tablets/
        // http://www.leader-online.net.au/List/Tablet
        'LeaderTablet' => 'TBLT10Q|TBLT10I|TBL-10WDKB|TBL-10WDKBO2013|TBL-W230V2|TBL-W450|TBL-W500|SV572|TBLT7I|TBA-AC7-8G|TBLT79|TBL-8W16|TBL-10W32|TBL-10WKB|TBL-W100',
        // http://www.datawind.com/ubislate/
        'UbislateTablet' => 'UbiSlate[\s]?7C',
        // http://www.pocketbook-int.com/ru/support
        'PocketBookTablet' => 'Pocketbook',
        // http://www.kocaso.com/product_tablet.html
        'KocasoTablet' => '\b(TB-1207)\b',
        // http://global.hisense.com/product/asia/tablet/Sero7/201412/t20141215_91832.htm
        'HisenseTablet' => '\b(F5281|E2371)\b',
        // http://www.tesco.com/direct/hudl/
        'Hudl' => 'Hudl HT7S3|Hudl 2',
        // http://www.telstra.com.au/home-phone/thub-2/
        'TelstraTablet' => 'T-Hub2',
        'GenericTablet' => 'Android.*\b97D\b|Tablet(?!.*PC)|BNTV250A|MID-WCDMA|LogicPD Zoom2|\bA7EB\b|CatNova8|A1_07|CT704|CT1002|\bM721\b|rk30sdk|\bEVOTAB\b|M758A|ET904|ALUMIUM10|Smartfren Tab|Endeavour 1010|Tablet-PC-4|Tagi Tab|\bM6pro\b|CT1020W|arc 10HD|\bTP750\b'
    );

    /**
     * List of mobile Operating Systems.
     *
     * @var array
     */
    protected static $operatingSystems = array(
        'AndroidOS' => 'Android',
        'BlackBerryOS' => 'blackberry|\bBB10\b|rim tablet os',
        'PalmOS' => 'PalmOS|avantgo|blazer|elaine|hiptop|palm|plucker|xiino',
        'SymbianOS' => 'Symbian|SymbOS|Series60|Series40|SYB-[0-9]+|\bS60\b',
        // @reference: http://en.wikipedia.org/wiki/Windows_Mobile
        'WindowsMobileOS' => 'Windows CE.*(PPC|Smartphone|Mobile|[0-9]{3}x[0-9]{3})|Window Mobile|Windows Phone [0-9.]+|WCE;',
        // @reference: http://en.wikipedia.org/wiki/Windows_Phone
        // http://wifeng.cn/?r=blog&a=view&id=106
        // http://nicksnettravels.builttoroam.com/post/2011/01/10/Bogus-Windows-Phone-7-User-Agent-String.aspx
        // http://msdn.microsoft.com/library/ms537503.aspx
        // https://msdn.microsoft.com/en-us/library/hh869301(v=vs.85).aspx
        'WindowsPhoneOS' => 'Windows Phone 10.0|Windows Phone 8.1|Windows Phone 8.0|Windows Phone OS|XBLWP7|ZuneWP7|Windows NT 6.[23]; ARM;',
        'iOS' => '\biPhone.*Mobile|\biPod|\biPad',
        // http://en.wikipedia.org/wiki/MeeGo
        // @todo: research MeeGo in UAs
        'MeeGoOS' => 'MeeGo',
        // http://en.wikipedia.org/wiki/Maemo
        // @todo: research Maemo in UAs
        'MaemoOS' => 'Maemo',
        'JavaOS' => 'J2ME/|\bMIDP\b|\bCLDC\b', // '|Java/' produces bug #135
        'webOS' => 'webOS|hpwOS',
        'badaOS' => '\bBada\b',
        'BREWOS' => 'BREW',
    );

    /**
     * List of mobile User Agents.
     *
     * IMPORTANT: This is a list of only mobile browsers.
     * Mobile Detect 2.x supports only mobile browsers,
     * it was never designed to detect all browsers.
     * The change will come in 2017 in the 3.x release for PHP7.
     *
     * @var array
     */
    protected static $browsers = array(
        //'Vivaldi'         => 'Vivaldi',
        // @reference: https://developers.google.com/chrome/mobile/docs/user-agent
        'Chrome' => '\bCrMo\b|CriOS|Android.*Chrome/[.0-9]* (Mobile)?',
        'Dolfin' => '\bDolfin\b',
        'Opera' => 'Opera.*Mini|Opera.*Mobi|Android.*Opera|Mobile.*OPR/[0-9.]+|Coast/[0-9.]+',
        'Skyfire' => 'Skyfire',
        'Edge' => 'Mobile Safari/[.0-9]* Edge',
        'IE' => 'IEMobile|MSIEMobile', // |Trident/[.0-9]+
        'Firefox' => 'fennec|firefox.*maemo|(Mobile|Tablet).*Firefox|Firefox.*Mobile|FxiOS',
        'Bolt' => 'bolt',
        'TeaShark' => 'teashark',
        'Blazer' => 'Blazer',
        // @reference: http://developer.apple.com/library/safari/#documentation/AppleApplications/Reference/SafariWebContent/OptimizingforSafarioniPhone/OptimizingforSafarioniPhone.html#//apple_ref/doc/uid/TP40006517-SW3
        'Safari' => 'Version.*Mobile.*Safari|Safari.*Mobile|MobileSafari',
        // http://en.wikipedia.org/wiki/Midori_(web_browser)
        //'Midori'          => 'midori',
        //'Tizen'           => 'Tizen',
        'UCBrowser' => 'UC.*Browser|UCWEB',
        'baiduboxapp' => 'baiduboxapp',
        'baidubrowser' => 'baidubrowser',
        // https://github.com/serbanghita/Mobile-Detect/issues/7
        'DiigoBrowser' => 'DiigoBrowser',
        // http://www.puffinbrowser.com/index.php
        'Puffin' => 'Puffin',
        // http://mercury-browser.com/index.html
        'Mercury' => '\bMercury\b',
        // http://en.wikipedia.org/wiki/Obigo_Browser
        'ObigoBrowser' => 'Obigo',
        // http://en.wikipedia.org/wiki/NetFront
        'NetFront' => 'NF-Browser',
        // @reference: http://en.wikipedia.org/wiki/Minimo
        // http://en.wikipedia.org/wiki/Vision_Mobile_Browser
        'GenericBrowser' => 'NokiaBrowser|OviBrowser|OneBrowser|TwonkyBeamBrowser|SEMC.*Browser|FlyFlow|Minimo|NetFront|Novarra-Vision|MQQBrowser|MicroMessenger',
        // @reference: https://en.wikipedia.org/wiki/Pale_Moon_(web_browser)
        'PaleMoon' => 'Android.*PaleMoon|Mobile.*PaleMoon',
    );

    /**
     * Utilities.
     *
     * @var array
     */
    protected static $utilities = array(
        // Experimental. When a mobile device wants to switch to 'Desktop Mode'.
        // http://scottcate.com/technology/windows-phone-8-ie10-desktop-or-mobile/
        // https://github.com/serbanghita/Mobile-Detect/issues/57#issuecomment-15024011
        // https://developers.facebook.com/docs/sharing/best-practices
        'Bot' => 'Googlebot|facebookexternalhit|AdsBot-Google|Google Keyword Suggestion|Facebot|YandexBot|YandexMobileBot|bingbot|ia_archiver|AhrefsBot|Ezooms|GSLFbot|WBSearchBot|Twitterbot|TweetmemeBot|Twikle|PaperLiBot|Wotbox|UnwindFetchor|Exabot|MJ12bot|YandexImages|TurnitinBot|Pingdom',
        'MobileBot' => 'Googlebot-Mobile|AdsBot-Google-Mobile|YahooSeeker/M1A1-R2D2',
        'DesktopMode' => 'WPDesktop',
        'TV' => 'SonyDTV|HbbTV', // experimental
        'WebKit' => '(webkit)[ /]([\w.]+)',
        // @todo: Include JXD consoles.
        'Console' => '\b(Nintendo|Nintendo WiiU|Nintendo 3DS|PLAYSTATION|Xbox)\b',
        'Watch' => 'SM-V700',
    );

    /**
     * All possible HTTP headers that represent the
     * User-Agent string.
     *
     * @var array
     */
    protected static $uaHttpHeaders = array(
        // The default User-Agent string.
        'HTTP_USER_AGENT',
        // Header can occur on devices using Opera Mini.
        'HTTP_X_OPERAMINI_PHONE_UA',
        // Vodafone specific header: http://www.seoprinciple.com/mobile-web-community-still-angry-at-vodafone/24/
        'HTTP_X_DEVICE_USER_AGENT',
        'HTTP_X_ORIGINAL_USER_AGENT',
        'HTTP_X_SKYFIRE_PHONE',
        'HTTP_X_BOLT_PHONE_UA',
        'HTTP_DEVICE_STOCK_UA',
        'HTTP_X_UCBROWSER_DEVICE_UA'
    );

    /**
     * The individual segments that could exist in a User-Agent string. VER refers to the regular
     * expression defined in the constant self::VER.
     *
     * @var array
     */
    protected static $properties = array(

        // Build
        'Mobile' => 'Mobile/[VER]',
        'Build' => 'Build/[VER]',
        'Version' => 'Version/[VER]',
        'VendorID' => 'VendorID/[VER]',

        // Devices
        'iPad' => 'iPad.*CPU[a-z ]+[VER]',
        'iPhone' => 'iPhone.*CPU[a-z ]+[VER]',
        'iPod' => 'iPod.*CPU[a-z ]+[VER]',
        //'BlackBerry'    => array('BlackBerry[VER]', 'BlackBerry [VER];'),
        'Kindle' => 'Kindle/[VER]',

        // Browser
        'Chrome' => array('Chrome/[VER]', 'CriOS/[VER]', 'CrMo/[VER]'),
        'Coast' => array('Coast/[VER]'),
        'Dolfin' => 'Dolfin/[VER]',
        // @reference: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent/Firefox
        'Firefox' => array('Firefox/[VER]', 'FxiOS/[VER]'),
        'Fennec' => 'Fennec/[VER]',
        // http://msdn.microsoft.com/en-us/library/ms537503(v=vs.85).aspx
        // https://msdn.microsoft.com/en-us/library/ie/hh869301(v=vs.85).aspx
        'Edge' => 'Edge/[VER]',
        'IE' => array('IEMobile/[VER];', 'IEMobile [VER]', 'MSIE [VER];', 'Trident/[0-9.]+;.*rv:[VER]'),
        // http://en.wikipedia.org/wiki/NetFront
        'NetFront' => 'NetFront/[VER]',
        'NokiaBrowser' => 'NokiaBrowser/[VER]',
        'Opera' => array(' OPR/[VER]', 'Opera Mini/[VER]', 'Version/[VER]'),
        'Opera Mini' => 'Opera Mini/[VER]',
        'Opera Mobi' => 'Version/[VER]',
        'UC Browser' => 'UC Browser[VER]',
        'MQQBrowser' => 'MQQBrowser/[VER]',
        'MicroMessenger' => 'MicroMessenger/[VER]',
        'baiduboxapp' => 'baiduboxapp/[VER]',
        'baidubrowser' => 'baidubrowser/[VER]',
        'SamsungBrowser' => 'SamsungBrowser/[VER]',
        'Iron' => 'Iron/[VER]',
        // @note: Safari 7534.48.3 is actually Version 5.1.
        // @note: On BlackBerry the Version is overwriten by the OS.
        'Safari' => array('Version/[VER]', 'Safari/[VER]'),
        'Skyfire' => 'Skyfire/[VER]',
        'Tizen' => 'Tizen/[VER]',
        'Webkit' => 'webkit[ /][VER]',
        'PaleMoon' => 'PaleMoon/[VER]',

        // Engine
        'Gecko' => 'Gecko/[VER]',
        'Trident' => 'Trident/[VER]',
        'Presto' => 'Presto/[VER]',
        'Goanna' => 'Goanna/[VER]',

        // OS
        'iOS' => ' \bi?OS\b [VER][ ;]{1}',
        'Android' => 'Android [VER]',
        'BlackBerry' => array('BlackBerry[\w]+/[VER]', 'BlackBerry.*Version/[VER]', 'Version/[VER]'),
        'BREW' => 'BREW [VER]',
        'Java' => 'Java/[VER]',
        // @reference: http://windowsteamblog.com/windows_phone/b/wpdev/archive/2011/08/29/introducing-the-ie9-on-windows-phone-mango-user-agent-string.aspx
        // @reference: http://en.wikipedia.org/wiki/Windows_NT#Releases
        'Windows Phone OS' => array('Windows Phone OS [VER]', 'Windows Phone [VER]'),
        'Windows Phone' => 'Windows Phone [VER]',
        'Windows CE' => 'Windows CE/[VER]',
        // http://social.msdn.microsoft.com/Forums/en-US/windowsdeveloperpreviewgeneral/thread/6be392da-4d2f-41b4-8354-8dcee20c85cd
        'Windows NT' => 'Windows NT [VER]',
        'Symbian' => array('SymbianOS/[VER]', 'Symbian/[VER]'),
        'webOS' => array('webOS/[VER]', 'hpwOS/[VER];'),
    );

    /**
     * Construct an instance of this class.
     *
     * @param array $headers Specify the headers as injection. Should be PHP _SERVER flavored.
     *                          If left empty, will use the global _SERVER['HTTP_*'] vars instead.
     * @param string $userAgent Inject the User-Agent header. If null, will use HTTP_USER_AGENT
     *                          from the $headers array instead.
     */
    public function __construct(
        array $headers = null,
        $userAgent = null
    )
    {
        $this->setHttpHeaders($headers);
        $this->setUserAgent($userAgent);
    }

    /**
     * Get the current script version.
     * This is useful for the demo.php file,
     * so people can check on what version they are testing
     * for mobile devices.
     *
     * @return string The version number in semantic version format.
     */
    public static function getScriptVersion()
    {
        return self::VERSION;
    }

    /**
     * Set the HTTP Headers. Must be PHP-flavored. This method will reset existing headers.
     *
     * @param array $httpHeaders The headers to set. If null, then using PHP's _SERVER to extract
     *                           the headers. The default null is left for backwards compatibility.
     */
    public function setHttpHeaders($httpHeaders = null)
    {
        // use global _SERVER if $httpHeaders aren't defined
        if (!is_array($httpHeaders) || !count($httpHeaders)) {
            $httpHeaders = $_SERVER;
        }

        // clear existing headers
        $this->httpHeaders = array();

        // Only save HTTP headers. In PHP land, that means only _SERVER vars that
        // start with HTTP_.
        foreach ($httpHeaders as $key => $value) {
            if (substr($key, 0, 5) === 'HTTP_') {
                $this->httpHeaders[$key] = $value;
            }
        }

        // In case we're dealing with CloudFront, we need to know.
        $this->setCfHeaders($httpHeaders);
    }

    /**
     * Retrieves the HTTP headers.
     *
     * @return array
     */
    public function getHttpHeaders()
    {
        return $this->httpHeaders;
    }

    /**
     * Retrieves a particular header. If it doesn't exist, no exception/error is caused.
     * Simply null is returned.
     *
     * @param string $header The name of the header to retrieve. Can be HTTP compliant such as
     *                       "User-Agent" or "X-Device-User-Agent" or can be php-esque with the
     *                       all-caps, HTTP_ prefixed, underscore seperated awesomeness.
     *
     * @return string|null The value of the header.
     */
    public function getHttpHeader($header)
    {
        // are we using PHP-flavored headers?
        if (strpos($header, '_') === false) {
            $header = str_replace('-', '_', $header);
            $header = strtoupper($header);
        }

        // test the alternate, too
        $altHeader = 'HTTP_' . $header;

        //Test both the regular and the HTTP_ prefix
        if (isset($this->httpHeaders[$header])) {
            return $this->httpHeaders[$header];
        } elseif (isset($this->httpHeaders[$altHeader])) {
            return $this->httpHeaders[$altHeader];
        }

        return null;
    }

    public function getMobileHeaders()
    {
        return self::$mobileHeaders;
    }

    /**
     * Get all possible HTTP headers that
     * can contain the User-Agent string.
     *
     * @return array List of HTTP headers.
     */
    public function getUaHttpHeaders()
    {
        return self::$uaHttpHeaders;
    }


    /**
     * Set CloudFront headers
     * http://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/header-caching.html#header-caching-web-device
     *
     * @param array $cfHeaders List of HTTP headers
     *
     * @return  boolean If there were CloudFront headers to be set
     */
    public function setCfHeaders($cfHeaders = null)
    {
        // use global _SERVER if $cfHeaders aren't defined
        if (!is_array($cfHeaders) || !count($cfHeaders)) {
            $cfHeaders = $_SERVER;
        }

        // clear existing headers
        $this->cloudfrontHeaders = array();

        // Only save CLOUDFRONT headers. In PHP land, that means only _SERVER vars that
        // start with cloudfront-.
        $response = false;
        foreach ($cfHeaders as $key => $value) {
            if (substr(strtolower($key), 0, 16) === 'http_cloudfront_') {
                $this->cloudfrontHeaders[strtoupper($key)] = $value;
                $response = true;
            }
        }

        return $response;
    }

    /**
     * Retrieves the cloudfront headers.
     *
     * @return array
     */
    public function getCfHeaders()
    {
        return $this->cloudfrontHeaders;
    }

    /**
     * Set the User-Agent to be used.
     *
     * @param string $userAgent The user agent string to set.
     *
     * @return string|null
     */
    public function setUserAgent($userAgent = null)
    {
        // Invalidate cache due to #375
        $this->cache = array();

        if (false === empty($userAgent)) {
            return $this->userAgent = $userAgent;
        } else {
            $this->userAgent = null;
            foreach ($this->getUaHttpHeaders() as $altHeader) {
                if (false === empty($this->httpHeaders[$altHeader])) { // @todo: should use getHttpHeader(), but it would be slow. (Serban)
                    $this->userAgent .= $this->httpHeaders[$altHeader] . " ";
                }
            }

            if (!empty($this->userAgent)) {
                return $this->userAgent = trim($this->userAgent);
            }
        }

        if (count($this->getCfHeaders()) > 0) {
            return $this->userAgent = 'Amazon CloudFront';
        }
        return $this->userAgent = null;
    }

    /**
     * Retrieve the User-Agent.
     *
     * @return string|null The user agent if it's set.
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Set the detection type. Must be one of self::DETECTION_TYPE_MOBILE or
     * self::DETECTION_TYPE_EXTENDED. Otherwise, nothing is set.
     *
     * @deprecated since version 2.6.9
     *
     * @param string $type The type. Must be a self::DETECTION_TYPE_* constant. The default
     *                     parameter is null which will default to self::DETECTION_TYPE_MOBILE.
     */
    public function setDetectionType($type = null)
    {
        if ($type === null) {
            $type = self::DETECTION_TYPE_MOBILE;
        }

        if ($type !== self::DETECTION_TYPE_MOBILE && $type !== self::DETECTION_TYPE_EXTENDED) {
            return;
        }

        $this->detectionType = $type;
    }

    public function getMatchingRegex()
    {
        return $this->matchingRegex;
    }

    public function getMatchesArray()
    {
        return $this->matchesArray;
    }

    /**
     * Retrieve the list of known phone devices.
     *
     * @return array List of phone devices.
     */
    public static function getPhoneDevices()
    {
        return self::$phoneDevices;
    }

    /**
     * Retrieve the list of known tablet devices.
     *
     * @return array List of tablet devices.
     */
    public static function getTabletDevices()
    {
        return self::$tabletDevices;
    }

    /**
     * Alias for getBrowsers() method.
     *
     * @return array List of user agents.
     */
    public static function getUserAgents()
    {
        return self::getBrowsers();
    }

    /**
     * Retrieve the list of known browsers. Specifically, the user agents.
     *
     * @return array List of browsers / user agents.
     */
    public static function getBrowsers()
    {
        return self::$browsers;
    }

    /**
     * Retrieve the list of known utilities.
     *
     * @return array List of utilities.
     */
    public static function getUtilities()
    {
        return self::$utilities;
    }

    /**
     * Method gets the mobile detection rules. This method is used for the magic methods $detect->is*().
     *
     * @deprecated since version 2.6.9
     *
     * @return array All the rules (but not extended).
     */
    public static function getMobileDetectionRules()
    {
        static $rules;

        if (!$rules) {
            $rules = array_merge(
                self::$phoneDevices,
                self::$tabletDevices,
                self::$operatingSystems,
                self::$browsers
            );
        }

        return $rules;

    }

    /**
     * Method gets the mobile detection rules + utilities.
     * The reason this is separate is because utilities rules
     * don't necessary imply mobile. This method is used inside
     * the new $detect->is('stuff') method.
     *
     * @deprecated since version 2.6.9
     *
     * @return array All the rules + extended.
     */
    public function getMobileDetectionRulesExtended()
    {
        static $rules;

        if (!$rules) {
            // Merge all rules together.
            $rules = array_merge(
                self::$phoneDevices,
                self::$tabletDevices,
                self::$operatingSystems,
                self::$browsers,
                self::$utilities
            );
        }

        return $rules;
    }

    /**
     * Retrieve the current set of rules.
     *
     * @deprecated since version 2.6.9
     *
     * @return array
     */
    public function getRules()
    {
        if ($this->detectionType == self::DETECTION_TYPE_EXTENDED) {
            return self::getMobileDetectionRulesExtended();
        } else {
            return self::getMobileDetectionRules();
        }
    }

    /**
     * Retrieve the list of mobile operating systems.
     *
     * @return array The list of mobile operating systems.
     */
    public static function getOperatingSystems()
    {
        return self::$operatingSystems;
    }

    /**
     * Check the HTTP headers for signs of mobile.
     * This is the fastest mobile check possible; it's used
     * inside isMobile() method.
     *
     * @return bool
     */
    public function checkHttpHeadersForMobile()
    {

        foreach ($this->getMobileHeaders() as $mobileHeader => $matchType) {
            if (isset($this->httpHeaders[$mobileHeader])) {
                if (is_array($matchType['matches'])) {
                    foreach ($matchType['matches'] as $_match) {
                        if (strpos($this->httpHeaders[$mobileHeader], $_match) !== false) {
                            return true;
                        }
                    }

                    return false;
                } else {
                    return true;
                }
            }
        }

        return false;

    }

    /**
     * Magic overloading method.
     *
     * @method boolean is[...]()
     * @param  string $name
     * @param  array $arguments
     * @return mixed
     * @throws BadMethodCallException when the method doesn't exist and doesn't start with 'is'
     */
    public function __call($name, $arguments)
    {
        // make sure the name starts with 'is', otherwise
        if (substr($name, 0, 2) !== 'is') {
            throw new BadMethodCallException("No such method exists: $name");
        }

        $this->setDetectionType(self::DETECTION_TYPE_MOBILE);

        $key = substr($name, 2);

        return $this->matchUAAgainstKey($key);
    }

    /**
     * Find a detection rule that matches the current User-agent.
     *
     * @param  null $userAgent deprecated
     * @return boolean
     */
    protected function matchDetectionRulesAgainstUA($userAgent = null)
    {
        // Begin general search.
        foreach ($this->getRules() as $_regex) {
            if (empty($_regex)) {
                continue;
            }

            if ($this->match($_regex, $userAgent)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Search for a certain key in the rules array.
     * If the key is found then try to match the corresponding
     * regex against the User-Agent.
     *
     * @param string $key
     *
     * @return boolean
     */
    protected function matchUAAgainstKey($key)
    {
        // Make the keys lowercase so we can match: isIphone(), isiPhone(), isiphone(), etc.
        $key = strtolower($key);
        if (false === isset($this->cache[$key])) {

            // change the keys to lower case
            $_rules = array_change_key_case($this->getRules());

            if (false === empty($_rules[$key])) {
                $this->cache[$key] = $this->match($_rules[$key]);
            }

            if (false === isset($this->cache[$key])) {
                $this->cache[$key] = false;
            }
        }

        return $this->cache[$key];
    }

    /**
     * Check if the device is mobile.
     * Returns true if any type of mobile device detected, including special ones
     * @param  null $userAgent deprecated
     * @param  null $httpHeaders deprecated
     * @return bool
     */
    public function isMobile($userAgent = null, $httpHeaders = null)
    {

        if ($httpHeaders) {
            $this->setHttpHeaders($httpHeaders);
        }

        if ($userAgent) {
            $this->setUserAgent($userAgent);
        }

        // Check specifically for cloudfront headers if the useragent === 'Amazon CloudFront'
        if ($this->getUserAgent() === 'Amazon CloudFront') {
            $cfHeaders = $this->getCfHeaders();
            if (array_key_exists('HTTP_CLOUDFRONT_IS_MOBILE_VIEWER', $cfHeaders) && $cfHeaders['HTTP_CLOUDFRONT_IS_MOBILE_VIEWER'] === 'true') {
                return true;
            }
        }

        $this->setDetectionType(self::DETECTION_TYPE_MOBILE);

        if ($this->checkHttpHeadersForMobile()) {
            return true;
        } else {
            return $this->matchDetectionRulesAgainstUA();
        }

    }

    /**
     * Check if the device is a tablet.
     * Return true if any type of tablet device is detected.
     *
     * @param  string $userAgent deprecated
     * @param  array $httpHeaders deprecated
     * @return bool
     */
    public function isTablet($userAgent = null, $httpHeaders = null)
    {
        // Check specifically for cloudfront headers if the useragent === 'Amazon CloudFront'
        if ($this->getUserAgent() === 'Amazon CloudFront') {
            $cfHeaders = $this->getCfHeaders();
            if (array_key_exists('HTTP_CLOUDFRONT_IS_TABLET_VIEWER', $cfHeaders) && $cfHeaders['HTTP_CLOUDFRONT_IS_TABLET_VIEWER'] === 'true') {
                return true;
            }
        }

        $this->setDetectionType(self::DETECTION_TYPE_MOBILE);

        foreach (self::$tabletDevices as $_regex) {
            if ($this->match($_regex, $userAgent)) {
                return true;
            }
        }

        return false;
    }

    /**
     * This method checks for a certain property in the
     * userAgent.
     * @todo: The httpHeaders part is not yet used.
     *
     * @param  string $key
     * @param  string $userAgent deprecated
     * @param  string $httpHeaders deprecated
     * @return bool|int|null
     */
    public function is($key, $userAgent = null, $httpHeaders = null)
    {
        // Set the UA and HTTP headers only if needed (eg. batch mode).
        if ($httpHeaders) {
            $this->setHttpHeaders($httpHeaders);
        }

        if ($userAgent) {
            $this->setUserAgent($userAgent);
        }

        $this->setDetectionType(self::DETECTION_TYPE_EXTENDED);

        return $this->matchUAAgainstKey($key);
    }

    /**
     * Some detection rules are relative (not standard),
     * because of the diversity of devices, vendors and
     * their conventions in representing the User-Agent or
     * the HTTP headers.
     *
     * This method will be used to check custom regexes against
     * the User-Agent string.
     *
     * @param $regex
     * @param  string $userAgent
     * @return bool
     *
     * @todo: search in the HTTP headers too.
     */
    public function match($regex, $userAgent = null)
    {
        $match = (bool)preg_match(sprintf('#%s#is', $regex), (false === empty($userAgent) ? $userAgent : $this->userAgent), $matches);
        // If positive match is found, store the results for debug.
        if ($match) {
            $this->matchingRegex = $regex;
            $this->matchesArray = $matches;
        }

        return $match;
    }

    /**
     * Get the properties array.
     *
     * @return array
     */
    public static function getProperties()
    {
        return self::$properties;
    }

    /**
     * Prepare the version number.
     *
     * @todo Remove the error supression from str_replace() call.
     *
     * @param string $ver The string version, like "2.6.21.2152";
     *
     * @return float
     */
    public function prepareVersionNo($ver)
    {
        $ver = str_replace(array('_', ' ', '/'), '.', $ver);
        $arrVer = explode('.', $ver, 2);

        if (isset($arrVer[1])) {
            $arrVer[1] = @str_replace('.', '', $arrVer[1]); // @todo: treat strings versions.
        }

        return (float)implode('.', $arrVer);
    }

    /**
     * Check the version of the given property in the User-Agent.
     * Will return a float number. (eg. 2_0 will return 2.0, 4.3.1 will return 4.31)
     *
     * @param string $propertyName The name of the property. See self::getProperties() array
     *                             keys for all possible properties.
     * @param string $type Either self::VERSION_TYPE_STRING to get a string value or
     *                             self::VERSION_TYPE_FLOAT indicating a float value. This parameter
     *                             is optional and defaults to self::VERSION_TYPE_STRING. Passing an
     *                             invalid parameter will default to the this type as well.
     *
     * @return string|float The version of the property we are trying to extract.
     */
    public function version($propertyName, $type = self::VERSION_TYPE_STRING)
    {
        if (empty($propertyName)) {
            return false;
        }

        // set the $type to the default if we don't recognize the type
        if ($type !== self::VERSION_TYPE_STRING && $type !== self::VERSION_TYPE_FLOAT) {
            $type = self::VERSION_TYPE_STRING;
        }

        $properties = self::getProperties();

        // Check if the property exists in the properties array.
        if (true === isset($properties[$propertyName])) {

            // Prepare the pattern to be matched.
            // Make sure we always deal with an array (string is converted).
            $properties[$propertyName] = (array)$properties[$propertyName];

            foreach ($properties[$propertyName] as $propertyMatchString) {

                $propertyPattern = str_replace('[VER]', self::VER, $propertyMatchString);

                // Identify and extract the version.
                preg_match(sprintf('#%s#is', $propertyPattern), $this->userAgent, $match);

                if (false === empty($match[1])) {
                    $version = ($type == self::VERSION_TYPE_FLOAT ? $this->prepareVersionNo($match[1]) : $match[1]);

                    return $version;
                }

            }

        }

        return false;
    }

    /**
     * Retrieve the mobile grading, using self::MOBILE_GRADE_* constants.
     *
     * @return string One of the self::MOBILE_GRADE_* constants.
     */
    public function mobileGrade()
    {
        $isMobile = $this->isMobile();

        if (
            // Apple iOS 4-7.0 – Tested on the original iPad (4.3 / 5.0), iPad 2 (4.3 / 5.1 / 6.1), iPad 3 (5.1 / 6.0), iPad Mini (6.1), iPad Retina (7.0), iPhone 3GS (4.3), iPhone 4 (4.3 / 5.1), iPhone 4S (5.1 / 6.0), iPhone 5 (6.0), and iPhone 5S (7.0)
            $this->is('iOS') && $this->version('iPad', self::VERSION_TYPE_FLOAT) >= 4.3 ||
            $this->is('iOS') && $this->version('iPhone', self::VERSION_TYPE_FLOAT) >= 4.3 ||
            $this->is('iOS') && $this->version('iPod', self::VERSION_TYPE_FLOAT) >= 4.3 ||

            // Android 2.1-2.3 - Tested on the HTC Incredible (2.2), original Droid (2.2), HTC Aria (2.1), Google Nexus S (2.3). Functional on 1.5 & 1.6 but performance may be sluggish, tested on Google G1 (1.5)
            // Android 3.1 (Honeycomb)  - Tested on the Samsung Galaxy Tab 10.1 and Motorola XOOM
            // Android 4.0 (ICS)  - Tested on a Galaxy Nexus. Note: transition performance can be poor on upgraded devices
            // Android 4.1 (Jelly Bean)  - Tested on a Galaxy Nexus and Galaxy 7
            ($this->version('Android', self::VERSION_TYPE_FLOAT) > 2.1 && $this->is('Webkit')) ||

            // Windows Phone 7.5-8 - Tested on the HTC Surround (7.5), HTC Trophy (7.5), LG-E900 (7.5), Nokia 800 (7.8), HTC Mazaa (7.8), Nokia Lumia 520 (8), Nokia Lumia 920 (8), HTC 8x (8)
            $this->version('Windows Phone OS', self::VERSION_TYPE_FLOAT) >= 7.5 ||

            // Tested on the Torch 9800 (6) and Style 9670 (6), BlackBerry® Torch 9810 (7), BlackBerry Z10 (10)
            $this->is('BlackBerry') && $this->version('BlackBerry', self::VERSION_TYPE_FLOAT) >= 6.0 ||
            // Blackberry Playbook (1.0-2.0) - Tested on PlayBook
            $this->match('Playbook.*Tablet') ||

            // Palm WebOS (1.4-3.0) - Tested on the Palm Pixi (1.4), Pre (1.4), Pre 2 (2.0), HP TouchPad (3.0)
            ($this->version('webOS', self::VERSION_TYPE_FLOAT) >= 1.4 && $this->match('Palm|Pre|Pixi')) ||
            // Palm WebOS 3.0  - Tested on HP TouchPad
            $this->match('hp.*TouchPad') ||

            // Firefox Mobile 18 - Tested on Android 2.3 and 4.1 devices
            ($this->is('Firefox') && $this->version('Firefox', self::VERSION_TYPE_FLOAT) >= 18) ||

            // Chrome for Android - Tested on Android 4.0, 4.1 device
            ($this->is('Chrome') && $this->is('AndroidOS') && $this->version('Android', self::VERSION_TYPE_FLOAT) >= 4.0) ||

            // Skyfire 4.1 - Tested on Android 2.3 device
            ($this->is('Skyfire') && $this->version('Skyfire', self::VERSION_TYPE_FLOAT) >= 4.1 && $this->is('AndroidOS') && $this->version('Android', self::VERSION_TYPE_FLOAT) >= 2.3) ||

            // Opera Mobile 11.5-12: Tested on Android 2.3
            ($this->is('Opera') && $this->version('Opera Mobi', self::VERSION_TYPE_FLOAT) >= 11.5 && $this->is('AndroidOS')) ||

            // Meego 1.2 - Tested on Nokia 950 and N9
            $this->is('MeeGoOS') ||

            // Tizen (pre-release) - Tested on early hardware
            $this->is('Tizen') ||

            // Samsung Bada 2.0 - Tested on a Samsung Wave 3, Dolphin browser
            // @todo: more tests here!
            $this->is('Dolfin') && $this->version('Bada', self::VERSION_TYPE_FLOAT) >= 2.0 ||

            // UC Browser - Tested on Android 2.3 device
            (($this->is('UC Browser') || $this->is('Dolfin')) && $this->version('Android', self::VERSION_TYPE_FLOAT) >= 2.3) ||

            // Kindle 3 and Fire  - Tested on the built-in WebKit browser for each
            ($this->match('Kindle Fire') ||
                $this->is('Kindle') && $this->version('Kindle', self::VERSION_TYPE_FLOAT) >= 3.0) ||

            // Nook Color 1.4.1 - Tested on original Nook Color, not Nook Tablet
            $this->is('AndroidOS') && $this->is('NookTablet') ||

            // Chrome Desktop 16-24 - Tested on OS X 10.7 and Windows 7
            $this->version('Chrome', self::VERSION_TYPE_FLOAT) >= 16 && !$isMobile ||

            // Safari Desktop 5-6 - Tested on OS X 10.7 and Windows 7
            $this->version('Safari', self::VERSION_TYPE_FLOAT) >= 5.0 && !$isMobile ||

            // Firefox Desktop 10-18 - Tested on OS X 10.7 and Windows 7
            $this->version('Firefox', self::VERSION_TYPE_FLOAT) >= 10.0 && !$isMobile ||

            // Internet Explorer 7-9 - Tested on Windows XP, Vista and 7
            $this->version('IE', self::VERSION_TYPE_FLOAT) >= 7.0 && !$isMobile ||

            // Opera Desktop 10-12 - Tested on OS X 10.7 and Windows 7
            $this->version('Opera', self::VERSION_TYPE_FLOAT) >= 10 && !$isMobile
        ) {
            return self::MOBILE_GRADE_A;
        }

        if (
            $this->is('iOS') && $this->version('iPad', self::VERSION_TYPE_FLOAT) < 4.3 ||
            $this->is('iOS') && $this->version('iPhone', self::VERSION_TYPE_FLOAT) < 4.3 ||
            $this->is('iOS') && $this->version('iPod', self::VERSION_TYPE_FLOAT) < 4.3 ||

            // Blackberry 5.0: Tested on the Storm 2 9550, Bold 9770
            $this->is('Blackberry') && $this->version('BlackBerry', self::VERSION_TYPE_FLOAT) >= 5 && $this->version('BlackBerry', self::VERSION_TYPE_FLOAT) < 6 ||

            //Opera Mini (5.0-6.5) - Tested on iOS 3.2/4.3 and Android 2.3
            ($this->version('Opera Mini', self::VERSION_TYPE_FLOAT) >= 5.0 && $this->version('Opera Mini', self::VERSION_TYPE_FLOAT) <= 7.0 &&
                ($this->version('Android', self::VERSION_TYPE_FLOAT) >= 2.3 || $this->is('iOS'))) ||

            // Nokia Symbian^3 - Tested on Nokia N8 (Symbian^3), C7 (Symbian^3), also works on N97 (Symbian^1)
            $this->match('NokiaN8|NokiaC7|N97.*Series60|Symbian/3') ||

            // @todo: report this (tested on Nokia N71)
            $this->version('Opera Mobi', self::VERSION_TYPE_FLOAT) >= 11 && $this->is('SymbianOS')
        ) {
            return self::MOBILE_GRADE_B;
        }

        if (
            // Blackberry 4.x - Tested on the Curve 8330
            $this->version('BlackBerry', self::VERSION_TYPE_FLOAT) <= 5.0 ||
            // Windows Mobile - Tested on the HTC Leo (WinMo 5.2)
            $this->match('MSIEMobile|Windows CE.*Mobile') || $this->version('Windows Mobile', self::VERSION_TYPE_FLOAT) <= 5.2 ||

            // Tested on original iPhone (3.1), iPhone 3 (3.2)
            $this->is('iOS') && $this->version('iPad', self::VERSION_TYPE_FLOAT) <= 3.2 ||
            $this->is('iOS') && $this->version('iPhone', self::VERSION_TYPE_FLOAT) <= 3.2 ||
            $this->is('iOS') && $this->version('iPod', self::VERSION_TYPE_FLOAT) <= 3.2 ||

            // Internet Explorer 7 and older - Tested on Windows XP
            $this->version('IE', self::VERSION_TYPE_FLOAT) <= 7.0 && !$isMobile
        ) {
            return self::MOBILE_GRADE_C;
        }

        // All older smartphone platforms and featurephones - Any device that doesn't support media queries
        // will receive the basic, C grade experience.
        return self::MOBILE_GRADE_C;
    }
}
