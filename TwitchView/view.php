<?php
define("_D", "../");
require_once _D . "aresak.php";

$md = new Mobile_Detect();
if($md->isMobile()) {
    die(include "mobile.php");
}


$cid = "";
$offline_banner = "https://static-cdn.jtvnw.net/ttv-static/404_preview-640x360.jpg";


$cols = (isset($_GET["cols"]) ? $_GET["cols"] : 5);

$sql    = Aresak::SQL();

$result = mysqli_query($sql, "SELECT * FROM ares_links WHERE service='" . Service_Twitch . "'")
    or die(mysqli_error($sql));

$streamers = array();
$u = "https://api.twitch.tv/kraken/streams?channel=";
for($i = 0; $i < mysqli_num_rows($result); $i ++) {
    $b = explode("||", Aresak::mysqli_result($result, $i, "extras"));
    $streamers[$i] = $b[0];
    $u .= $b[0] . ",";
}
$ch = curl_init($u."&client_id=".$cid);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
$r = curl_exec($ch);
curl_close($ch);
$r = json_decode($r, true);


foreach($r["streams"] as $stream) {
    if(!file_exists("streams/" . $stream["channel"]["name"])) {
        $data = array();
        $data["display"] = $stream["channel"]["display_name"];
        $data["name"] = $stream["channel"]["name"];
        $data["avatar"] = $stream["channel"]["logo"];
        $data["id"] = $stream["channel"]["_id"];
        fopen("streams/" . $data["name"], 'w');
        file_put_contents("streams/" . $data["name"], json_encode($data));
    }
}


function draw_list($r, $streamers) {
    $online = array();
    $offline = array();

    foreach($streamers as $streamer) {
        $found = false;
        foreach($r["streams"] as $onliner) {
            if($streamer == $onliner["channel"]["name"]) {
                $online[count($online)] = $onliner;
                $found = true;
                break;
            }
        }

        if(!$found) {
            $offline[count($offline)] = $streamer;
        }

    }
    ?>
    <div id="list">
        <?php

        $offliner_t = array();
        $offliner_f = array();

        foreach($offline as $offliner) {
            if(file_exists("streams/$offliner")) {
                $offliner_t[count($offliner_t)] = json_decode(file_get_contents("streams/$offliner"), true);
            } else {
                $offliner_f[count($offliner_f)] = $offliner;
            }
        }

        function cmp_online($a, $b)
        {
            return strcmp($a["channel"]["name"], $b["channel"]["name"]);
        }

        function cmp_offline1($a, $b) {
            return strcmp($a["name"], $b["name"]);
        }

        usort($online, "cmp_online");
        usort($offliner_t, "cmp_offline1");
        asort($offliner_f);

        foreach($online as $onliner) {
            ?>
            <div onclick="goto('<?php echo $onliner["channel"]["name"] ?>', true);" class="channel online" channel="<?php echo $onliner["channel"]["name"] ?>">
                <img src="<?php echo $onliner["channel"]["logo"] ?>">
                <div class="info">
                    <b><?php echo $onliner["channel"]["display_name"] ?></b><br>
                    <span class="viewers">
                            <img src="https://lh4.ggpht.com/Smwvtf8W3996Rz6KaMQxAFpY9Dk-r3cGAy0D_Me3-AzUt9oJ07ZHFUbXPN5ErkoU2A=w300" width="15" height="16">
                        <?php echo $onliner["viewers"]; ?></span>
                </div>
            </div>
            <?php
        }

        foreach($offliner_t as $offliner) {
            ?>
            <div onclick="goto('<?php echo $offliner["name"] ?>', true);" class="channel offline offline1 pointer" channel="<?php echo $offliner["name"]; ?>">
                <img src="<?php echo $offliner["avatar"]; ?>">
                <div class="info">
                    <b><?php echo $offliner["display"]; ?></b>
                </div>
            </div>
            <?php
        }

        foreach($offliner_f as $offliner) {
            ?>
            <div onclick="goto('<?php echo $offliner ?>', true);" class="channel offline offline2 pointer" channel="<?php echo $offliner; ?>">
                <img src="https://lh4.ggpht.com/c5fuK7eoARzF7v6vZYZYu-mWX0dD993ssmvfrKeDxJlEBqstzL9Ngmdgy8GULZaIG2JB=w300">
                <div class="info">
                    <b><?php echo $offliner; ?></b>
                </div>
            </div>
            <?php
        }

        ?>
    </div>
    <?php
}

function draw_play($r, $sql) {
    if(!isset($_COOKIE["twitchview_autoplay"])) {
        $autoplay = "true";
        $muted = "true";
    } else {
        $autoplay = ($_COOKIE["twitchview_autoplay"] == "true" ? "true" : "false");
        $muted = ($_COOKIE["twitchview_muted"] == "true" ? "true" : "false");
    }

    $enabled = "Enabled <span class=\"glyphicon glyphicon-thumbs-up\"></span>";
    $disabled = "Disabled <span class=\"glyphicon glyphicon-thumbs-down\"></span>";

    $priority = mysqli_query($sql, "SELECT * FROM atm_twitchview_priority")
        or die(mysqli_error($sql));

    $priorityInt = 1000;
    $priorityChan = null;
    $priorityID = 0;

    $a = 0;
    foreach($r["streams"] as $stream) {
        for($i = 0; $i < mysqli_num_rows($priority); $i ++) {
            if($stream["channel"]["name"] == Aresak::mysqli_result($priority, $i, "twitch")) {
                $p = Aresak::mysqli_result($priority, $i, "priority");

                if($p < $priorityInt) {
                    $priorityInt = $p;
                    $priorityID = $a;
                    $priorityChan = $stream;
                }
                break;
            }
        }
        $a ++;
    }

    if($priorityChan != null) {
        $rand = $priorityID;
    } else {
        $rand = rand(0, count($r["streams"]) - 1);

        $a = array();
        for($i = 0; $i < 20; $i ++) {
            array_push($a, $rand = rand(0, count($r["streams"]) - 1));
        }
    }
    ?>
    <div id="play">
        <?php
        if(sizeof($r["streams"]) > 0) {
            ?>
            <iframe
                src="http://player.twitch.tv/?channel=<?php echo $r["streams"][$rand]["channel"]["name"] ?>&autoplay=<?php echo $autoplay ?>&muted=<?php echo $muted ?>"
                height="360"
                width="640"
                frameborder="0"
                scrolling="no"
                allowfullscreen="true" class="player">
            </iframe>
            <div class="play-info" align="center">
                <span class="config pointer" onClick="$('#config2').toggle();">Edit <span class="glyphicon glyphicon-cog"></span></span> |
                <span onclick="goto('<?php echo $r["streams"][$rand]["channel"]["name"] ?>', true);" class="title pointer" channel="<?php echo $r["streams"][$rand]["channel"]["name"] ?>"><b><?php echo $r["streams"][$rand]["channel"]["display_name"] ?></b></span>
            </div>
            <div id="config2">
                <span class="cfg-close glyphicon glyphicon-remove" onclick="$('#config2').hide(1);"></span>
                <div class="option" onclick="setting('autoplay');"><span class="glyphicon glyphicon-play-circle"></span> <b>Autoplay:</b> <span class="cfg-cookie cfg-autoplay"><?php if($autoplay == "true") echo $enabled; else echo $disabled; ?></span></div>
                <div class="option" onclick="setting('muted');"><span class="glyphicon glyphicon-headphones"></span> <b>Audio:</b> <span class="cfg-cookie cfg-muted"><?php if($muted == "true") echo $disabled; else echo $enabled; ?></span></div>
                <div class="credit">Created by Aresak for Dragonsgetit.com</div>
            </div>
            <?php
        } else {
            ?>
            <div id="shutdown">
                <img src="https://static-cdn.jtvnw.net/ttv-static/404_preview-640x360.jpg">
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}


function draw_live($r, $cols) {
    ?>
    <div id="live_list">
        <?php

        $col = 0;
        foreach($r["streams"] as $s) {
            if ($col == $cols) echo "<br>";
            $id = $s["channel"]["_id"];
            ?>
            <div class="live_content" id="<?php echo $id; ?>_content">
                <a href="<?php echo $s["channel"]["url"] ?>" target="_blank">
                    <div class="live_preview">
                        <img class="responsive-width" id="<?php echo $id; ?>_preview" src="<?php echo $s["preview"]["medium"]; ?>">
                    </div>
                    <div class="live_title">
                        <span class="name"><?php echo $s["channel"]["display_name"]; ?></span> |
                        <span class="status"><?php echo $s["channel"]["status"]; ?></span> <br>
                    <span class="followers"
                          id="<?php echo $id; ?>_followers">
                        <img src="https://cdn0.iconfinder.com/data/icons/admin-panel-glyph-black/2048/599_-_Subscribers-512.png" width="16" height="16">
                        <?php echo $s["channel"]["followers"]; ?></span> |
                        <span class="views" id="<?php echo $id; ?>_views">
                            <img src="https://cdn3.iconfinder.com/data/icons/faticons/32/view-01-128.png" width="16" height="16">
                            <?php echo $s["channel"]["views"]; ?></span> |
                        <span class="viewers" id="<?php echo $id; ?>_viewers">
                            <img src="https://lh4.ggpht.com/Smwvtf8W3996Rz6KaMQxAFpY9Dk-r3cGAy0D_Me3-AzUt9oJ07ZHFUbXPN5ErkoU2A=w300" width="15" height="16">
                            <?php echo $s["viewers"]; ?></span>
                    </div>
                </a>
            </div>
            <?php
        }

        ?>
    </div>
    <?php
}

function draw_css() {
    ?>
    <style>
        body {
            font-family: Arial;
        }

        a, a:active {
            color: black;
            text-decoration: none;
        }

        .cfg-cookie {
            cursor: pointer;
            color: rgba(69, 105, 90, 1);
        }

        .credit {
            position: absolute;
            bottom: 2px;
            left: 2px;
            font-size: smaller;
        }

        #config2 {
            position: relative;
            width: 200px;
            height: 80px;
            top: 5px;
            left: 330px;
            background-color: white;
            border-radius: 4px;
            display: none;
            color: black;
            z-index: 2;

            border: 1px solid black;
        }

        .cfg-close {
            position: absolute;
            top: 2px;
            right: 2px;
            cursor: pointer;
        }

        .channel {
            width: 100%;
            position: relative;
            height: 64px;
            border-bottom: 1px solid black;
        }

        .pointer {
            cursor: pointer;
        }

        .channel img {
            left: 0;
            top: 0;
            width: 64px;
            height: 64px;
        }

        .viewers img {
            width: 16px;
            height: 15px;
        }

        .play-info {
            position: relative;
            top: -10px;
        }

        .channel .info {
            position: relative;
            top: -50px;
            left: 70px;
            height: 100%;
            width: 100%;
            word-wrap: break-word;
        }

        #play iframe {
            position: relative;
            left: 117px;
        }

        #shutdown {
            position: relative;
            left: 117px;
        }

        .online {
            background-color: rgba(46, 172, 61, 1);
            cursor: pointer;
        }

        .online:hover {
            background-color: rgba(40, 143, 55, 1);
        }

        .offline {
            background-color: rgba(172, 38, 32, 1);
        }

        .offline:hover {
            background-color: rgba(154, 36, 30, 1);
        }

        .offline img {
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
        }

        .live_content {
            cursor: pointer;
        }

        .responsive-width {
            width: 175px;
        }

        .responsive-height {
            height: 225px;
        }

        .viewers {
            color: red;
        }

        #play {
            background-color: black;
            color: rgba(223, 221, 220, 1);

            position: fixed;
            left: 290px;
            width: 875px;
            height: 380px;

            z-index: 1;
        }

        #list {
            position: fixed;
            left: 0;
            top: 0;
            width: 288px;
            height: 100%;


            overflow-y: auto;
            overflow-x: hidden;

            background: black url("https://cdn.iconscout.com/public/images/icon/premium/png-512/twitch-301b623d26225ce9-512x512.png");
            background-size: 64px;
        }

        #live_list {
            position: fixed;
            left: 290px;
            top: 382px;
            width: 875px;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        #live_list .live_content {
            width: 175px;
            height: 225px;
            margin-left: 3px;
            float: left;
            word-wrap: break-word;
        }

        .live_content a:hover {
            text-decoration: none;
        }
    </style>
    <?php
}

function draw_script() {
    ?>
    <script>
        function setting(type) {
            var enabled = "Enabled <span class=\"glyphicon glyphicon-thumbs-up\"></span>";
            var disabled = "Disabled <span class=\"glyphicon glyphicon-thumbs-down\"></span>";

            switch(type) {
                case "autoplay":
                    if(Cookies.get("twitchview_autoplay") == "true") {
                        Cookies.set("twitchview_autoplay", "false", { expires: 365 });
                        $(".cfg-autoplay").html(disabled);
                        console.log("Autoplay disabled");
                    } else {
                        Cookies.set("twitchview_autoplay", "true", { expires: 365 });
                        $(".cfg-autoplay").html(enabled);
                        console.log("Autoplay enabled");
                    }
                    break;
                case "muted":
                    if(Cookies.get("twitchview_muted") == "true") {
                        Cookies.set("twitchview_muted", "false", { expires: 365 });
                        $(".cfg-muted").html(enabled);
                        console.log("Audio enabled");
                    } else {
                        Cookies.set("twitchview_muted", "true", { expires: 365 });
                        $(".cfg-muted").html(disabled);
                        console.log("Audio disabled");
                    }
                    break;
            }
        }

        function goto(channel, extra) {
            if(extra) {
                var win = window.open("https://twitch.tv/" + channel, '_blank');
                win.focus();
            }
            else
                window.location.href = "https://twitch.tv/" + channel;
        }

        function loadList() {
            //$("#list").load("?passive=true&list=true");
        }

        function loadLive() {
            $("#live_list").load("?passive=true&live=true");
        }

        function cycle() {
            loadList();
            loadLive();

            console.log("TwitchView updated");
            setTimeout(cycle, 5000);
        }

        setTimeout(cycle, 5000);
    </script>
    <?php
}

function draw($r, $cols, $streamers, $sql) {
    ?>
    <div id="twitchview">
        <?php

        draw_list($r, $streamers);
        draw_play($r, $sql);
        draw_live($r, $cols);
        draw_css();
        draw_script();
        ?>
    </div>
    <?php
}
/*
?>
<html>
    <head>
        <title>#DragonsGetIt live</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="http://dragonsgetit.com/TwitchScout/cookie.js"></script>
    </head>
    <body>
        <?php draw($r, $cols, $streamers, $sql); ?>
    </body>
</html>

<?php*/

if(isset($_GET["passive"])) {
    if(isset($_GET["list"])) {
        draw_list($r, $streamers);
    }
    if(isset($_GET["live"])) {
        draw_live($r, $cols);
    }
} else {
    ?>
    <html>
    <head>
        <title>#DragonsGetIt live</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
    <?php draw($r, $cols, $streamers, $sql); ?>
    </body>
    </html>

    <?php
}

?>
