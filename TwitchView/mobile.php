<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 22.05.2017
 * Time: 11:52
 */
define("_D", "../");
require_once _D . "aresak.php";
// http://dragonsgetit.com/ares/TwitchView/mobile.php?iframe-width=100%&window-refresh=1&iframe-height=100%&livebox-width=322px&livebox-height=280px&cols=5&livebox-max=0
$cid = "";
if (isset($_GET["passive"])) {
    $cols = (isset($_GET["cols"]) ? $_GET["cols"] : 5);

    $sql = Aresak::SQL();

    $result = mysqli_query($sql, "SELECT * FROM ares_links WHERE service='" . Service_Twitch . "'")
        or die(mysqli_error($sql));


    $u = "https://api.twitch.tv/kraken/streams?channel=";
    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
        $b = explode("||", Aresak::mysqli_result($result, $i, "extras"));
        $u .= $b[0] . ",";
    }
    $ch = curl_init($u."&client_id=".$cid);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $r = curl_exec($ch);
    curl_close($ch);
    $r = json_decode($r, true);

    $col = 0;
    foreach($r["streams"] as $s) {
        if ($col == $cols) echo "<br>";
        $id = $s["channel"]["_id"];
        ?>
        <div class="live_content" id="<?php echo $id; ?>_content">
            <a href="<?php echo $s["channel"]["url"] ?>" target="_blank">
                <div class="live_preview">
                    <img class="responsive-width" id="<?php echo $id; ?>_preview" src="<?php echo $s["preview"]["medium"]; ?>">
                </div>
                <div class="live_title">
                    <span class="name"><?php echo $s["channel"]["display_name"]; ?></span> |
                    <span class="status"><?php echo $s["channel"]["status"]; ?></span> <br>
                    <span class="followers"
                          id="<?php echo $id; ?>_followers">
                        <img src="https://cdn0.iconfinder.com/data/icons/admin-panel-glyph-black/2048/599_-_Subscribers-512.png" width="16" height="16">
                        <?php echo $s["channel"]["followers"]; ?></span> |
                        <span class="views" id="<?php echo $id; ?>_views">
                            <img src="https://cdn3.iconfinder.com/data/icons/faticons/32/view-01-128.png" width="16" height="16">
                            <?php echo $s["channel"]["views"]; ?></span> |
                        <span class="viewers" id="<?php echo $id; ?>_viewers">
                            <img src="https://lh4.ggpht.com/Smwvtf8W3996Rz6KaMQxAFpY9Dk-r3cGAy0D_Me3-AzUt9oJ07ZHFUbXPN5ErkoU2A=w300" width="15" height="16">
                            <?php echo $s["viewers"]; ?></span>
                </div>
            </a>
        </div>
        <?php
    }
} else {
    ?>
    <html>
    <head>
        <!-- <meta http-equiv="refresh" content="<?php echo $_GET["window-refresh"]; ?>"> -->
        <title>#DragonsGetIt live</title>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <!-- Made by Aresak -->
    </head>
    <body>
    <?php
    $cols = (isset($_GET["cols"]) ? $_GET["cols"] : 5);

    $sql = Aresak::SQL();

    $result = mysqli_query($sql, "SELECT * FROM ares_links WHERE service='" . Service_Twitch . "'")
        or die(mysqli_error($sql));



    $u = "https://api.twitch.tv/kraken/streams?channel=";
    for($i = 0; $i < mysqli_num_rows($result); $i ++) {
        $b = explode("||", Aresak::mysqli_result($result, $i, "extras"));
        $u .= $b[0] . ",";
    }
    $ch = curl_init($u."&client_id=".$cid);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $r = curl_exec($ch);
    curl_close($ch);
    $r = json_decode($r, true);


    $col = 0;
    echo "<div id='live_list'>";
    foreach($r["streams"] as $s) {
        if ($col == $cols) echo "<br>";
        $id = $s["channel"]["_id"];
        ?>
        <div class="live_content" id="<?php echo $id; ?>_content">
            <a href="<?php echo $s["channel"]["url"] ?>" target="_blank">
                <div class="live_preview">
                    <img class="responsive-width" id="<?php echo $id; ?>_preview" src="<?php echo $s["preview"]["medium"]; ?>">
                </div>
                <div class="live_title">
                    <span class="name"><?php echo $s["channel"]["display_name"]; ?></span> |
                    <span class="status"><?php echo $s["channel"]["status"]; ?></span> <br>
                    <span class="followers"
                          id="<?php echo $id; ?>_followers">
                        <img src="https://cdn0.iconfinder.com/data/icons/admin-panel-glyph-black/2048/599_-_Subscribers-512.png" width="16" height="16">
                        <?php echo $s["channel"]["followers"]; ?></span> |
                        <span class="views" id="<?php echo $id; ?>_views">
                            <img src="https://cdn3.iconfinder.com/data/icons/faticons/32/view-01-128.png" width="16" height="16">
                            <?php echo $s["channel"]["views"]; ?></span> |
                        <span class="viewers" id="<?php echo $id; ?>_viewers">
                            <img src="https://lh4.ggpht.com/Smwvtf8W3996Rz6KaMQxAFpY9Dk-r3cGAy0D_Me3-AzUt9oJ07ZHFUbXPN5ErkoU2A=w300" width="15" height="16">
                            <?php echo $s["viewers"]; ?></span>
                </div>
            </a>
        </div>
        <?php
    }echo "</div>";
    ?>
    <script>
        function r() {
            $("#live_list").load("?passive&cols=<?php echo $cols; ?>");

            console.log("LiveBoxes updated");
            setTimeout(r, <?php echo $_GET["window-refresh"] * 1000; ?>);
        }

        setTimeout(r, <?php echo $_GET["window-refresh"] * 1000; ?>);
    </script>
    <style>
        body {
            font-family: Arial;
        }

        a, a:active {
            color: black;
            text-decoration: none;
        }

        .live_content {
            cursor: pointer;
        }

        .responsive-width {
            width: <?php echo $_GET["livebox-width"]; ?>;
        }

        .responsive-height {
            height: <?php echo $_GET["livebox-height"]; ?>;
        }

        .viewers {
            color: red;
        }

        #live_list {
            width: <?php echo $_GET["livebox-width"] * $cols; ?>;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        #live_list .live_content {
            width: <?php echo $_GET["livebox-width"]; ?>;
            height: <?php echo $_GET["livebox-height"]; ?>;
            float: left;
        }
    </style>
    </body>
    </html>
    <?php
}
?>
