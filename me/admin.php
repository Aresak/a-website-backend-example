<?php

define("_D", "../");
require_once _D . "header.php";


if (!_Logged) {
    // Logged only page
    header("Location: " . _SITE_ . "?err=invalid_token");
}
if ($account->Role()->ID() != 3) {
    // Administrators only page
    header("Location: " . _SITE_ . "?err=not_admin");
}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-67750513-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-67750513-1');
    </script>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DragonsGetIt.Com</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo _SITE_; ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo _SITE_; ?>/css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo _SITE_; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon"
          href="https://cdn.discordapp.com/attachments/189091483217821696/386006553926303744/whoslive_small25x25.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="<?php echo _SITE_; ?>/js/jquery.js"></script>
    <script src="<?php echo _SITE_; ?>/js/jquery-ui.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo _SITE_; ?>/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
    </script>

    <?php

    Aresak::Meta();             // Write Aresak META

    ?>
</head>

<body>
<?php

Aresak::Body();             // Write Aresak BODY meta

?>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo _SITE_; ?>/index.php">DragonsGetIt</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo _SITE_; ?>/index.php">Home</a>
                </li>
                <li>
                    <a href="<?php echo _SITE_; ?>/whoarewe.php">Who are we?</a>
                </li>
                <li>
                    <a href="<?php echo _SITE_; ?>/social-requests.php">Social Requests</a>
                </li>
                <?php

                Aresak::NavBar($account);

                ?>
            </ul>
        </div>

        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Header Carousel -->
<header id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill"
                 style="background-image:url('https://wallpaper.wiki/wp-content/uploads/2017/05/Youtube-Logo-Wallpapers.png');"></div>
            <div class="carousel-caption">
                <h3>YouTube Support</h3>
            </div>
        </div>
        <div class="item">
            <div class="fill"
                 style="background-image:url('https://i.ytimg.com/vi/1qN8l6WoZQI/maxresdefault.jpg');"></div>
            <div class="carousel-caption">
                <h3>Twitch Support</h3>
            </div>
        </div>
        <div class="item">
            <div class="fill"
                 style="background-image:url('http://donthatethegeek.com/wp-content/uploads/2016/05/gaming-28646-1680x1050.jpg');"></div>
            <div class="carousel-caption">
                <h3>Community Building</h3>
            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>

<!-- Page Content -->
<div class="container-fluid">
    <div class="row">
        <ul class="nav nav-pills">
            <li role="presentation" target="overall" id="admin-item-overall" class="admin-menu active"><a href="#">Overall</a></li>
            <li role="presentation" target="twitch-view" id="admin-item-twitch-view" class="admin-menu"><a href="#">Twitch View</a></li>
            <li role="presentation" target="youtube-view" id="admin-item-youtube-view" class="admin-menu"><a href="#">YouTube View</a></li>
            <li role="presentation" target="dgi-bot" id="admin-item-dgi-bot" class="admin-menu"><a href="#">DGI Bot</a></li>
        </ul>
    </div>
    <div class="row">
        <div id="overall">
            <h1>Overall admin</h1>
            <h2>Members</h2>
            <table class="table table-striped" id="members">
                <tr><td><h3>Loading members...</h3></td></tr>
            </table>
            <hr>
            <h2>Roles</h2>

            <div id="create-role">
                Display Name: <input type="text" id="cr-displayName" placeholder="Display Name">
                Color: <input type="color" id="cr-color" placeholder="Color">
                <button onclick="createRole();">Create a new role</button>
            </div>
            <table class="table table-striped" id="roles">
                <tr><td><h3>Loading roles...</h3></td></tr>
            </table>
        </div>

        <div id="twitch-view" style="display: none;">
            <h1>Twitch View admin</h1>
            <table id="list" class="table">
                <tr>
                    <td>
                        <button onclick="twitch.savePriority();" style="position: relative; width: 100%; left: 0;">
                            SAVE!
                        </button>
                    </td><td></td>
                </tr>
                <tr>
                    <td align="center"><b>PRIORITIZED STREAMS</b></td>
                    <td align="center"><b>UNSORTED STREAMS</b></td>
                </tr>
                <tr>
                    <td align="right">
                        <ul id="priority" class="connectedSortable">
                            <li class="tw" id="dragonsgetit" class="prioritized permanent">Loading...</li>
                        </ul>
                    </td>
                    <td align="left">
                        <ul id="streamers" class="connectedSortable">
                            <li class="tw" id="dragonsgetit" class="permanent">Loading...</li>
                        </ul>
                    </td>
                </tr>
            </table>

            <textarea style="width: 100%"><iframe style="width: 100%; height: 800px;" src="http://dragonsgetit.com/ares/TwitchView/view.php?iframe-width=100%&window-refresh=1&iframe-height=100%&livebox-width=322px&livebox-height=280px&cols=5&livebox-max=0"></iframe></textarea>
            <iframe style="width: 100%; height: 800px;" src="http://dragonsgetit.com/ares/TwitchView/view.php?iframe-width=100%&window-refresh=30&iframe-height=100%&livebox-width=322px&livebox-height=280px&cols=5&livebox-max=0"></iframe>
        </div>

        <div id="youtube-view" style="display: none;">
            <h1>YouTube View admin</h1>
            <h2>Fetch playlists</h2>
            You can fetch playlists for new users here.<br>
            <button onclick="youtube.fetchPlaylists()">Fetch Playlists</button><br>
            <table class="table table-striped" id="yt-fetched">
                <tr>
                    <td><b>Video ID</b></td>
                    <td><b>Video Title</b></td>
                    <td><b>Video Publish Date</b></td>
                </tr>
            </table>
            <h2>Playlists</h2>
            <table class="table table-striped" id="yt-playlists"><tr><td>Loading...</td></tr></table>
            <h2>Videos</h2>
            <table class="table table-striped" id="yt-videos"><td><td>Loading...</td></table>

            <br><br>
            <textarea style="width: 100%;"><iframe style="width: 100%; height: 800px;" src="http://dragonsgetit.com/ares/YouTubeView/?port=base"></iframe></textarea>
            <iframe style="width: 100%; height: 800px;" src="http://dragonsgetit.com/ares/YouTubeView/?port=base"></iframe>
        </div>

        <div id="dgi-bot" style="display: none;">
            <?php
            $cfg = \GuzzleHttp\json_decode( file_get_contents(_SITE_ . "/ares/bot/config.json"), true );
            ?>
            <h1>DGI Bot admin panel</h1>
            <div id="dgi-bot-settings">
                <h2 align="center">Settings</h2>
                <table class="table table-striped" id="dgi-bot-settings-tbl">
                    <tr>
                        <td><b>Name</b></td><td><b>Value</b></td><td><b>Description</b></td>
                    </tr>
                    <tr>
                        <td>Coins \ Processor</td>
                        <td><input type="number" id="bot-coins-processor" value="<?php echo $cfg["coins"]["processor"]; ?>"></td>
                        <td>How much coins gets the host who is currently processing the request</td>
                    </tr>
                    <tr>
                        <td>Coins \ Queue</td>
                        <td><input type="number" id="bot-coins-queue" value="<?php echo $cfg["coins"]["queue"]; ?>"></td>
                        <td>How much coins gets the host in the queue to become the Processor</td>
                    </tr>
                    <tr>
                        <td>Coins \ Messenger</td>
                        <td><input type="number" id="bot-coins-messenger" value="<?php echo $cfg["coins"]["messenger"]; ?>"></td>
                        <td>How much coins gets the viewer for chatting in the stream each check</td>
                    </tr>
                    <tr>
                        <td>Times \ Check</td>
                        <td><input type="number" id="bot-times-check" value="<?php echo $cfg["times"]["check"]; ?>"></td>
                        <td>How long is each user's check. This is the time the viewer if active, he gets coins</td>
                    </tr>
                    <tr>
                        <td>Times \ Dead</td>
                        <td><input type="number" id="bot-times-dead" value="<?php echo $cfg["times"]["dead"]; ?>"></td>
                        <td>What is the timeout for processor in the pulse. If this time will take longer,<br>
                            someone else will become processor</td>
                    </tr>
                    <tr>
                        <td>Times \ Pulse</td>
                        <td><input type="number" id="bot-times-pulse" value="<?php echo $cfg["times"]["pulse"]; ?>"></td>
                        <td>This is the time the host pulses into our service for updates, also keeps the host alive</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button onclick="bot.saveSettings()">Update Bot Config</button></td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <br>
            <div id="dgi-bot-hosts">
                <h2 align="center">Hosts</h2>
                <table class="table table-striped" id="dgi-bot-hosts-table">
                    <tr><td>Loading...</td></tr>
                </table>
            </div>
            <br>
            <div id="dgi-bot-streams">
                <h2 align="center">Online Streams</h2>
                <table class="table table-striped" id="dgi-bot-streams-table">
                    <tr><td>Loading...</td></tr>
                </table>
            </div>
            <br>
            <div id="dgi-bot-viewers">
                <h2 align="center">Viewers</h2>
                <table class="table table-striped" id="dgi-bot-viewers-table">
                    <td><td>Loading...</td>
                </table>
            </div>
            <br>
            <div id="dgi-bot-debugger">
                <h2 align="center">Debugger</h2>
                <table class="table table-striped">
                    <tr>
                        <td align="right"><button onclick="bot.removeAllViewers()">Remove All Viewers</button></td>
                        <td><button onclick="bot.removeAllStreams()">Remove All Streams</button></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Aiden Teran 2017</p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

</body>

</html>
<script>
    var panelOverall = $("#overall");
    var panelTwitchView = $("#twitch-view");
    var panelYouTubeView = $("#youtube-view");
    var panelDGIBot = $("#dgi-bot");
    var activePanel = "overall";
    var callingAJAX = false;

    $(".admin-menu").click(function() {
        var t = $(this).attr("target");
        if(t == activePanel)
            return;

        $("#admin-item-" + activePanel).removeClass("active");
        $("#admin-item-" + t).addClass("active");

        $("#" + activePanel).hide();
        $("#" + t).show();
        console.log("Switched from tab " + activePanel + " to " + t);
        activePanel = t;

        switch(t) {
            case "twitch-view":
                twitch.onOpen();
                break;
            case "youtube-view":
                youtube.onOpen();
                break;
            case "dgi-bot":
                bot.onOpen();
                break;
        }
    });

    $( function() {
        $( "#priority, #streamers" ).sortable({
            connectWith: ".connectedSortable",
            cancel: ".permanent",
            placeholder: "highlighted",
            change: function( event, ui ) {

            }
        }).disableSelection();
    } );

    var bot = {
        opened: false,
        onOpen: function() {
            if(this.opened)
                return;

            this.loadAll();

            this.opened = true;
        },
        saveSettings: function() {
            if(callingAJAX) {
                setTimeout(bot.saveSettings, 20);
                return;
            } else
                callingAJAX = true;

            var coins_processor = $("#bot-coins-processor").val();
            var coins_queue = $("#bot-coins-queue").val();
            var coins_messenger = $("#bot-coins-messenger").val();

            var time_check = $("#bot-time-check").val();
            var time_pulse = $("#bot-time-pulse").val();
            var time_dead = $("#bot-time-dead").val();

            $.get("ajax.php", {admin: true, method: "bot_saveCfg", processor: coins_processor, queue: coins_queue,
            messenger: coins_messenger, check: time_check, pulse: time_pulse, dead: time_dead}, function(data) {
                console.log("[BOT] Config saved.");
                callingAJAX = false;
            }, "html");
        },
        removeAllViewers: function() {
            if(callingAJAX) {
                setTimeout(bot.removeAllViewers, 20);
                return;
            } else
                callingAJAX = true;

            $.get("ajax.php", {admin: true, method: "bot_removeViewers"}, function(data) {
                console.log("[BOT] Viewers removed.");
                callingAJAX = false;
            }, "html");
        },
        removeAllStreams: function() {
            if(callingAJAX) {
                setTimeout(bot.removeAllStreams, 20);
                return;
            } else
                callingAJAX = true;

            $.get("ajax.php", {admin: true, method: "bot_removeStreams"}, function(data) {
                console.log("[BOT] Streams removed.");
                callingAJAX = false;
            }, "html");
        },
        loadStreams: function() {
            if(callingAJAX) {
                setTimeout(bot.loadStreams, 20);
                return;
            } else
                callingAJAX = true;

            $.get("ajax.php", {admin: true, method: "bot_loadStreams"}, function(data) {
                $("#dgi-bot-streams-table").html(data);
                console.log("[BOT] Streams loaded.");
                callingAJAX = false;
            }, "html");
        },
        loadViewers: function() {
            if(callingAJAX) {
                setTimeout(bot.loadViewers, 20);
                return;
            } else
                callingAJAX = true;

            $.get("ajax.php", {admin: true, method: "bot_loadViewers"}, function(data) {
                $("#dgi-bot-viewers-table").html(data);
                console.log("[BOT] Viewers loaded.");
                callingAJAX = false;
            }, "html");
        },
        loadHosts: function() {
            if(callingAJAX) {
                setTimeout(bot.loadHosts, 20);
                return;
            } else
                callingAJAX = true;

            $.get("ajax.php", {admin: true, method: "bot_loadHosts"}, function(data) {
                $("#dgi-bot-hosts-table").html(data);
                console.log("[BOT] Hosts loaded.");
                callingAJAX = false;
            }, "html");
        },
        loadAll: function() {
            if(callingAJAX) {
                setTimeout(bot.loadAll, 20);
                return;
            } else
                callingAJAX = true;

            $.get("ajax.php", {admin: true, method: "bot_loadStreams"}, function(data) {
                $("#dgi-bot-streams-table").html(data);
                console.log("[BOT] Streams loaded.");
                $.get("ajax.php", {admin: true, method: "bot_loadViewers"}, function(data) {
                    $("#dgi-bot-viewers-table").html(data);
                    console.log("[BOT] Viewers loaded.");
                    $.get("ajax.php", {admin: true, method: "bot_loadHosts"}, function(data) {
                        $("#dgi-bot-hosts-table").html(data);
                        console.log("[BOT] Hosts loaded.");
                        callingAJAX = false;
                    }, "html")
                }, "html");
            }, "html");
        }
    };

    var twitch = {
        opened: false,
        savePriority: function() {
            if(callingAJAX) {
                setTimeout(twitch.savePriority, 20);
                return;
            } else
                callingAJAX = true;

            if(confirm("Are you sure you want to save new priority list?")) {
                var list = $( ".connectedSortable" ).sortable( "toArray" );
                var strigified = "";
                for(var i = 0; i < list.length; i ++) {
                    strigified += list[i] + ":";
                }

                $.get("ajax.php", {admin: true, list: strigified, method: "savePriority"}, function(data) {
                    callingAJAX = false;
                    if(data != "success")
                        console.log(data);
                    else
                        console.log("New priority list saved successfully.");
                }, "html");
            }
        },
        sortText: function() {
            console.log($( ".connectedSortable" ).sortable( "toArray" ));
        },
        loadStreams: function() {
            if(callingAJAX) {
                setTimeout(twitch.loadStreams, 20);
                return;
            } else
                callingAJAX = true;

            $.get("ajax.php", {admin: true, method: "getTwitchPriorityList"}, function(data) {
                callingAJAX = false;
                console.log("Priority list loaded");
                var p = JSON.parse(data);

                $("#streamers").html("");
                $("#priority").html("");

                for(var i = 0; i < p.priority_size; i ++) {
                    console.log(p.priority[i]);
                    $("#priority").append("<li class=\"tw\" id=\"" + p.priority[i] + "\" class=\"prioritized\">" + p.priority[i] + "</li>");
                }

                for(var i = 0; i < p.pleb.length; i ++) {
                    $("#streamers").append("<li class=\"tw\" id=\"" + p.pleb[i] + "\">" + p.pleb[i] + "</li>");
                }
            }, "html");
        },
        onOpen: function() {
            if(!this.opened) {
                this.opened = true;
                this.loadStreams();
            }
        }
    };

    var youtube = {
        opened: false,
        onOpen: function() {
            if(!this.opened) {
                if(callingAJAX) {
                    setTimeout(this.onOpen, 20);
                    return;
                } else
                    callingAJAX = true;

                this.opened = true;

                $.get("ajax.php", {admin: true, method: "show_playlists"}, function(data) {
                    $("#yt-playlists").html(data);
                    console.log("Playlists loaded");

                    $.get("ajax.php", {admin: true, method: "show_videos"}, function(data) {
                        $("#yt-videos").html(data);
                        console.log("Videos loaded");
                        callingAJAX = false;
                    })
                }, "html");
            }
        },
        loadPlaylists: function() {
            if(callingAJAX) {
                setTimeout(this.onOpen, 20);
                return;
            } else
                callingAJAX = true;

            $.get("ajax.php", {admin: true, method: "show_playlists"}, function(data) {
                $("#yt-playlists").html(data);
                console.log("Playlists loaded");
                callingAJAX = false;
            }, "html");
        },
        loadVideos: function() {
            if(callingAJAX) {
                setTimeout(this.onOpen, 20);
                return;
            } else
                callingAJAX = true;

            $.get("ajax.php", {admin: true, method: "show_videos"}, function(data) {
                $("#yt-videos").html(data);
                console.log("Videos loaded");
                callingAJAX = false;
            }, "html");
        },
        lockPlaylist: function(x) {
            if(callingAJAX) {
                setTimeout(this.onOpen, 20);
                return;
            } else
                callingAJAX = true;

            $.get("ajax.php", {admin: true, method: "lock-pl", id: x}, function(data) {
                console.log("Playlists " + x + " locked");
                callingAJAX = false;
            }, "html");
        },
        fetchPlaylists: function(x) {
            if(callingAJAX) {
                setTimeout(this.onOpen, 20);
                return;
            } else
                callingAJAX = true;

            $.get("ajax.php", {admin: true, method: "fetch-playlists"}, function(data) {
                console.log("Playlists fetched");
                callingAJAX = false;

                var json = JSON.parse(data);
                for(var channel in json) {
                    if(json[channel].status) {
                        console.log("Playlist " + id + " fetched successfully. Videos: " + json[channel].videos_count);

                        for(var video in json[channel].videos) {
                            var row = "<tr>";
                            row += "<td>" + json[channel].videos[video].id + "</td>";
                            row += "<td>" + json[channel].videos[video].title + "</td>";
                            row += "<td>" + json[channel].videos[video].published + "</td>";
                            row += "</tr>";
                            $("#yt-fetched").append(row);
                        }
                    } else
                        console.log("Playlist " + channel + " fetched in error " + json[channel].error);
                }

            }, "html");
        }
    };


    function createRole() {
        if(callingAJAX) {
            setTimeout(createRole, 20);
            return;
        } else
            callingAJAX = true;

        var crd = $("#cr-displayName");
        var crc = $("#cr-color");
        var name = crd.val();
        var color = crc.val().substr(1, 6);

        if (name.length < 3) {
            alert("The name of the role is too short. Minimum is 3 chars.");
            return;
        }

        crd.attr("disabled", true);
        crc.attr("disabled", true);

        $.get("ajax.php", {admin: true, method: "createRole", name: name, color: color}, function(data) {
            callingAJAX = false;
            if(data != "success") {
                console.log(data);
            }
            else {
                crd.val("");
                crd.attr("disabled", false);
                crc.attr("disabled", false);

                refreshRoles();
            }
        }, "html");
    }

    function refreshRoles() {
        if(callingAJAX) {
            setTimeout(refreshRoles, 20);
            return;
        } else
            callingAJAX = true;

        $.get("ajax.php", {admin: true, method: "drawRoles"}, function(data) {
            callingAJAX = false;
            $("#roles").html(data);
        }, "html");
    }

    function refreshMembers() {
        if(callingAJAX) {
            setTimeout(refreshMembers, 20);
            return;
        } else
            callingAJAX = true;

        $.get("ajax.php", {admin: true, method: "drawMembers"}, function(data) {
            callingAJAX = false;
            $("#members").html(data);
        }, "html");
    }

    function deleteRole(x) {
        if(callingAJAX) {
            setTimeout(deleteRole(x), 20);
            return;
        } else
            callingAJAX = true;

        $.get("ajax.php", {admin: true, method: "deleteRole", id: x}, function(data) {
            callingAJAX = false;
            if(data != "success") {
                console.log(data);
            }
            else {
                refreshRoles();
            }
        }, "html");
    }

    function startUpdateRole(x) {
        var rrn = $("#rr-" + x + "-name");
        var rrd = $("#rr-" + x + "-displayName");
        var rrc = $("#rr-" + x + "-color");
        var rrb = $("#rr-" + x + "-btn");

        rrn.html( "<input type='text' id='rr-" + x + "-vname' value='" + rrn.text() +  "'>" );
        rrd.html( "<input type='text' id='rr-" + x + "-vdisplayName' value='" + rrd.text() +  "'>" );
        rrc.html( "<input type='color' id='rr-" + x + "-vcolor' value='" + rrc.text() +  "'>" );
        rrb.html( "<button onclick='updateRole(\"" + x + "\");'>Update</button> "
                + "<button onclick='deleteRole(\"" + x + "\");'>Delete</button>");
    }

    function updateRole(x) {
        if(callingAJAX) {
            setTimeout(updateRole(x), 20);
            return;
        } else
            callingAJAX = true;

        var name = $("#rr-" + x + "-vname").val();
        var displayName = $("#rr-" + x + "-vdisplayName").val();
        var color = $("#rr-" + x + "-vcolor").val();
        $("#rr-" + x + "-btn").attr("disabled", true);

        $.get("ajax.php", {admin: true, method: "updateRole", id: x, name: name, displayName: displayName, color: color.substr(1, 6)}, function(data) {
            callingAJAX = false;
            if(data != "success") {
                console.log(data);
            } else {
                refreshRoles();
            }
        }, "html");
    }

    function unrollUser(x) {
        var baseRow = $("#u" + x);
        var displayName = $("#u" + x + "-displayName");
        var email = $("#u" + x + "-email");
        var role = $("#u" + x + "-role");
        var dob = $("#u" + x + "-dob");
        var location = $("#u" + x + "-location");
        var avatar = $("#u" + x + "-avatar");


        var roleSelector = "<select id='u" + x + "-v-role'>";

        for(var i = 0; i < roles.length; i ++) {
            roleSelector += "<option value='" + roles[i].ID + "' " + (roles[i].ID == role.attr("role") ? "selected" : "") + ">" + roles[i].display_name + "</option>";
        }

        roleSelector += "</select>";

        var update = "<tr style='background-color: rgba(217, 219, 159, 1);'>"
                    + "<td><input type='text' id='u" + x + "-v-displayName' value='" + displayName.text() + "'></td>"
                    + "<td><input type='email' id='u" + x + "-v-email' value='" + email.text() +"'></td>"
                    + "<td>" + roleSelector + "</td>"
                    + "<td><input type='date' id='u" + x + "-v-dob' value='" + dob.text() + "'></td>"
                    + "<td><input type='text' id='u" + x + "-v-location' value='" + location.text() + "'></td>"
                    + "<td><input type='text' id='u" + x + "-v-avatar' value='" + avatar.attr("avatar") + "'></td>"
                    + "<td><button onclick='updateUser(\"" + x + "\");'>Update</button></td>"
                    + "<td><button onclick='deleteUser(\"" + x + "\");'>Delete</button></td>"
                    + "</tr>";
        baseRow.after(update);
    }

    function updateUser(x) {
        if(callingAJAX) {
            setTimeout(updateUser(x), 20);
            return;
        } else
            callingAJAX = true;

        var displayName = $("#u" + x + "-v-displayName").val();
        var email = $("#u" + x + "-v-email").val();
        var role = $("#u" + x + "-v-role").val();
        var dob = $("#u" + x + "-v-dob").val();
        var location = $("#u" + x + "-v-location").val();
        var avatar = $("#u" + x + "-v-avatar").val();

        $.get("ajax.php", {admin: true, method: "updateUser", id: x, displayName: displayName,
                email: email, role: role, dob: dob, location: location, avatar: avatar}, function(data) {
            callingAJAX = false;
            if(data != "success")
                console.log(data);
            else
                refreshMembers();
        }, "html");
    }

    function deleteUser(x) {
        if(callingAJAX) {
            setTimeout(deleteUser(x), 20);
            return;
        } else
            callingAJAX = true;

        $.get("ajax.php", {admin: true, method: "deleteUser", id: x}, function(data) {
            callingAJAX = false;
            if(data != "success")
                console.log(data);
            else
                refreshMembers();
        }, "html");
    }

    var roles = [];

    setTimeout(function() {
        $.get("ajax.php", {admin: true, method: "drawRoles"}, function(data) {
            $("#roles").html(data);
            $.get("ajax.php", {admin: true, method: "drawMembers"}, function(data) {
                $("#members").html(data);
                $.get("ajax.php", {admin: true, method: "getAllRoles"}, function(data) {
                    roles = JSON.parse(data);
                }, "html");
            }, "html");
        }, "html");
    }, 500);
</script>
<style>
    .tw {
        padding: 2px 5px;
        border: 1px solid black;
        background-color: rgba(229, 227, 215, 1);
    }

    .highlighted {
        background-color: rgba(222, 229, 160, 1);
    }
</style>