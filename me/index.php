<?php

define("_D", "../");
require_once _D . "header.php";


if(!_Logged) {
    // Logged only page
    header("Location: " . _SITE_ . "?err=invalid_token");
}

$yn = new YouTube($sql);
$yn->Create_Client();

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-67750513-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-67750513-1');
    </script>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DragonsGetIt.Com</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo _SITE_; ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo _SITE_; ?>/css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo _SITE_; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="https://cdn.discordapp.com/attachments/189091483217821696/386006553926303744/whoslive_small25x25.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="<?php echo _SITE_; ?>/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo _SITE_; ?>/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
    </script>

    <?php

    Aresak::Meta();             // Write Aresak META

    ?>
</head>

<body>
<?php

Aresak::Body();             // Write Aresak BODY meta

?>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo _SITE_; ?>/index.php">DragonsGetIt</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo _SITE_; ?>/index.php">Home</a>
                </li>
                <li>
                    <a href="<?php echo _SITE_; ?>/whoarewe.php">Who are we?</a>
                </li>
                <li>
                    <a href="<?php echo _SITE_; ?>/social-requests.php">Social Requests</a>
                </li>
                <?php

                Aresak::NavBar($account);

                ?>
            </ul>
        </div>

        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Header Carousel -->
<header id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill" style="background-image:url('https://wallpaper.wiki/wp-content/uploads/2017/05/Youtube-Logo-Wallpapers.png');"></div>
            <div class="carousel-caption">
                <h3>YouTube Support</h3>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('https://i.ytimg.com/vi/1qN8l6WoZQI/maxresdefault.jpg');"></div>
            <div class="carousel-caption">
                <h3>Twitch Support</h3>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('http://donthatethegeek.com/wp-content/uploads/2016/05/gaming-28646-1680x1050.jpg');"></div>
            <div class="carousel-caption">
                <h3>Community Building</h3>
            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>

<!-- Page Content -->
<div class="container-fluid">

    <div class="row" id="bio">
        <div id="val-bio">
            <?php

            if($account->Bio() == "") {
                echo "<i>You have not set yourself a bio yet. Do it with the button bellow :)</i>";
            } else {
                echo $account->Bio();
            }

            ?>
        </div>

        <div id="in-bio">
            <span class="cursor" onclick="edit.bio();"><span class="glyphicon glyphicon-pencil""></span> Edit bio</span>
        </div>
    </div>
    <div class="row">

        <table class="table table-striped">
            <tr>
                <td align="right">
                    <b>Username:</b>
                </td>
                <td align="left">
                    <i><?php echo $account->Username(); ?></i>
                </td>
                <td></td>
            </tr>

            <tr>
                <td align="right">
                    <b>Display Name:</b>
                </td>
                <td align="left" id="val-displayName">
                    <?php echo $account->Display_Name(); ?>
                </td>
                <td id="in-displayName">
                    <span class="cursor" onclick="edit.val('displayName');"><span class="glyphicon glyphicon-pencil"></span> Edit</span>
                </td>
            </tr>

            <tr>
                <td align="right">
                    <b>Password:</b>
                </td>
                <td align="left" id="val-password">
                    **********
                </td>
                <td id="in-password">
                    <span class="cursor" onclick="edit.val('password');"><span class="glyphicon glyphicon-pencil"></span> Edit</span>
                </td>
            </tr>

            <tr>
                <td align="right">
                    <b>Date of Birth:</b>
                </td>
                <td align="left" id="val-dob">
                    <?php echo $account->DateOfBirth(); ?>
                </td>
                <td id="in-dob">
                    <span class="cursor" onclick="edit.val('dob');"><span class="glyphicon glyphicon-pencil"></span> Edit</span>
                </td>
            </tr>

            <tr>
                <td align="right">
                    <b>Location:</b>
                </td>
                <td align="left" id="val-location">
                    <?php echo $account->Location(); ?>
                </td>
                <td id="in-location">
                    <span class="cursor" onclick="edit.val('location');"><span class="glyphicon glyphicon-pencil"></span> Edit</span>
                </td>
            </tr>

            <tr>
                <td align="right">
                    <b>Avatar:</b>
                </td>
                <td align="left" id="val-avatar">
                    <img src="<?php echo $account->Avatar(); ?>" width="22" height="22">
                </td>
                <td id="in-avatar">
                    <span class="cursor" onclick="edit.val('avatar');"><span class="glyphicon glyphicon-pencil"></span> Edit</span>
                </td>
            </tr>

            <tr>
                <td align="right">
                    <b>Twitch:</b>
                </td>
                <td align="left" id="val-twitch">
                    <?php

                    $twitch     = new Twitch($sql);
                    if($twitch->CreateFrom_Account($account)) {
                        $d  = explode("||", $twitch->extras);
                        echo "Connected - <img class='avatar' src='" . $d[1] . "'> " . $d[0];
                    } else {
                        echo "Not connected - <a href='" . _SITE_ . "/ares/director.php?l=" . urlencode($twitch->GenerateAuthLink(array(
                                "user_read",
                                "user_subscriptions",
                                "channel_subscriptions"
                            ), $account)) . "&r=" . _SITE_ . "/ares/me/'>Connect Twitch</a>";
                    }

                    ?>
                </td>
                <td>
                    <?php
                    if($twitch->Initialized()) {
                        echo "<a href='#' onclick='edit.updateFromTwitch()'>Update from twitch</a>";
                    }
                    ?>
                </td>
            </tr>

            <tr>
                <td align="right">
                    <b>YouTube:</b>
                </td>
                <td align="left">
                    <?php
                    $youtube    = new YouTube($sql);
                    if($youtube->CreateFrom_Account($account)) {
                        $d      = explode("||", $youtube->extras);
                        echo "Connected - <img class='avatar' src='" . $d[1] . "'> " . $d[0];
                    } else {
                        echo "Not connected - <a href='" . _SITE_ . "/ares/director.php?l=" . urlencode($yn->Client()->createAuthUrl())
                           . "&r=" . _SITE_ . "/ares/me'>Connect YouTube</a>";
                    }

                    ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Aiden Teran 2017</p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

</body>

</html>
