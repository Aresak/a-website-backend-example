<?php

if(isset($_GET["method"])) {
    $method = $_GET["method"];

    define("_D", "../");
    require_once _D . "header.php";

    if(!_Logged)
        die("Error: User not logged in");

    if(isset($_GET["admin"])) {
        // /me/admin.php ajax
        if($account->Role()->ID() != 3)
            die("Error: User is not administrator");

        switch($method) {
            case "createRole":
                if(isset($_GET["color"]))
                    $color = $_GET["color"];
                else
                    die("Error: Missing parameter field in the request");

                if(isset($_GET["name"]))
                    $name = $_GET["name"];
                else
                    die("Error: Missing parameter field in the request");

                $role = Role::CreateNew($sql, $name, $color);
                die("success");
                break;

            case "drawRoles":
                $roles = Role::GetAllRoles($sql);
                echo "<tr>";
                ?>
                <td><b>IDentifier Name</b></td>
                <td><b>Display Name</b></td>
                <td><b>Color</b></td>
                <td><b>Actions</b></td>
                <?php
                echo "</tr>";
                foreach($roles as $role) {
                    echo "<tr id='rr-" . $role->ID() . "'>";
                    echo "<td id='rr-" . $role->ID() . "-name'>" . $role->Name() . "</td>";
                    echo "<td id='rr-" . $role->ID() . "-displayName'>" . $role->DisplayName() . "</td>";
                    echo "<td id='rr-" . $role->ID() . "-color'><span style='color: #" . $role->Color() . "'>#" . $role->Color() . "</span></td>";
                    echo "<td id='rr-" . $role->ID() . "-btn'><button onclick='startUpdateRole(\"" . $role->ID() . "\");'>Change</button></td>";
                    echo "</tr>";
                }
                break;

            case "drawMembers":
                $users = Account::GetAllMembers($sql);

                ?>
                <tr>
                    <td><b>Name</b></td>
                    <td><b>Email</b></td>
                    <td><b>Role</b></td>
                    <td><b>Date of Birth</b></td>
                    <td><b>Location</b></td>
                    <td><b>Avatar</b></td>
                    <td><b>Is activated</b></td>
                    <td><b>Twitch Name</b></td>
                    <td><b>YouTube Name</b></td>
                </tr>
                <?php

                foreach($users as $user) {
                    $id = $user->ID();
                    echo "<tr class='user' id='u$id' onclick='unrollUser(\"$id\");'>";
                    echo "<td id='u$id-displayName'>" . $user->Display_Name() . "</td>";
                    echo "<td id='u$id-email'>" . $user->Email() . "</td>";
                    echo "<td id='u$id-role' role='" . $user->Role()->ID() . "' style='color: #" . $user->Role()->Color() . "'>" . $user->Role()->DisplayName() . "</td>";
                    echo "<td id='u$id-dob'>" . $user->DateOfBirth() . "</td>";
                    echo "<td id='u$id-location'>" . $user->Location() . "</td>";
                    echo "<td id='u$id-avatar' avatar='" . $user->Avatar() . "'><img src='" . $user->Avatar() . "' class='avatar'></td>";
                    echo "<td>" . ($user->Activated() ? "Yes" : "No") . "</td>";
                    $twitch = new Twitch($sql);
                    $twitch->CreateFrom_Account($user);
                    $tu = explode("||", $twitch->extras);
                    echo "<td>" . $tu[0] . "</td>";
                    $youtube = new YouTube($sql);
                    $youtube->CreateFrom_Account($user);
                    $yu = explode("||", $youtube->extras);
                    echo "<td>" . $yu[0] . "</td>";
                    echo "</tr>";
                }
                break;

            case "updateRole":
                if(isset($_GET["id"]))
                    $id = $_GET["id"];
                else
                    die("Error: Missing parameter field in the request");

                if(isset($_GET["name"]))
                    $name = $_GET["name"];
                else
                    die("Error: Missing parameter field in the request");

                if(isset($_GET["displayName"]))
                    $displayName = $_GET["displayName"];
                else
                    die("Error: Missing parameter field in the request");

                if(isset($_GET["color"]))
                    $color = $_GET["color"];
                else
                    die("Error: Missing parameter field in the request");


                $name = strtolower(strtr($name, array(" " => "_")));

                $role = new Role($sql, $id);
                $role->Name($name);
                $role->DisplayName($displayName);
                $role->Color($color);
                die("success");
                break;
            case "deleteRole":
                if(isset($_GET["id"]))
                    $id = $_GET["id"];
                else
                    die("Error: Missing parameter field in the request");


                $query      = "DELETE FROM ares_roles WHERE ID='$id'";
                $result     = mysqli_query($sql, $query)
                    or die(mysqli_error($sql));
                die("success");
                break;
            case "getAllRoles":
                $roles = Role::GetAllRoles($sql);

                $arr = array();
                for($i = 0; $i < count($roles); $i ++) {
                    $arr[$i]["ID"] = $roles[$i]->ID();
                    $arr[$i]["name"] = $roles[$i]->Name();
                    $arr[$i]["display_name"] = $roles[$i]->DisplayName();
                    $arr[$i]["color"] = $roles[$i]->Color();
                }
                die(json_encode($arr));
                break;
            case "deleteUser":
                if(isset($_GET["id"]))
                    $id = $_GET["id"];
                else
                    die("Error: Missing parameter field in the request");

                $query      = "DELETE FROM ares_accounts WHERE ID='$id'";
                $result     = mysqli_query($sql, $query)
                    or die(mysqli_error($sql));
                die("success");
                break;
            case "updateUser":
                if(isset($_GET["id"]))
                    $id = $_GET["id"];
                else
                    die("Error: Missing parameter field in the request");

                if(isset($_GET["displayName"]))
                    $displayName = $_GET["displayName"];
                else
                    die("Error: Missing parameter field in the request");

                if(isset($_GET["email"]))
                    $email = $_GET["email"];
                else
                    die("Error: Missing parameter field in the request");

                if(isset($_GET["role"]))
                    $role = $_GET["role"];
                else
                    die("Error: Missing parameter field in the request");

                if(isset($_GET["dob"]))
                    $dob = $_GET["dob"];
                else
                    die("Error: Missing parameter field in the request");

                if(isset($_GET["location"]))
                    $location = $_GET["location"];
                else
                    die("Error: Missing parameter field in the request");

                if(isset($_GET["avatar"]))
                    $avatar = $_GET["avatar"];
                else
                    die("Error: Missing parameter field in the request");

                $role   = mysqli_escape_string($sql, $role);
                $id     = mysqli_escape_string($sql, $id);
                $role   = new Role($sql, $role);


                $user   = new Account($sql);
                $user->CreateFrom_ID((int)$id);

                $user->Display_Name($displayName);
                $user->Email($email);
                $user->Role($role);
                $user->DateOfBirth($dob);
                $user->Location($location);
                $user->Avatar($avatar);

                die("success");
                break;

            case "getTwitchPriorityList":
                $query = "SELECT * FROM atm_twitchview_priority";
                $resultPriority = mysqli_query($sql, $query)
                    or die(mysqli_error($sql));
                $query = "SELECT * FROM ares_links WHERE service='" . Service_Twitch . "'";
                $resultStreams = mysqli_query($sql, $query)
                    or die(mysqli_error($sql));


                $output = array();

                for($i = 0; $i < mysqli_num_rows($resultStreams); $i ++) {
                    $name   = explode("||", Aresak::mysqli_result($resultStreams, $i, "extras"));
                    $name   = strtolower($name[0]);
                    $priority = -1;
                    for($j = 0; $j < mysqli_num_rows($resultPriority); $j ++) {

                        if($priority == -1) {
                            if($name    == Aresak::mysqli_result($resultPriority, $j, "twitch")) {
                                $priority = Aresak::mysqli_result($resultPriority, $j, "priority");
                                $output["priority"][$priority] = $name;
                            }
                        }

                    }

                    if($priority == -1) {
                        $output["pleb"][ (isset($output["pleb"]) ? count($output["pleb"]) : 0) ] = $name;
                    }
                }
                $output["priority_size"] = count($output["priority"]);
                sort($output["pleb"]);
                die(json_encode($output));
                break;
            case "savePriority":
                if(isset($_GET["list"]))
                    $list = explode(":", $_GET["list"]);
                else
                    die("Error: Missing parameter field in the request");

                mysqli_query($sql, "DELETE FROM atm_twitchview_priority")
                    or die(mysqli_error($sql));

                for($i = 0; $i < count($list); $i ++) {
                    mysqli_query($sql, "INSERT INTO atm_twitchview_priority (twitch, priority) VALUES ('" . $list[$i] . "', '$i')")
                        or die(mysqli_error($sql));
                }
                die("success");
                break;

            case "show_playlists":
                $query = "SELECT * FROM atm_ytview_playlists";
                $result = mysqli_query($sql, $query)
                    or die(mysqli_error($sql));
                ?>
                <tr>
                    <td><b>ID</b></td>
                    <td><b>User ID</b></td>
                    <td><b>Playlist ID</b></td>
                    <td><b>LOCK/UNLOCK</b></td>
                </tr>
                <?php
                for($i = 0; $i < mysqli_num_rows($result); $i ++) {
                    $l = Aresak::mysqli_result($result, $i, "locked");
                    $id = Aresak::mysqli_result($result, $i, "ID");
                    echo "<tr>";
                    echo "<td class='lock-$l'>" . $id . "</td>";
                    echo "<td class='lock-$l'>" . Aresak::mysqli_result($result, $i, "user_id") . "</td>";
                    echo "<td class='lock-$l'>" . Aresak::mysqli_result($result, $i, "playlist_id") . "</td>";
                    echo "<td><a href='#' onclick='youtube.lockPlaylist(\"$id\")'><span class='lock " . ($l == 0 ? "glyphicon glyphicon-eye-close" : "glyphicon glyphicon-eye-open") . "'></span></a></td>";
                    echo "</tr>";
                }
                break;
            case "show_videos":
                $query = "SELECT * FROM atm_ytview_videos";
                $result = mysqli_query($sql, $query)
                    or die(mysqli_error($sql));

                ?>
                <tr>
                    <td><b>ID</b></td>
                    <td><b>Published At</b></td>
                    <td><b>Title</b></td>
                    <td><b>User ID</b></td>
                    <td><b>Playlist ID</b></td>
                    <td><b>Video ID</b></td>
                    <td><b>Channel ID</b></td>
                </tr>
                <?php
                for($i = 0; $i < mysqli_num_rows($result); $i ++) {
                    $l = Aresak::mysqli_result($result, $i, "locked");
                    $id = Aresak::mysqli_result($result, $i, "ID");
                    echo "<tr>";
                    echo "<td>" . Aresak::mysqli_result($result, $i, "ID") . "</td>";
                    echo "<td>" . Aresak::mysqli_result($result, $i, "published_at") . "</td>";
                    echo "<td><b>" . Aresak::mysqli_result($result, $i, "title") . "</b></td>";
                    echo "<td>" . Aresak::mysqli_result($result, $i, "user_id") . "</td>";
                    echo "<td>" . Aresak::mysqli_result($result, $i, "playlist_id") . "</td>";
                    echo "<td>" . Aresak::mysqli_result($result, $i, "video_id") . "</td>";
                    echo "<td>" . Aresak::mysqli_result($result, $i, "channel_id") . "</td>";
                    echo "</tr>";
                }
                break;
            case "lock-pl":
                $query = "SELECT * FROM atm_ytview_playlists WHERE ID='" . $_GET["id"] . "'";
                $result = mysqli_query($sql, $query)
                    or die(mysqli_error($sql));
                $l = Aresak::mysqli_result($result, 0, "locked");
                $query2 = "UPDATE atm_ytview_playlists SET locked='" . ($l == 0 ? 1 : 0) . "' WHERE ID='" . $_GET["id"] . "'";
                $result2 = mysqli_query($sql, $query2)
                    or die(mysqli_error($sql));
                echo "success ";
                break;
            case "fetch-playlists":
                $linksQuery             = "SELECT * FROM ares_links WHERE service='" . Service_YouTube . "'";
                $linksResult            = mysqli_query($sql, $linksQuery)
                    or die(mysqli_error($sql));
                $output = array();
                $output["start"] = time();
                $output["end"] = "";

                $playlistsExistsQuery   = "SELECT * FROM atm_ytview_playlists";
                $playlistsExistsResult  = mysqli_query($sql, $playlistsExistsQuery)
                    or die(mysqli_error($sql));


                for($linkIndex = 0; $linkIndex < mysqli_num_rows($linksResult); $linkIndex ++) {

                    $found = false;
                    for($playlistIndex = 0; $playlistIndex < mysqli_num_rows($playlistsExistsResult); $playlistIndex ++) {

                        if(
                            Aresak::mysqli_result($playlistsExistsResult, $playlistIndex, "user_id") ==
                            Aresak::mysqli_result($linksResult, $linkIndex, "user_id")
                        ) {
                            $found = true;
                            break;
                        }
                    }

                    $channelID = explode("||", Aresak::mysqli_result($linksResult, $linkIndex, "extras"));
                    $channelID = $channelID[2];

                    if(!$found) {
                        // add this playlist
                        $content = file_get_contents("https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id=" . $channelID . "&key=AIzaSyBdtvt44zw7jWIhL065tGCMJdO_aJkxSGs", true);
                        $j = json_decode($content, true);

                        if(isset($j["error"])) {
                            $output[$channelID]["status"] = false;
                            $output[$channelID]["error"] = $j["error"];
                        } else {
                            $output[$channelID]["id"] = $j["items"][0]["contentDetails"]["relatedPlaylists"]["uploads"];
                            $output[$channelID]["status"] = true;

                            $insertQuery    = "INSERT INTO atm_ytview_playlists (user_id, playlist_id) VALUES
                                          ('" . Aresak::mysqli_result($linksResult, $linkIndex, "user_id") . "',
                                           '" . $output[$channelID]["id"] . "')";
                            $insertResult   = mysqli_query($sql, $insertQuery)
                                or die(mysqli_error($sql));

                            $aytm           = new AresakYouTubeModule($sql);
                            // create playlist
                            $play = new YTPlaylist($aytm, $output[$channelID]["id"], Aresak::mysqli_result($linksResult, $linkIndex, "user_id"), 0);

                            // get all uploads
                            $play->GetUploads(true);

                            // insert all uploads
                            $play->NewVideos();

                            // dump the videos
                            $vids = array();
                            foreach($play->videos as $video) {
                                array_push($vids, array(
                                    "id" => $video->videoId,
                                    "title" => $video->title,
                                    "published" => $video->published_at
                                ));
                            }
                            $output[$channelID]["videos"] = $vids;
                            $output[$channelID]["videos_count"] = count($output[$channelID]["videos"]);
                        }
                    }
                }

                $output["end"] = time();
                die(json_encode($output));
                break;
            case "bot_loadStreams":
                $query      = "SELECT * FROM ares_bot_streams";
                $result     = mysqli_query($sql, $query)
                    or die(mysqli_error($sql));

                ?>
                <tr>
                    <td><b>Twitch</b></td>
                    <td><b>Name</b></td>
                    <td><b>Online since</b></td>
                    <td><b>Next status check</b></td>
                </tr>
                <?php

                for($i = 0; $i < mysqli_num_rows($result); $i ++) {
                    $t = Aresak::mysqli_result($result, $i, "next_check") - time();
                    $minutes = round($t / 60);
                    $seconds = $t % 60;

                    $linkQuery  = "SELECT * FROM ares_links WHERE ID='" . Aresak::mysqli_result($result, $i, "link_id") . "'";
                    $linkResult = mysqli_query($sql, $linkQuery)
                        or die(mysqli_error($sql));

                    $streamUser = new Account($sql);
                    $streamUser->CreateFrom_ID( Aresak::mysqli_result($linkResult, 0, "user_id") );
                    $twitchName = explode("||", $linkResult);

                    echo "<tr>";
                    echo "<td>" . $twitchName[0] . "</td>";
                    echo "<td>" . $streamUser->Display_Name() . "</td>";
                    echo "</tr>";
                }
                break;
            case "bot_loadViewers":
                break;
            case "bot_loadHosts":
                break;
            case "bot_removeStreams":
                break;
            case "bot_removeViewers":
                break;
            case "bot_saveCfg":
                break;
        }
    }
    else {
        // /me/index.php ajax
        switch($method) {
            case "val":
                if(isset($_GET["field"]))
                    $field  = $_GET["field"];
                else
                    die("Error: Missing parameter field in the request");

                if(isset($_GET["value"]))
                    $value  = $_GET["value"];
                else
                    die("Error: Missing parameter value in the request");

                switch($field) {
                    case "displayName":
                        $account->Display_Name($value);
                        die("success");
                        break;
                    case "password":
                        $newSalt    = Aresak::GenerateString(25);
                        $account->Password_Salt($newSalt);
                        $value      = md5( $account->Password_Salt() . $value );
                        $account->Password($value);
                        die("success");
                        break;
                    case "dob":
                        $account->DateOfBirth($value);
                        die("success");
                        break;
                    case "location":
                        $account->Location($value);
                        die("success");
                        break;
                    case "avatar":
                        $account->Avatar($value);
                        die("success");
                        break;
                }
                break;
            case "bio":
                if(isset($_GET["value"]))
                    $value  = $_GET["value"];
                else
                    die("Error: Missing parameter value in the request");

                $account->Bio($value);
                die("success");
                break;
            case "twitch-update":
                $twitch     = new Twitch($sql);
                if($twitch->CreateFrom_Account($account)) {
                    $account->Avatar( $twitch->Logo() );
                    $account->Bio( $twitch->Bio() );
                    die("success");
                } else {
                    die("Error: Couldn't create the account from Twitch");
                }
                break;
        }
    }
} else
    die("Error: Missing parameter method in the request");


?>