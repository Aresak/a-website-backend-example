<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 27.12.2017
 * Time: 13:03
 */

if(isset($_GET["l"])) {
    $link       = $_GET["l"];
} else die("Error: Link not found");

if(isset($_GET["r"])) {
    $redirect   = $_GET["r"];
} else die("Error: Redirect not found");

session_start();
$_SESSION["redirect"]       = $redirect;


header("Location: $link");