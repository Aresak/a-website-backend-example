<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 01.02.2018
 * Time: 12:28
 */

define("_D", "../");
require_once _D . "aresak.php";

if(isset($_GET["method"]))
    $method     = $_GET["method"];
else
    die("Error: Missing required request parameter 'method'");

if(isset($_GET["key"]))
    $key        = $_GET["key"];
else
    die("Error: Missing required request parameter 'key'");

$output         = array("start-time" => time(), "end-time" => 0);
$sql            = Aresak::SQL();
$host           = new Bot_Host($sql);

if(!$host->CreateFrom_Key($key)) {
    $output["error"]        = "Invalid key";
    $output["error-code"]   = 21;
    $output["end-time"]     = time();
    die(json_encode($output));
}

$host->Session_Check( time() );
$newKey                     = Aresak::GenerateString(20);
$keyExpire                  = time() + (60 * 60);
$output["key"]              = $newKey;
$host->Key_Old( $host->Key() );
$host->Key( $newKey );
$host->Key_Expire($keyExpire);
$output["key-expire"]       = $keyExpire;
$config                     = \GuzzleHttp\json_decode( file_get_contents(_SITE_ . "/ares/bot/config.json"), true );

switch($method) {
    case "left":
        if(!$host->Active()) {
            $output["error"] = "Not current processor";
            $output["error-code"] = 22;
            $output["end-time"] = time();
            die(json_encode($output));
        }

        if(isset($_GET["viewer"]))
            $viewer     = $_GET["viewer"];
        else
            die("Error: Missing required request parameter 'viewer'");

        if(isset($_GET["streamer"]))
            $streamer   = $_GET["streamer"];
        else
            die("Error: Missing required request parameter 'streamer'");

        $viewer         = mysqli_escape_string($sql, $viewer);
        $streamer       = mysqli_escape_string($sql, $streamer);

        $delQuery       = "DELETE FROM ares_bot_online WHERE viewer='$viewer' AND streamer='$streamer'";
        $delResult      = mysqli_query($sql, $delQuery)
            or die(mysqli_error($sql));

        $output["end-time"] = time();
        break;
    case "check":
        if(!$host->Active()) {
            $output["error"] = "Not current processor";
            $output["error-code"] = 22;
            $output["end-time"] = time();
            die(json_encode($output));
        }

        if(isset($_GET["viewer"]))
            $viewer     = $_GET["viewer"];
        else
            die("Error: Missing required request parameter 'viewer'");

        if(isset($_GET["streamer"]))
            $streamer   = $_GET["streamer"];
        else
            die("Error: Missing required request parameter 'streamer'");


        $viewer         = mysqli_escape_string($sql, $viewer);
        $streamer       = mysqli_escape_string($sql, $streamer);

        $getQuery       = "SELECT * FROM ares_bot_online WHERE viewer='$viewer' AND streamer='$streamer'";
        $getResult      = mysqli_query($sql, $getQuery)
            or die(mysqli_error($sql));

        if(mysqli_num_rows($getResult) == 0) {
            // Create a new log
            // ...
            $newQuery           = "INSERT INTO ares_bot_online (streamer, viewer, updated, started) VALUES
                            ('$streamer', '$viewer', '" . time() . "', '" . time() . "')";
            $newResult          = mysqli_query($sql, $newQuery)
                or die(mysqli_error($sql));


            $output["check"]    = "new";
            $output["end-time"] = time();
            die(json_encode($output));
        } else {
            // Update the user
            if($viewer != $streamer) {
                $linkQuery      = "SELECT * FROM ares_links WHERE service='" . Service_Twitch . "'";
                $linkResult     = mysqli_query($sql, $linkQuery)
                    or die(mysqli_error($sql));

                $found = false;
                for($i = 0; $i < mysqli_num_rows($linkResult); $i ++) {
                    $sn = explode("||", Aresak::mysqli_result($linkResult, $i, "extras"));
                    if($sn[0] == $viewer) {
                        $account->CreateFrom_ID( Aresak::mysqli_result($linkResult, $i, "user_id") );
                        $found = true;
                        break;
                    }
                }

                if(!$found) {
                    $output["warning"]      = "Viewer is not registered on the website or has not linked Twitch account";
                    $output["warning-code"] = 1;
                    $output["end-time"]     = time();
                    die(json_encode($output));
                }

                if($account->Username() == $viewer) {
                    // Add coins to user
                    $account->Coins( $account->Coins() + $config["coins"]["messenger"] );
                    Transaction::Create_New($sql, 0, $account, $config["coins"]["messenger"], "watching", "Watching " . $streamer);
                    $host->Session_Earned_Users( $host->Session_Earned_Users() + $config["coins"]["messenger"] );

                    $sc = $config["coins"]["messenger"] + Aresak::mysqli_result($getResult, 0, "coins_earned");

                    $updateQuery    = "UPDATE ares_bot_online SET coins_earned='$sc',
                                updated='" . time() . "' WHERE ID='" . Aresak::mysqli_result($getResult, 0, "ID") . "'";
                    $updateResult   = mysqli_query($sql, $updateQuery)
                        or die(mysqli_error($sql));

                    $output["coins-added"]  = $config["coins"]["messenger"];
                    $output["coins-session"] = $sc;
                    $output["end-time"]     = time();
                    $output["check"]        = "updated";
                    die(json_encode($output));
                    break;
                }

            }
            else {
                $output["warning"]      = "Cannot add coins for streamer itself";
                $output["warning-code"] = 2;
                $output["end-time"]     = time();
                die(json_encode($output));
            }
        }
        break;
    case "pulse":
        if($host->Active()) {
            // Current processor
            // ...
            if(isset($_GET["watching"]))
                $watching = \GuzzleHttp\json_decode($_GET["watching"]);
            else
                die("Error: Missing required request parameter 'watching'");

            Transaction::Create_New($sql, 0, $host->Account(), $config["coins"]["processor"], "hosting", "For hosting the DGI Twitch Bot");
            $host->Session_Earned_Self( $host->Session_Earned_Self() + $config["coins"]["processor"] );
            $hosts_count    = count( Bot_Host::GetAllHosts($sql) );

            $users          = array();
            foreach(Account::GetAllMembers($sql) as $_account) {
                array_push($users, array("name" => md5($_account->Username()), "coins" => $_account->Coins()));
            }
            $output["users"]= $users;
            $output["processor"]    = "you";


            // todo Get list of live streams here...
        } else {
            // Processor in queue
            // ...
            $hosts  = Bot_Host::GetAllHosts($sql);
            $mytime = true;
            $hosts_count = -1;
            foreach($hosts as $_host) {
                if($_host->Active()) {
                    if($_host->Session_Check() + $config["times"]["dead"] <= time()) {
                        // The current processor is DEAD!
                        $_host->Active(false);
                        $_host->Delete();
                    }
                    else {
                        // Valid active processor
                        $output["processor"] = $_host->Account()->Username();
                        $mytime = false;
                        $hosts_count ++;
                    }
                }
            }

            Transaction::Create_New($sql, 0, $host->Account(), $config["coins"]["queue"], "hosting", "For hosting the DGI Twitch Bot");
            $host->Session_Earned_Self( $host->Session_Earned_Self() + $config["coins"]["queue"] );
            if($mytime) {
                // HOHOHO! It's my time! Let's handle the requests x)))
                // ...
                $users          = array();
                foreach(Account::GetAllMembers($sql) as $_account) {
                    array_push($users, array("name" => md5($_account->Username()), "coins" => $_account->Coins()));
                }
                $output["users"]= $users;
                $output["processor"]    = "you";

                $host->Active(true);
            }

            $output["session-earned"]["self"]   = $host->Session_Earned_Self();
            $output["session-earned"]["users"]  = $host->Session_Earned_Users();
            $output["active"]   = $mytime;
            $output["end-time"] = time();
            $output["hosts-in-queue"] = $hosts_count;
            die(json_encode($output));
        }
        break;
    default:
        die("Error: Unknown method '$method'");
        break;
}