<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 02.02.2018
 * Time: 8:09
 */

define("_D", "../../");
require_once _D . "aresak.php";

if(isset($_GET["bye"])) {
    $bye    = true;

    if(isset($_GET["key"]))
        $key    = $_GET["key"];
    else
        die("Error: Missing required parameter 'key'");
}
else
    $bye    = false;


if(isset($_GET["uid"]))
    $uid    = $_GET["uid"];
else
    die("Error: Missing required parameter 'uid'");

if(isset($_GET["v"]))
    $version= $_GET["v"];
else
    die("Error: Missing required parameter 'v' version");


$sql        = Aresak::SQL();
$output     = array("start-time" => time(), "end-time" => 0);
if($bye) {
    // Good bye handshake
    // ...
    $host       = new Bot_Host($sql);
    if(!$host->CreateFrom_Key($key)) {
        $output["error"]    = "Invalid key";
        $output["error-code"] = 21;
        $output["end-time"] = time();
        die(json_encode($output));
    }

    $output["released"]     = true;
    $host->Delete();

    $output["end-time"] = time();
    die(json_encode($output));
} else {
    // Welcome handshake
    // ...
    $account    = new Account($sql);
    $account->CreateFrom_CreationDate($uid);

    $host       = new Bot_Host($sql);
    $out        = $host->CreateNew($account, $version);
    if(!empty($out["error"])) {
        $output["error"]    = $out["error"];
        $output["error-code"] = $out["error-code"];
        $output["end-time"] = time();
        $output["username"] = $account->Username();
        die(json_encode($output));
    }

    $output["key"]      = $out["key"];
    $output["key-expire"]   = $out["key-expire"];
    $output["active"]   = $out["active"];
    $output["username"] = $account->Username();

    $output["end-time"] = time();

    if(!isset($output["error-code"]))
        $output["error-code"] = 0;
    die(json_encode($output));
}