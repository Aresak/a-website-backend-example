<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 25.12.2017
 * Time: 14:33
 *
 *
 *
 *  THIS SCRIPT HAS TO BE ON TOP OF EVERY PAGE
 *
 */

require_once _D . "aresak.php";
$sql        = Aresak::SQL();
if(isset($_GET["activation"])) {
    $a = $_GET["activation"];
    $a = base64_decode($a);
    $a = explode("|", $a);
    $a = $a[1];
    $a      = mysqli_escape_string($sql, $a);
    $q      = "SELECT * FROM ares_accounts WHERE activation_key='$a'";
    $r      = mysqli_query($sql, $q)
        or die($sql);
    if(mysqli_num_rows($r) != 0) {
        $account = new Account($sql);
        $account->CreateFrom_ID((int) Aresak::mysqli_result($r, 0, "ID"));
        if(!$account->Activated()) {
            $account->Activated(true);
            header("Location: /ares/authenticate/");
        } else {
            header("Location: /");
        }
    } else {
        header("Location: /");
    }
}

if(isset($_COOKIE["aresak_auth"])) {
    $cookie     = new Cookie($sql);
    $cr         = $cookie->CreateFrom_Key($_COOKIE["aresak_auth"]);

    if(!$cr) {
        define("_Logged", false);
        setcookie("aresak_auth", "", time() - 1, "/");
    }
    else {
        $account            = $cookie->User();
        $_SESSION["uid"]    = $account->ID();
        define("_Logged", true);
    }
} else {
    define("_Logged", false);
}

if(!isset($account)) {
    $account = null;
}