<?php
    // PHP

    require_once "aresak.php";                                      // Include the library

    $results = "";


    /*
     * This is how to check if is the user logged in by cookie
     */
    if(isset($_COOKIE["aresak_auth"])) {
        // The authentication cookie exists
        $cookie = new Cookie(Aresak::SQL());                        // Create the cookie object, with linked database

        if($cookie->CreateFrom_Key( $_COOKIE["aresak_auth"] )) {    // Try to login with this cookie
            // There is linked this cookie to some account
            $account    = $cookie->User();                          // Get the user's account with this cookie
            // ... the account is now ready and set
        }
        else {
            // This cookie is fake
            setcookie("aresak_auth", "", time() - 1);               // Unset the fake cookie lol - plebs (hckers)
        }
    }



    // Testing Forms here...
    if(isset($_POST)) {
        // POST Requests
        switch($_POST["form"]) {
            case "login":
                $sql = Aresak::SQL();
                $account = new Account($sql);
                $pw = md5($_POST["password"]);
                try {
                    $b = $account->CreateFrom_Login($_POST["username"], $pw);
                    $results .= "Login State: $b";

                    // remember me
                    $cookie = new Cookie($sql, $account, Aresak::GenerateString(128));
                }
                catch(Exception $e) {
                    $s = "Login State: false, Error Code: " . $e->getCode();
                    /*
                     * Error Codes:
                     * [0]          Wrong username or password  (Username doesn't exists)
                     * [1]          Wrong username or password  (Password doesn't match)
                     * [3]          The account is not activated
                     * */
                    $results .= $s;
                }
                break;
            case "register":
                $account = new Account(Aresak::SQL());
                $pw = md5($_POST["password"]);
                try {
                    $b = $account->Register($_POST["username"], $_POST["display_name"], $pw, $_POST["email"], $_POST["dob"]);
                    $results .= "Register State: " . $b;
                }
                catch(Exception $e) {
                    $s = "Register State: false, Error Code: " . $e->getCode();
                    /*
                     * Error Codes:
                     * [0]          Username is already registered
                     * [1]          Email is already registered
                     * */
                    $results .= $s;
                }
                break;
            case "logout":

                break;
        }
    }
    if(!isset($account))
        $account = new Account();

    $t2     = new Twitch();
    $scopes = array("user_read", "user_subscriptions", "channel_subscriptions");
?>
<!-- HTML -->
<html>
    <head>
        <title>DGIv2 ARES Test</title>
        <!-- CSS -->
        <style>
            body {
                font-family: "Arial", serif;
            }
        </style>

        <!-- JS -->
        <script>

        </script>
    </head>
    <!-- BODY-AH -->
    <body>
        <div id="testing-forms">
            <!-- Testing Forms -->
            <b>Login</b><br>
            <form method="post">
                <input type="hidden" name="form" value="login">
                Username: <input type="text" name="username"><br>
                Password: <input type="text" name="password"><br>
                <button>LOGIN</button>
            </form>
            <br><br>
            <b>Register</b>
            <form method="post">
                <input type="hidden" name="form" value="register">
                Username: <input type="text" name="username"><br>
                Display Name: <input type="text" name="display_name"><br>
                Password: <input type="text" name="password"><br>
                Email: <input type="email" name="email"><br>
                Date of Birth: <input type="date" name="dob"><br>
                <button>REGISTER</button>
            </form>
            <br><br>
            <form method="post">
                <input type="hidden" name="form" value="logout">
                <button>LOGOUT</button>
            </form>
            <br><br>
            <a href="<?php echo $t2->GenerateAuthLink($scopes, $account); ?>">Twitch Auth link - <?php echo $t2->GenerateAuthLink($scopes, $account); ?></a>
        </div>
        <hr>
        <div id="testing-results">
            <!-- Testing Results -->
            <?php
                echo $results;


                if(isset($account)) {
                    if($account->Initialized()) {
                        echo "<br><hr>";
                        echo "ID: " . $account->ID() . "<br>";
                        echo "Username: " . $account->Username() . "<br>";
                        echo "Display name: " . $account->Display_Name() . "<br>";
                        echo "Email: " . $account->Email() . "<br>";
                        echo "Date of Birth: " . $account->DateOfBirth() . "<br>";
                        echo "Location: " . $account->Location() . "<br>";
                        echo "Avatar: " . $account->Avatar() . "<br>";
                        echo "Bio: " . $account->Bio() . "<br>";
                        echo "Banner: " . $account->Banner() . "<br>";
                        echo "Created at: " . $account->Created_At() . "<br>";
                        echo "Last seen at: " . $account->Last_Seen_At() . "<br>";
                        echo "Activation key: " . $account->Activation_Key() . "<br>";
                        echo "Activated: " . $account->Activated() . "<br>";
                    }
                }

                echo "<br>Cookie aresak_auth: " . $_COOKIE["aresak_auth"];
            ?>
        </div>
    </body>
</html>