<?php

define("_D", "../");
require_once _D . "header.php";

if(_Logged) {
    header("Location: " . _SITE_ . "?from=authenticate");                   // Already logged in, redirect to index
}

$ft     = new Twitch();
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-67750513-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-67750513-1');
    </script>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DragonsGetIt.Com</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo _SITE_; ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo _SITE_; ?>/css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo _SITE_; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="https://cdn.discordapp.com/attachments/189091483217821696/386006553926303744/whoslive_small25x25.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="<?php echo _SITE_; ?>/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo _SITE_; ?>/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
    </script>

    <?php

    Aresak::Meta();             // Write Aresak META

    ?>
</head>

<body>
<?php

Aresak::Body();             // Write Aresak BODY meta

?>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo _SITE_; ?>/index.php">DragonsGetIt</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo _SITE_; ?>/index.php">Home</a>
                </li>
                <li>
                    <a href="<?php echo _SITE_; ?>/whoarewe.php">Who are we?</a>
                </li>
                <li>
                    <a href="<?php echo _SITE_; ?>/social-requests.php">Social Requests</a>
                </li>
                <?php

                Aresak::NavBar($account);

                ?>
            </ul>
        </div>

        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Header Carousel -->
<header id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill" style="background-image:url('https://wallpaper.wiki/wp-content/uploads/2017/05/Youtube-Logo-Wallpapers.png');"></div>
            <div class="carousel-caption">
                <h3>YouTube Support</h3>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('https://i.ytimg.com/vi/1qN8l6WoZQI/maxresdefault.jpg');"></div>
            <div class="carousel-caption">
                <h3>Twitch Support</h3>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('http://donthatethegeek.com/wp-content/uploads/2016/05/gaming-28646-1680x1050.jpg');"></div>
            <div class="carousel-caption">
                <h3>Community Building</h3>
            </div>
        </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>

<!-- Page Content -->
<div class="container-fluid">

    <div class="row">

        <div id="forms">
            <div id="error" class="hidden2 alert alert-danger" role="alert"></div>
            <div id="success" class="hidden2 alert alert-success" role="alert"></div>
            <div id="dgi-login">
                <table class="table table-striped">
                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgil-username">
                                    Username:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="text" id="dgil-username" placeholder="Enter your username here">
                            <span class="text-danger" id="dgil-username-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgil-password">
                                    Password:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="password" id="dgil-password" placeholder="Enter your password here">
                            <span class="text-danger" id="dgil-password-err"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgil-remember">
                                    Remember me:
                                </label>
                            </b>
                            <input type="checkbox" id="dgil-remember" checked>
                        </td>
                        <td align="left">
                            <button onclick="a.login();" class="btn btn-primary">
                                LOGIN
                            </button>
                            or
                            <button onclick="a.switchL();" class="btn btn-default">
                                REGISTER
                            </button>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="dgi-register">
                <table class="table table-striped">
                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgir-displayName">
                                    Display name:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="text" id="dgir-displayName" placeholder="How will others see you as?">
                            <span class="text-danger" id="dgir-displayName-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgir-username">
                                    Username:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="text" id="dgir-username" placeholder="Username used to login with">
                            <span class="text-danger" id="dgir-username-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgir-pw1">
                                    Password:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="password" id="dgir-pw1" placeholder="Password user to login with">
                            <span class="text-danger" id="dgir-pw1-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" id="dgir-pwres" class="text-danger">
                        </td>
                        <td align="left">
                            <input type="password" id="dgir-pw2" placeholder="Your password again">
                            <span class="text-danger" id="dgir-pw2-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgir-email">
                                    Email:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="email" id="dgir-email" placeholder="Your email">
                            <span class="text-danger" id="dgir-email-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgir-dob">
                                    Your date of birth:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="date" id="dgir-dob">
                            <span class="text-danger" id="dgir-dob-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <button class="btn btn-primary" onclick="a.register();">
                                REGISTER
                            </button>
                        </td>
                        <td align="left">
                            <button class="btn btn-default" onclick="a.switchR();">
                                LOGIN
                            </button>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="services" align="center">
                <a id="con-twitch" href="<?php echo $ft->GenerateAuthLink(array(
                    "user_read",
                    "user_subscriptions",
                    "channel_subscriptions"
                )); ?>">
                    <img src="http://ttv-api.s3.amazonaws.com/assets/connect_dark.png" alt="Login with Twitch" title="Twitch">
                </a>
                <a id="con-youtube" href="<?php $y = new YouTube(); $y->Create_Client(); echo $y->Client()->createAuthUrl(); ?>">
                    <img width="172" height="32" src="https://i.stack.imgur.com/XzoRm.png" alt="Login with Google" title="Google">
                </a>
            </div>
        </div>

    </div>
    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Aiden Teran 2017</p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

</body>

</html>

<script>
    /* Scripts */
    $("#go-back").on("click", function() {

        window.location.href = "<?php echo _SITE_; ?>";

    });

    $("#dgir-displayName").on("input", function() {

        $("#dgir-username").val($(this).val().split(' ').join('_').toLowerCase());

    });

    var a = {
        UsernameLengthMin:      <?php echo _CC_UsernameLengthMin; ?>,
        UsernameLengthMax:      <?php echo _CC_UsernameLengthMax; ?>,
        UsernameLegal:          <?php echo _CC_UsernameLegal; ?>g,
        DisplayNameLengthMin:   <?php echo _CC_DisplayNameLengthMin; ?>,
        DisplayNameLengthMax:   <?php echo _CC_DisplayNameLengthMax; ?>,
        DisplayNameLegal:       <?php echo _CC_DisplayNameLegal; ?>g,
        PasswordMin:            <?php echo _CC_PasswordMin; ?>,
        PasswordMax:            <?php echo _CC_PasswordMax; ?>,
        DoBMin:                 <?php echo _CC_DoBMin; ?>,
        DoBMax:                 <?php echo _CC_DoBMax; ?>,

        login: function() {
            var username        = $("#dgil-username").val();
            var password        = $("#dgil-password").val();
            var remember        = $("#dgil-remember").is(":checked");

            if(username.length < this.UsernameLengthMin) {
                $("#dgil-username-err").text("Username is too short!");
                return false;
            } else if(username.length > this.UsernameLengthMax) {
                $("#dgil-username-err").text("Username is too long!");
                return false;
            }

            if(username.match(this.UsernameLegal).join('') != username) {
                $("#dgil-username-err").text("Username uses illegal characters!");
                return false;
            }

            if(password.length < this.PasswordMin) {
                $("#dgil-password-err").text("Password is too short!");
                return false;
            } else if(password.length > this.PasswordMax) {
                $("#dgil-password-err").text("Password is too long!");
                return false;
            }

            password            = md5(password);            // Hash the password for transfer

            $.get("ajax.php", {username: username, password: password, remember: remember},
                function(data) {
                    if(data     == "success") {
                        window.location.href = "<?php echo _SITE_; ?>";
                    } else {
                        var s = $("#error");
                        s.show();
                        s.html(data);
                    }
                }, "html");
        },
        register: function() {
            var displayName     = $("#dgir-displayName").val();
            var username        = $("#dgir-username").val();
            var password1       = $("#dgir-pw1").val();
            var password2       = $("#dgir-pw2").val();
            var email           = $("#dgir-email").val();
            var dob             = $("#dgir-dob").val();

            if(displayName.length < this.DisplayNameLengthMin) {
                console.error("E: Display name is too short!");
                $("#dgir-displayName-err").text("Display name is too short!");
                return false;
            } else if(displayName.length > this.DisplayNameLengthMax) {
                console.error("E: Display name is too long!");
                $("#dgir-displayName-err").text("Display name is too long!");
                return false;
            }

            if(displayName.match(this.DisplayNameLegal).join('') != displayName) {
                console.error("E: Display name uses illegal characters!");
                $("#dgir-displayName-err").text("Display name uses illegal characters!");
                return false;
            }

            if(username.length < this.UsernameLengthMin) {
                console.error("E: Username is too short!");
                $("#dgir-username-err").text("Username is too short!");
                return false;
            }
            else if(username.length > this.UsernameLengthMax) {
                console.error("E: Username is too long!");
                $("#dgir-username-err").text("Username is too long!");
                return false;
            }

            if(username.match(this.UsernameLegal).join('') != username) {
                console.error("E: Username uses illegal characters!");
                $("#dgir-username-err").text("Username uses illegal characters!");
                return false;
            }

            if(password1.length < this.PasswordMin) {
                console.error("E: The password is too short!");
                $("#dgir-pwres").text("The password is too short!");
                return false;
            } else if(password2.length > this.PasswordMax) {
                console.error("E: The password is too long!");
                $("#dgir-pwres").text("The password is too long!");
                return false;
            }

            if(password1 != password2) {
                console.error("E: There is a password mismatch!");
                $("#dgir-pwres").text("There is a password mismatch!");
                return false;
            }

            if(dob.split("-")[0] < this.DoBMin) {
                console.error("E: Is that you, Jesus?");
                $("#dgir-dob-err").text("Is that you, Jesus?");
                return false;
            }
            else if(dob.split("-")[0] > this.DoBMax) {
                console.error("E: Too young to apply");
                $("#dgir-dob-err").text("Too young to apply");
                return false;
            }


            password1                   = md5(password1);                   // Hash the password for transfer
            $.get("ajax.php", {
                username: username,
                password: password1,
                displayName: displayName,
                email: email,
                dob: dob
            }, function(data) {
                if(data     == "success") {
                    var s   = $("#success");
                    s.show();
                    s.html("Please now go to your email inbox and validate your email address. After that you will be able to login with this account.");

                    setTimeout(function() {
                        window.location.href    = "<?php echo _SITE_; ?>";
                    }, 5000);
                } else {
                    var s   = $("#error");
                    s.show();
                    s.html(data);
                }
            }, "html");
        },
        switchR: function() {
            $("#dgi-register").hide();
            $("#dgi-login").show();
        },
        switchL: function() {
            $("#dgi-login").hide();
            $("#dgi-register").show();
        }
    };
</script>

<style>
    /* Styles */

    #dgi-register {
        display: none;
    }


    .hidden2 {
        display: none;
    }
</style>
