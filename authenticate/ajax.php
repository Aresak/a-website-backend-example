<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 26.12.2017
 * Time: 17:42
 */
ini_set('display_errors', 'On');                                    // Display error
error_reporting(E_ALL | E_STRICT);                                  // Show even warnings and be very strict
define("_D", "../");
require_once _D . "aresak.php";
if(isset($_GET)) {
    // GET request
    $sql                    = Aresak::SQL();

    if(isset($_COOKIE["aresak_auth"])) {
        //die("Error: You are already logged in by cookie. Logout or reset your cookies on this site.");
    }


    if(isset($_GET["dob"])) {
        // Register Request
        // Initialize username field
        if(isset($_GET["username"])) {
            $username       = $_GET["username"];                                       // Fetch Username
        } else {
            die("Error: Missing field 'username'");                                     // No username field error
        }

        // Initialize display name field
        if(isset($_GET["displayName"])) {
            $displayName    = $_GET["displayName"];                                    // Fetch display name
        } else {
            die("Error: Missing field 'display name'");                                 // No display name field error
        }

        // Initialize password field
        if(isset($_GET["password"])) {
            $password       = $_GET["password"];                                       // Fetch password
        } else {
            die("Error: Missing field 'password'");                                     // No password field error
        }

        // Initialize email field
        if(isset($_GET["email"])) {
            $email          = $_GET["email"];                                          // Fetch email
        } else {
            die("Error: Missing field 'email'");                                        // No email field error
        }

        // Initialize date of birth field
        if(isset($_GET["dob"])) {
            $dob            = $_GET["dob"];                                            // Fetch date of birth
        } else {
            die("Error: Missing field 'dob'");                                          // No dob field error
        }


        // Requirements check
        // Is the password hashed?
        if(strlen($password) != 32) {
            die("Error: The password is not hashed!");
        }

        // Username check
        if(strlen($username) < _CC_UsernameLengthMin) {
            die("The username is too short!");
        } else if(strlen($username) > _CC_UsernameLengthMax) {
            die("The username is too long!");
        }

        $b1 = preg_match_all(_CC_UsernameLegal, $username, $username_match);
        if($b1 == 0 || is_bool($b1)) {
            die("Error: Username uses illegal characters");
        } else if( (join('', $username_match[0])) != $username ) {
            die("Error: Username uses illegal characters");
        }


        // Display name check
        if(strlen($displayName) < _CC_DisplayNameLengthMin) {
            die("The display name is too short!");
        } else if(strlen($displayName > _CC_DisplayNameLengthMax)) {
            die("The display name is too long!");
        }

        $b2 = preg_match_all(_CC_DisplayNameLegal, $displayName, $displayName_match);
        if($b2 == 0 || is_bool($b2)) {
            die("Error: Display name uses illegal characters");
        } else if( (join('', $displayName_match[0])) != $displayName ) {
            die("Error: Display name uses illegal characters");
        }

        // Date of birth check
        $e          = explode("-", $dob);
        if((int) $e[0] < (int) _CC_DoBMin) {
            die("Error: Are you Jesus?!");
        } else if((int) $e[0] > (int) _CC_DoBMax) {
            die("Error: You are too young to register.");
        }


        // Setup the account object
        $account        = new Account($sql);
        try {
            $register   = $account->Register(
                $username,
                $displayName,
                $password,
                $email,
                $dob
            );

            if(!$register) {
                die("Error: Failed to register for unknown reason.");
            }


            die("success");
        } catch(Exception $e) {
            switch($e->getCode()) {
                case 0:
                    // Username already registered
                    die("Error: This username is already taken X___x");
                    break;
                case 1:
                    // Email already registered
                    die("Error: This email is already taken x___X");
                    break;
            }
        }
    }
    else if(isset($_GET["logout"])) {
        if(isset($_COOKIE["aresak_auth"])) {
            $c          = $_COOKIE["aresak_auth"];
        } else {
            die("Error: There is no auth cookie stored");
        }

        $cookie         = new Cookie($sql);
        if(!$cookie->CreateFrom_Key($c)) {
            setcookie("aresak_auth", "", time() - 1, "/");                  // Expire the wrong cookie
            die("success");
        }

        setcookie("aresak_auth", "", time() - 1, "/");                      // Expire the old cookie
        $cookie->Delete();                                                  // Delete the cookie from the DB
        die("success");
    }
    else if(isset($_GET["remember"])) {
        // Login Request
        // Initialize username field
        if(isset($_GET["username"])) {
            $username   = $_GET["username"];
        } else {
            die("Error: Missing username field");
        }

        // Initialize password field
        if(isset($_GET["password"])) {
            $password   = $_GET["password"];
        } else {
            die("Error: Missing password field");
        }

        // Initialize remember me field
        if(isset($_GET["remember"])) {
            $remember   = $_GET["remember"];
        } else {
            die("Error: Missing remember me field");
        }

        // Password hash check
        if(strlen($password) != 32) {
            die("Error: The password isn't hashed");
        }


        // Create account object
        $account        = new Account($sql);
        try {
            $login      = $account->CreateFrom_Login($username, $password);
            if(!$login) {
                die("Error: Failed to login from unknown reason.");
            }

            if($remember) {
                $cookie = new Cookie($sql, $account, Aresak::GenerateString(128));                  // Set the cookie to few months
            } else {
                $cookie = new Cookie($sql, $account, Aresak::GenerateString(128), true);            // Set the cookie to 24 hours and
                                                                                                    // expire after the user closes
                                                                                                    // his browser
            }
            die("success");
        } catch(Exception $e) {
            switch($e->getCode()) {
                case 0:     // Wrong username
                case 1:     // Wrong password
                    die("Error: Wrong username or password");
                    break;
                case 3:     // Not activated
                    die("Error: Email is not validated yet");
                    break;
            }
        }
    } else {
        die("Error: So you are not logging in, or registering, or logging out... So what do you want from me?!");
    }
} else
    die("Error: Failed to fetch fields.");                                              // No GET method error