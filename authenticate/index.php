<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 25.12.2017
 * Time: 14:34
 *
 *  AUTHENTICATION PAGE
 */

define("_D", "../");
require_once "../header.php";

if(_Logged) {
    header("Location: " . _SITE_ . "?from=authenticate");                   // Already logged in, redirect to index
}

$ft     = new Twitch();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Authentication made by Aresak -->
        <!-- [HEAD] -->
        <title>Login | DragonsGetIt.com</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="icon" href="https://cdn.discordapp.com/attachments/189091483217821696/386006553926303744/whoslive_small25x25.png">

        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="http://developer.symbiant.cz/projects/dgi/ares/md5.js"></script>
        <!-- [/HEAD] -->
    </head>
    <body>
        <!-- [BODY] -->
        <div id="forms">
            <div id="error" class="hidden2 alert alert-danger" role="alert"></div>
            <div id="success" class="hidden2 alert alert-success" role="alert"></div>
            <div id="dgi-login">
                <table class="table table-striped">
                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgil-username">
                                    Username:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="text" id="dgil-username" placeholder="Enter your username here">
                            <span class="text-danger" id="dgil-username-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgil-password">
                                    Password:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="password" id="dgil-password" placeholder="Enter your password here">
                            <span class="text-danger" id="dgil-password-err"></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgil-remember">
                                    Remember me:
                                </label>
                            </b>
                            <input type="checkbox" id="dgil-remember" checked>
                        </td>
                        <td align="left">
                            <button onclick="a.login();" class="btn btn-primary">
                                LOGIN
                            </button>
                            or
                            <button onclick="a.switchL();" class="btn btn-default">
                                REGISTER
                            </button>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="dgi-register">
                <table class="table table-striped">
                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgir-displayName">
                                    Display name:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="text" id="dgir-displayName" placeholder="How will others see you as?">
                            <span class="text-danger" id="dgir-displayName-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgir-username">
                                    Username:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="text" id="dgir-username" placeholder="Username used to login with">
                            <span class="text-danger" id="dgir-username-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgir-pw1">
                                    Password:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="password" id="dgir-pw1" placeholder="Password user to login with">
                            <span class="text-danger" id="dgir-pw1-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right" id="dgir-pwres" class="text-danger">
                        </td>
                        <td align="left">
                            <input type="password" id="dgir-pw2" placeholder="Your password again">
                            <span class="text-danger" id="dgir-pw2-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgir-email">
                                    Email:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="email" id="dgir-email" placeholder="Your email">
                            <span class="text-danger" id="dgir-email-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <b>
                                <label for="dgir-dob">
                                    Your date of birth:
                                </label>
                            </b>
                        </td>
                        <td align="left">
                            <input type="date" id="dgir-dob">
                            <span class="text-danger" id="dgir-dob-err"></span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <button class="btn btn-primary" onclick="a.register();">
                                REGISTER
                            </button>
                        </td>
                        <td align="left">
                            <button class="btn btn-default" onclick="a.switchR();">
                                LOGIN
                            </button>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="services">
                <button id="go-back" class="btn btn-danger">GO BACK to INDEX page</button>
                <br>
                <a id="con-twitch" href="<?php echo $ft->GenerateAuthLink(array(
                    "user_read",
                    "user_subscriptions",
                    "channel_subscriptions"
                )); ?>">
                    <img src="http://ttv-api.s3.amazonaws.com/assets/connect_dark.png" alt="Login with Twitch" title="Twitch">
                </a>
                <br>
                <a id="con-youtube" href="<?php $y = new YouTube(); $y->Create_Client(); echo $y->Client()->createAuthUrl(); ?>">
                    <img width="172" height="32" src="https://i.stack.imgur.com/XzoRm.png" alt="Login with Google" title="Google">
                </a>
            </div>
        </div>
        <!-- [/BODY] -->
    </body>
</html>
<script>
    /* Scripts */
    $("#go-back").on("click", function() {

        window.location.href = "<?php echo _SITE_; ?>";

    });

    $("#dgir-displayName").on("input", function() {

        $("#dgir-username").val($(this).val().split(' ').join('_').toLowerCase());

    });

    var a = {
        UsernameLengthMin:      <?php echo _CC_UsernameLengthMin; ?>,
        UsernameLengthMax:      <?php echo _CC_UsernameLengthMax; ?>,
        UsernameLegal:          <?php echo _CC_UsernameLegal; ?>g,
        DisplayNameLengthMin:   <?php echo _CC_DisplayNameLengthMin; ?>,
        DisplayNameLengthMax:   <?php echo _CC_DisplayNameLengthMax; ?>,
        DisplayNameLegal:       <?php echo _CC_DisplayNameLegal; ?>g,
        PasswordMin:            <?php echo _CC_PasswordMin; ?>,
        PasswordMax:            <?php echo _CC_PasswordMax; ?>,
        DoBMin:                 <?php echo _CC_DoBMin; ?>,
        DoBMax:                 <?php echo _CC_DoBMax; ?>,

        login: function() {
            var username        = $("#dgil-username").val();
            var password        = $("#dgil-password").val();
            var remember        = $("#dgil-remember").is(":checked");

            if(username.length < this.UsernameLengthMin) {
                $("#dgil-username-err").text("Username is too short!");
                return false;
            } else if(username.length > this.UsernameLengthMax) {
                $("#dgil-username-err").text("Username is too long!");
                return false;
            }

            if(username.match(this.UsernameLegal).join('') != username) {
                $("#dgil-username-err").text("Username uses illegal characters!");
                return false;
            }

            if(password.length < this.PasswordMin) {
                $("#dgil-password-err").text("Password is too short!");
                return false;
            } else if(password.length > this.PasswordMax) {
                $("#dgil-password-err").text("Password is too long!");
                return false;
            }

            password            = md5(password);            // Hash the password for transfer

            $.get("ajax.php", {username: username, password: password, remember: remember},
                function(data) {
                    if(data     == "success") {
                        window.location.href = "<?php echo _SITE_; ?>";
                    } else {
                        var s = $("#error");
                        s.show();
                        s.html(data);
                    }
                }, "html");
        },
        register: function() {
            var displayName     = $("#dgir-displayName").val();
            var username        = $("#dgir-username").val();
            var password1       = $("#dgir-pw1").val();
            var password2       = $("#dgir-pw2").val();
            var email           = $("#dgir-email").val();
            var dob             = $("#dgir-dob").val();

            if(displayName.length < this.DisplayNameLengthMin) {
                console.error("E: Display name is too short!");
                $("#dgir-displayName-err").text("Display name is too short!");
                return false;
            } else if(displayName.length > this.DisplayNameLengthMax) {
                console.error("E: Display name is too long!");
                $("#dgir-displayName-err").text("Display name is too long!");
                return false;
            }

            if(displayName.match(this.DisplayNameLegal).join('') != displayName) {
                console.error("E: Display name uses illegal characters!");
                $("#dgir-displayName-err").text("Display name uses illegal characters!");
                return false;
            }

            if(username.length < this.UsernameLengthMin) {
                console.error("E: Username is too short!");
                $("#dgir-username-err").text("Username is too short!");
                return false;
            }
            else if(username.length > this.UsernameLengthMax) {
                console.error("E: Username is too long!");
                $("#dgir-username-err").text("Username is too long!");
                return false;
            }

            if(username.match(this.UsernameLegal).join('') != username) {
                console.error("E: Username uses illegal characters!");
                $("#dgir-username-err").text("Username uses illegal characters!");
                return false;
            }

            if(password1.length < this.PasswordMin) {
                console.error("E: The password is too short!");
                $("#dgir-pwres").text("The password is too short!");
                return false;
            } else if(password2.length > this.PasswordMax) {
                console.error("E: The password is too long!");
                $("#dgir-pwres").text("The password is too long!");
                return false;
            }

            if(password1 != password2) {
                console.error("E: There is a password mismatch!");
                $("#dgir-pwres").text("There is a password mismatch!");
                return false;
            }

            if(dob.split("-")[0] < this.DoBMin) {
                console.error("E: Is that you, Jesus?");
                $("#dgir-dob-err").text("Is that you, Jesus?");
                return false;
            }
            else if(dob.split("-")[0] > this.DoBMax) {
                console.error("E: Too young to apply");
                $("#dgir-dob-err").text("Too young to apply");
                return false;
            }


            password1                   = md5(password1);                   // Hash the password for transfer
            $.get("ajax.php", {
                username: username,
                password: password1,
                displayName: displayName,
                email: email,
                dob: dob
            }, function(data) {
                if(data     == "success") {
                    var s   = $("#success");
                    s.show();
                    s.html("Please now go to your email inbox and validate your email address. After that you will be able to login with this account.");

                    setTimeout(function() {
                        window.location.href    = "<?php echo _SITE_; ?>";
                    }, 5000);
                } else {
                    var s   = $("#error");
                    s.show();
                    s.html(data);
                }
            }, "html");
        },
        switchR: function() {
            $("#dgi-register").hide();
            $("#dgi-login").show();
        },
        switchL: function() {
            $("#dgi-login").hide();
            $("#dgi-register").show();
        }
    };
</script>

<style>
    /* Styles */
    #forms {
        position: fixed;
        top: 30%;
        left: 10%;
        width: 55%;
        height: 70%;
    }
    
    #dgi-register {
        display: none;
    }

    #services {
        position: fixed;
        top: 30%;
        right: 0;
        width: 35%;
        height: 70%;
        padding-left: 5px;
    }

    .hidden2 {
        display: none;
    }
</style>
